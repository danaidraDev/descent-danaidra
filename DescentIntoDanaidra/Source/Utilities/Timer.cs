﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DescentIntoDanaidra
{
    public class Timer
    {
        public double Counter { get; private set; }

        public bool Tick(int timeStep, GameTime gameTime)
        {
            if (Counter >= timeStep)
            {
                Reset();

                return true;
            }
            else
            {
                Counter += gameTime.ElapsedGameTime.TotalMilliseconds;
                return false;
            }
        }

        public double Current()
        {
            return Counter;
        }

        public void Reset()
        {
            Counter = 0;
        }
    }
}
