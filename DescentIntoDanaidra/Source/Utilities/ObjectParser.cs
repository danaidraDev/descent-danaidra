﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ObjectParser
    {
        ContentManager content;
        UIStateHandler ui;

        private readonly Dictionary<string, Action<string[], Location, Point>> parseMethods;

        public ObjectParser(ContentManager content, UIStateHandler ui)
        {
            this.content = content;
            this.ui = ui;

            parseMethods = new Dictionary<string, Action<string[], Location, Point>>()
            {
                { "obstacle", CreateObstacle },
                { "container", CreateContainer },
                { "npc", CreateNPC },
                { "bed", CreateBed },
                { "door", CreateDoor },
                { "altar", CreateAltar },
                { "sign", CreateSign },
                { "spawn", CreateSpawn },
                { "pushable", CreatePushable },
                { "dungeon", CreateDungeon },
                { "stash", CreateStash },
            };
        }
        
        public TimeOfDay TimeOfDay { get; set; }
        public TileTypeList TileTypeList { get; set; }
        public ObstacleTypeList ObstacleTypeList { get; set; }
        public MonsterTypeList MonsterTypeList { get; set; }
        public ContainerTypeList ContainerTypeList { get; set; }
        public AltarTypeList AltarTypeList { get; set; }
        public NPCTypeList NpcTypeList { get; set; }
        public FoodTypeList FoodTypeList { get; set; }
        public ArmorTypeList ArmorTypeList { get; set; }
        public WeaponTypeList WeaponTypeList { get; set; }
        public PotionTypeList PotionTypeList { get; set; }
        public WeaponModifierList WeaponModifierList { get; set; }
        public ArmorModifierList ArmorModifiersList { get; set; }
        public BookTypeList BookTypeList { get; set; }
        public ScrapTypeList ScrapTypeList { get; set; }
        public SkillTypeList SkilleTypeList { get; set; }

        public void ParseObject(string line, Location location)
        {
            string[] substr = line.Split(',');

            if (substr.Length > 2 && ParsePosition(substr[1], substr[2]) != null)
            {
                foreach (KeyValuePair<string, Action<string[], Location, Point>> m in parseMethods)
                {
                    if (substr[0] == m.Key)
                    {
                        m.Value(substr, location, ParsePosition(substr[1], substr[2]).GetValueOrDefault());
                    }
                }
            }
        }

        private void CreatePushable(string[] substr, Location location, Point position)
        {
            foreach (KeyValuePair<string, ObstacleType> p in ObstacleTypeList.Dictionary)
            {
                if (substr[3] == p.Key)
                {
                    new Pushable(location, position, p.Value);
                }
            }
        }

        private void CreateNPC(string[] substr, Location location, Point position)
        {
            foreach (KeyValuePair<string, NPCType> p in NpcTypeList.Dictionary)
            {
                if (substr[3] == p.Key)
                {
                    int walkAround;
                    if (int.TryParse(substr[5], out walkAround))
                    {
                        new NPC(location, position, p.Value, substr[4], TimeOfDay, walkAround, ui, content);
                    }
                }
            }
        }

        private void CreateBed(string[] substr, Location location, Point position)
        {
            new Bed(location, position, content.Load<Texture2D>("objects/bed"));
        }

        private void CreateStash(string[] substr, Location location, Point position)
        {
            new Stash(location, position, content.Load<Texture2D>("objects/chest"), ui);
        }

        private void CreateDoor(string[] substr, Location location, Point position)
        {
            new Door(location, position, TileTypeList);
        }

        private void CreateContainer(string[] substr, Location location, Point position)
        {
            foreach (KeyValuePair<string, ContainerType> p in ContainerTypeList.Dictionary)
            {
                if (substr[3] == p.Key)
                {
                    if (substr.Length > 4 && int.TryParse(substr[4], out int itemChance))
                    {
                        new Container(location, position, p.Value, ui, itemChance, 0);
                    }
                    else
                    {
                        new Container(location, position, p.Value, ui, 25, 0);
                    }
                }
            }
        }

        private void CreateObstacle(string[] substr, Location location, Point position)
        {
            foreach (KeyValuePair<string, ObstacleType> p in ObstacleTypeList.Dictionary)
            {
                if (substr[3] == p.Key)
                {
                    new Obstacle(location, position, p.Value);
                }
            }
        }

        private void CreateAltar(string[] substr, Location location, Point position)
        {
            if (substr[3] == "random")
            {
                new Altar(location, position, AltarTypeList.GetRandomType(), ui);
            }
            else
            {
                foreach (KeyValuePair<string, AltarType> p in AltarTypeList.Dictionary)
                {
                    if (substr[3] == p.Key)
                    {
                        new Altar(location, position, p.Value, ui);
                    }
                }
            }
        }

        private void CreateSign(string[] substr, Location location, Point position)
        {
            new Sign(location, position, ObstacleTypeList.Sign, !string.IsNullOrEmpty(substr[3]) ? substr[3] : "This is just a sign lol", ui);
        }

        private void CreateSpawn(string[] substr, Location location, Point position)
        {
            if (substr[3] == "random")
            {
                new MonsterSpawn(location, position, 2, 400, Randomiser.RandomNumber(2, 3), null, MonsterTypeList.GetRandomType(0));
                return;
            }
            else
            {
                foreach (KeyValuePair<string, MonsterType> p in MonsterTypeList.Dictionary)
                {
                    if (substr[3] == p.Key)
                    {
                        location.Spawns.Add(new MonsterSpawn(location, position, 2, 400, Randomiser.RandomNumber(2, 3), null, p.Value));
                    }
                }
            }
        }

        private void CreateDungeon(string[] substr, Location location, Point position)
        {
            if (substr[3] != "")
            {
                DungeonData data = DungeonData.LoadFromFile(substr[3]);

                if (data != null)
                {
                    var monsters = new List<MonsterType>();
                    foreach (string s in data.Monsters)
                    {
                        if (MonsterTypeList.Dictionary.TryGetValue(s, out MonsterType monsterType))
                        {
                            monsters.Add(monsterType);
                        }
                    }

                    new Dungeon(location, position, content, TileTypeList, MonsterTypeList, ObstacleTypeList, ContainerTypeList, ui)
                    {
                        Name = data.Name,
                        Music = MusicLibrary.DungeonTheme1,
                        TotalLevels = data.TotalLevels,
                        Width = data.Width,
                        Height = data.Height,
                        MinRooms = data.MinRooms,
                        MaxRooms = data.MaxRooms,
                        MinRoomWidth = data.MinRoomWidth,
                        MaxRoomWidth = data.MaxRoomWidth,
                        MinRoomHeight = data.MinRoomHeight,
                        MaxRoomHeight = data.MaxRoomHeight,
                        Cavelike = data.Cavelike,
                        ForceRegularRooms = false,
                        FloorType = TileTypeList.Floor,
                        WallType = TileTypeList.Sandstone,
                        MonstersPerLevel = data.MonstersPerLevel,
                        Monsters = monsters,
                        ItemsPerLevel = data.ItemsPerLevel,
                        ChestsPerLevel = data.ChestsPerLevel,
                        AmbientLight = new Color(data.RedAmbient, data.GreenAmbient, data.BlueAmbient),
                    };
                }
                else
                {
                    ui.OpenMessagePrompt("Error", "Could not load dungeon data for <" + substr[3] + ">.");
                }
            }
        }

        Point? ParsePosition(string strX, string strY)
        {
            int x = 0;
            int y = 0;

            if (int.TryParse(strX, out x) && int.TryParse(strY, out y))
            {
                return new Point(x, y);
            }
            else return null;
        }
    }
}
