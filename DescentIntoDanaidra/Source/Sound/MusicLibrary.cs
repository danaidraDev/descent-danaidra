﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class MusicLibrary
    {
        public MusicLibrary(ContentManager content)
        {
            MenuTheme = content.Load<SoundEffect>("music/menu_theme");
            GameoverTheme = content.Load<SoundEffect>("music/gameover_theme");
            CalmTheme1 = content.Load<SoundEffect>("music/calm_theme_1");
            DungeonTheme1 = content.Load<SoundEffect>("music/dungeon_theme_1");
            DungeonTheme2 = content.Load<SoundEffect>("music/dungeon_theme_2");
        }
        
        public static SoundEffect MenuTheme { get; private set; }
        public static SoundEffect DungeonTheme2 { get; private set; }
        public static SoundEffect DungeonTheme1 { get; private set; }
        public static SoundEffect GameoverTheme { get; private set; }
        public static SoundEffect CalmTheme1 { get; private set; }
    }
}
