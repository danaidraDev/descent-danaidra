﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra
{
    public class SoundHandler
    {
        private static SoundEffect currentMusic;
        private static SoundEffectInstance currentMusicInstance;
        private static Queue<SoundEffect> sfxQueue;

        public SoundHandler()
        {
            sfxQueue = new Queue<SoundEffect>();
            SfxVolume = 0.5f;
            MusicVolume = 0.5f;
        }

        public static float MusicVolume { get; private set; }
        public static float SfxVolume { get; private set; }

        public static void IncreaseSfxVolume()
        {
            SfxVolume = MathHelper.Clamp(SfxVolume + 0.1f, 0, 1);
        }

        public static void DecreaseSfxVolume()
        {
            SfxVolume = MathHelper.Clamp(SfxVolume - 0.1f, 0, 1);
        }

        public static void IncreaseMusicVolume()
        {
                MusicVolume = MathHelper.Clamp(MusicVolume + 0.1f, 0, 1);

            if (currentMusic != null)
            {
                currentMusicInstance.Volume = MusicVolume;
            }
        }

        public static void DecreaseMusicVolume()
        {
                MusicVolume = MathHelper.Clamp(MusicVolume - 0.1f, 0, 1);

            if (currentMusic != null)
            {
                currentMusicInstance.Volume = MusicVolume;
            }
        }

        public static void PlayMusic(SoundEffect music, bool looping)
        {
            if (music != currentMusic)
            {
                if (currentMusicInstance != null)
                {
                    currentMusicInstance.Stop();
                }
                currentMusic = music;

                if (currentMusic != null)
                {
                    currentMusicInstance = music.CreateInstance();
                    currentMusicInstance.Volume = MusicVolume;
                    currentMusicInstance.IsLooped = looping;
                    currentMusicInstance.Play();
                }
            }
        }

        public static void PlaySound(SoundEffect sound)
        {
            if (sfxQueue.Count < 24 && sound != null && !sfxQueue.Contains(sound))
            {
                sfxQueue.Enqueue(sound);
            }
        }

        public void Update()
        {
            while (sfxQueue.Any())
            {
                SoundEffectInstance sound = sfxQueue.Dequeue().CreateInstance();
                float pitchChange = Randomiser.RandomNumber(-15, 0) * 0.01f;
                sound.Volume = SfxVolume;
                sound.Pitch += pitchChange;
                sound.Play();
            }
        }
    }
}
