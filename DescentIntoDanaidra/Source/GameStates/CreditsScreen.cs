﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class CreditsScreen : GameState
    {
        private readonly ContentManager content;
        private readonly GraphicsDeviceManager graphics;
        private readonly InputHandler input;
        private readonly SoundHandler sound;
        private readonly Stack<GameState> gameStates;

        public CreditsScreen(ContentManager content, GraphicsDeviceManager graphics, InputHandler input, Stack<GameState> gameStates, SoundHandler sound)
        {
            this.content = content;
            this.gameStates = gameStates;
            this.input = input;
            this.sound = sound;
            this.graphics = graphics;
            Font = content.Load<SpriteFont>("fonts/font");
            BgImage = content.Load<Texture2D>("ui/splash");
            Background = new Rectangle(Point.Zero, Helper.ScreenSize());

            Window = new Window(new Rectangle(Helper.CenterOnScreen(UIStateHandler.WindowSize), UIStateHandler.WindowSize), content, Font)
            {
                Title = "Credits",
            };

            CreditsText = new TextBox("This game was made using the MonoGame framework (monogame.net) br br " +
                "Music was created using MuseScore (musescore.org) br br " +
                "Font used in the game is Generic Mobile System (dafont.com/generic-mobile-system.font) br br " +
                "Coding, design, art, sounds and music by danaidraDev (danaidradev.itch.io)",
                 Window.Box, Font);

            FitWindowToText();

            Menu = new Menu(new List<MenuOption>() { new MenuOption("Back", true, Close) },
                new Point(Helper.CenterHorizontally((int)Font.MeasureString("Back").X, Window.Box), Window.Box.Bottom - 15),
                Font, 0, false);

            Open();
        }

        public SpriteFont Font { get; private set; }
        public Window Window { get; private set; }
        public Menu Menu { get; private set; }
        public Rectangle Background { get; private set; }
        public Texture2D BgImage { get; private set; }
        public TextBox CreditsText { get; private set; }

        public override void Update(GameTime gameTime)
        {
            sound.Update();
            Menu.Update(input);

            if (KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            spriteBatch.Draw(BgImage, Background, Color.White);
            Window.Draw(spriteBatch);
            CreditsText.Draw(spriteBatch);
            Menu.Draw(spriteBatch);
            spriteBatch.End();
        }

        private void FitWindowToText()
        {
            Point size = new Point(UIStateHandler.WindowSize.X, CreditsText.Height() + 100);
            Window.Rectangle = new Rectangle(Helper.CenterOnScreen(size), size);
            CreditsText.Box = new Rectangle(Window.Box.Left, Window.Box.Top, Window.Box.Width, 50);
        }

        public override void Open()
        {
        }

        public override void Close()
        {
            End = true;
        }
    }
}
