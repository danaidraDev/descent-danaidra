﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class Tile
    {
        public Tile(TileType type)
        {
            Type = type;
        }

        public TileType Type { get; set; }
        public TileType.Kind TileKind { get { return Type.TileKind; } }
        public Texture2D Texture { get { return Type.Texture; } }
        public Texture2D SideTexture { get { return Type.SideTexture; } }
    }
}
