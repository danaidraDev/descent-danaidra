﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class TileType
    {
        public TileType(Kind tileKind, Texture2D texture, Texture2D sideTexture)
        {
            TileKind = tileKind;
            Texture = texture;
            SideTexture = sideTexture;
        }

        public enum Kind
        {
            floor,
            wall,
            pit
        }

        public Kind TileKind { get; private set; }
        public Texture2D Texture { get; private set; }
        public Texture2D SideTexture { get; set; }
    }
}
