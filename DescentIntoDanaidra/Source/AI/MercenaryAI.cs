﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class MercenaryAI : CharacterAI
    {
        private const int DetectRange = 5;
        private const int MaxDistanceFromPlayer = 3;
        private const int MaxTurnsAway = 15;
        private const int ChanceToMove = 75;
        private const int BasePrice = 75;
        public Player FollowedPlayer { get; private set; }

        public override void Behave(NPC npc, Player player)
        {
            if (player.Party.Contains(npc))
            {
                if (npc.TurnsAwayFromAnchor < MaxTurnsAway)
                {
                    npc.TurnsAwayFromAnchor++;

                    IMapObject monsterNearPlayer = npc.Location.Tilemap.FindClosest(typeof(Monster), player.Position.GetValueOrDefault());

                    if (monsterNearPlayer != null && monsterNearPlayer is Monster monster && !monster.IsDead)
                    {
                        AttackMonster(npc, monster);
                    }
                    else
                    {
                        FollowPlayer(npc, player);
                    }
                }
                else
                {
                    FollowPlayer(npc, player);
                }
            }
            else
            {
                ReturnToAnchor(npc);
            }
        }

        public override bool OnCollision(NPC npc, Player player)
        {
            if (player.Party.Contains(npc))
            {
                GetShoved(npc, player);
            }
            else
            {
                npc.ui.OpenYesNoPrompt("Recruit?", "Do you want to recruit this mercenary for " + PriceToRecruit(npc) + " gp?",
                    new Action(() => { JoinPlayerParty(npc, player); }), null);
            }
            return false;
        }

        private void JoinPlayerParty(NPC npc, Player player)
        {
            if (player.Gold.Current > PriceToRecruit(npc))
            {
                player.Party.Add(npc);
                FollowedPlayer = player;
                player.Gold.Current -= PriceToRecruit(npc);
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You do not have enough gold!", Colors.Orange);
            }
        }

        private int PriceToRecruit(NPC npc)
        {
            return BasePrice + (BasePrice / 2) * npc.Level.Current;
        }

        private void GetShoved(NPC npc, Player player)
        {
            Point previousPosition = npc.Position.GetValueOrDefault();
            Point targetPosition = npc.NearestFreeToTeleport(MaxDistanceFromPlayer).GetValueOrDefault();

            if (npc.CanMoveToPosition(targetPosition))
            {
                FloatingTextHandler.DisplayMapText(new FloatingText("Shove!", Colors.SteelGray, npc.Position.GetValueOrDefault()));
                npc.Location.Tilemap.ChangeObjectPosition(npc, targetPosition);
            }
        }

        private void AttackMonster(NPC npc, Monster monster)
        {
            if (AI.AreNeighboring(npc, monster))
            {
                Combat.MeleeHit(npc, monster, null);
            }
            else
            {
                AI.MoveTowards(npc, monster);
            }
        }

        private void FollowPlayer(NPC npc, Player player)
        {
            if (TooFarFromPlayer(npc, player))
            {
                ReturnToPlayer(npc, player);
            }
            else
            {
                ResetTurnsAway(npc);

                if (CanWalkAround(npc))
                {
                    WanderAround(npc);
                }
            }
        }

        protected void ReturnToPlayer(NPC npc, Player player)
        {
            Point returnPosition = player.NearestFreeToTeleport(MaxDistanceFromPlayer).GetValueOrDefault();
            if (npc.TurnsAwayFromAnchor > MaxTurnsAway)
            {
                npc.Move(returnPosition);
            }
            else
            {
                npc.TurnsAwayFromAnchor++;
                AI.MoveTowards(npc, player.Position.GetValueOrDefault());
            }
        }

        protected bool TooFarFromPlayer(NPC npc, Player player)
        {
            return Vector2.Distance(npc.Position.GetValueOrDefault().ToVector2(), player.Position.GetValueOrDefault().ToVector2()) > MaxDistanceFromPlayer;
        }
    }
}
