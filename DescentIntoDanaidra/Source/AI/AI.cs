﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DescentIntoDanaidra
{
    public static class AI
    {
        public static float DistanceBetween(IMoveable moveable, IMoveable target)
        {
            return (moveable.Position != null && target.Position != null) ?
                Vector2.Distance(moveable.Position.GetValueOrDefault().ToVector2(), target.Position.GetValueOrDefault().ToVector2()) : 0;
        }

        public static bool HasLowHealth(IFighter fighter)
        {
            return fighter.Health.Current < fighter.Health.Max / 4;
        }

        public static bool AreNeighboring(IMoveable moveable, IMoveable target)
        {
            return DistanceBetween(moveable, target) < 2;
        }

        public static void MoveAtRandom(IMoveable moveable)
        {
            if (moveable.Position != null)
            {
                Point pos = moveable.Position.GetValueOrDefault();
                moveable.Move(new Point(Randomiser.RandomNumber(pos.X - 1, pos.X + 1), Randomiser.RandomNumber(pos.Y - 1, pos.Y + 1)));
            }
        }

        public static void MoveTowards(IMoveable moveable, Point position)
        {
            if (moveable.Position != null && OpenNeighbors(moveable).Any())
            {
                double minCost = OpenNeighbors(moveable).Min(p => MovementCost(moveable.Position.GetValueOrDefault(), p,
                    position));
                moveable.Move(OpenNeighbors(moveable).FirstOrDefault(p => MovementCost(moveable.Position.GetValueOrDefault(),
                    p, position) == minCost));
            }
        }

        public static void MoveTowards(IMoveable moveable, IMapObject mapObject)
        {
            MoveTowards(moveable, mapObject.Position.GetValueOrDefault());
        }

        private static double GetMinCost(IMoveable fighter, IMapObject mapObject)
        {
            return OpenNeighbors(fighter).Min(p => MovementCost(fighter.Position.GetValueOrDefault(), p,
                                mapObject.Position.GetValueOrDefault()));
        }

        public static void MoveAwayFrom(IMoveable moveable, IMapObject mapObject)
        {
            if (moveable.Position != null && mapObject.Position != null && OpenNeighbors(moveable).Any())
            {
                moveable.Move(OpenNeighbors(moveable).FirstOrDefault(p => MovementCost(moveable.Position.GetValueOrDefault(),
                    p, mapObject.Position.GetValueOrDefault()) == GetMaxCost(moveable, mapObject)));
            }
        }

        private static double GetMaxCost(IMoveable moveable, IMapObject mapObject)
        {
            return OpenNeighbors(moveable).Max(p => MovementCost(moveable.Position.GetValueOrDefault(), p,
                                mapObject.Position.GetValueOrDefault()));
        }

        public static List<Point> OpenNeighbors(IMoveable moveable)
        {
            return moveable.Position != null ? CountOpenNeighbors(moveable) : new List<Point>();
        }

        private static List<Point> CountOpenNeighbors(IMoveable moveable)
        {
            var resulting = new List<Point>();
            Point position = moveable.Position.GetValueOrDefault();
            for (int x = -1; x <= 1; x++)
                for (int y = -1; y <= 1; y++)
                {
                    Point pos = new Point(position.X + x, position.Y + y);
                    if (pos != position && IsPositionOpen(moveable, position, pos))
                    {
                        resulting.Add(pos);
                    }
                }
            return resulting;
        }

        private static bool IsPositionOpen(IMoveable moveable, Point position, Point pos)
        {
            return moveable.Location.Tilemap.IsPositionWithinTilemap(pos) &&
                moveable.Location.Tilemap.IsWalkable(pos) &&
                moveable.Location.Tilemap.GetObject(pos) == null;
        }

        public static double MovementCost(Point start, Point tested, Point target)
        {
            double g = Vector2.Distance(start.ToVector2(), tested.ToVector2());
            double h = Vector2.Distance(target.ToVector2(), tested.ToVector2());
            double f = g + h;
            return f;
        }

        public static bool CanMove(IMoveable moveable)
        {
            return OpenNeighbors(moveable).Any();
        }
    }
}
