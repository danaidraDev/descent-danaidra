﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class GuardAI : CharacterAI
    {
        private const int DetectRange = 5;

        public override void Behave(NPC npc, Player player)
        {
            if (CanSeeMonster(npc, out Monster monster) && !monster.IsDead)
            {
                AttackMonster(npc, monster);
            }
            else
            {
                StandGuard(npc);
            }
        }

        private void AttackMonster(NPC npc, Monster monster)
        {
            if (AI.AreNeighboring(npc, monster))
            {
                InstaKill(npc, monster);
            }
            else
            {
                AI.MoveTowards(npc, monster);
            }
        }

        private void InstaKill(NPC npc, Monster target)
        {
            HUD.MessageLog.DisplayMessage(npc.Name + " obliterates " + target.Name + ".", Colors.Orange);
            SoundHandler.PlaySound(SoundLibrary.ClangSquelch);
            target.Die();
        }

        private void StandGuard(NPC npc)
        {
            if (TooFarFromAnchor(npc))
            {
                ReturnToAnchor(npc);
            }
            else
            {
                ResetTurnsAway(npc);

                if (CanWalkAround(npc))
                {
                    WanderAround(npc);
                }
            }
        }

        private bool CanSeeMonster(NPC npc, out Monster seenMonster)
        {
            Point position = npc.Position.GetValueOrDefault();
            for (int x = position.X - DetectRange; x < position.X + DetectRange; x++)
                for (int y = position.Y - DetectRange; y < position.Y + DetectRange; y++)
                {
                    if (npc.Location.Tilemap.GetObject(new Point(x, y)) != null &&
                        npc.Location.Tilemap.GetObject(new Point(x, y)) is Monster monster)
                    {
                        seenMonster = monster;
                        return true;
                    }
                }
            seenMonster = null;
            return false;
        }
    }
}
