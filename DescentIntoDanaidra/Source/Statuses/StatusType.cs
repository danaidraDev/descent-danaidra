﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class StatusType
    {
        public StatusType(string name, Texture2D icon, Status.AffectTarget affect, Color color)
        {
            Name = name;
            Icon = icon;
            Affect = affect;
            Color = color;
        }

        public string Name { get; private set; }
        public Texture2D Icon { get; private set; }
        public Status.AffectTarget Affect { get; private set; }
        public Color Color { get; private set; }
    }
}
