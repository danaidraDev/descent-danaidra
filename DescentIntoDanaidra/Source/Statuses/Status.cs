﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Status
    {
        public Status(StatusType type, int lasts)
        {
            Type = type;
            Lasts = lasts;
        }

        public StatusType Type { get; private set; }
        public string Name { get { return Type.Name; } }
        public Texture2D Icon { get { return Type.Icon; } }
        public delegate void AffectTarget(IFighter fighter);
        AffectTarget Affect { get { return Type.Affect; } }
        public int Lasts { get; private set; }
        public int TurnsElapsed { get; private set; }
        public Color Color { get { return Type.Color; } }

        public void Update(IFighter target)
        {
            Affect(target);
            TurnsElapsed++;
        }
    }
}
