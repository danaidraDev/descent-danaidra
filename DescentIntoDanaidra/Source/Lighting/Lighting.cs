﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Lighting
    {
        private readonly Color[,] calculatedColors;
        private readonly int width;
        private readonly int height;
        private const int MinLightDifference = 10;

        public Lighting(Viewport viewport)
        {
            width = viewport.Width / Game1.TileSize;
            height = viewport.Height / Game1.TileSize;
            calculatedColors = new Color[width, height];
        }

        public Color GetCalculatedColor(int x, int y)
        {
            if (x >= 0 && x < width && y >= 0 && y < height)
            {
                return calculatedColors[x, y];
            }
            return Color.Magenta;
        }

        public void Update(Location location, Camera camera)
        {
            List<ILightSource> lightSources = GetLightSources(location, camera);

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    calculatedColors[x, y] = CalculateColor(location, camera, x, y, lightSources);
                }
        }

        private Color CalculateColor(Location location, Camera camera, int x, int y, List<ILightSource> lightSources)
        {
            int red = location.AmbientLight.R;
            int green = location.AmbientLight.G;
            int blue = location.AmbientLight.B;

            Vector2 position = new Vector2(camera.Position.X / Game1.TileSize + x, camera.Position.Y / Game1.TileSize + y);

            foreach (ILightSource l in lightSources)
            {
                float falloffFactor = Vector2.Distance(l.Position.GetValueOrDefault().ToVector2(), position) * l.Light.Falloff;
                Vector3 light = new Vector3(l.Light.Color.R, l.Light.Color.G, l.Light.Color.B) * l.Light.Intensity / (falloffFactor > 1 ? falloffFactor : 1);
                red += light.X > MinLightDifference ? (int)light.X : 0;
                green += light.Y > MinLightDifference ? (int)light.Y : 0;
                blue += light.Z > MinLightDifference ? (int)light.Z : 0;
            }

            return new Color(MathHelper.Clamp(red, 0, 255), MathHelper.Clamp(green, 0, 255), MathHelper.Clamp(blue, 0, 255));
        }

        private List<ILightSource> GetLightSources(Location location, Camera camera)
        {
            List<ILightSource> lightSources = new List<ILightSource>();

            int tileSize = Game1.TileSize;

            for (int y = camera.Position.Y / tileSize - 10; y < Math.Ceiling((double)(camera.Position.Y + camera.Viewport.Height) / tileSize) + 10; y++)
                for (int x = camera.Position.X / tileSize - 10; x < Math.Ceiling((double)(camera.Position.X + camera.Viewport.Width) / tileSize) + 10; x++)
                {
                    Point position = new Point(x, y);

                    IMapObject entity = location.Tilemap.GetObject(position);
                    if (entity != null && entity.Texture != null && entity is ILightSource lightSource && lightSource.Light != null)
                    {
                        lightSources.Add(lightSource);
                    }

                    foreach (Projectile p in location.Projectiles)
                    {
                        if (p.Position == position && p.Texture != null)
                        {
                            lightSources.Add(p);
                        }
                    }
                }

            return lightSources;
        }
    }
}
