﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Light
    {
        private float _intensity;
        private float _falloff;

        public Light(float intensity, float falloff, Color color)
        {
            Intensity = intensity;
            Falloff = falloff;
            Color = color;
        }

        public float Intensity { get => _intensity; private set => _intensity = MathHelper.Clamp(value, 0, 2); }
        public float Falloff { get => _falloff; private set => _falloff = MathHelper.Clamp(value, 1, 100); }
        public Color Color { get; private set; }
    }
}
