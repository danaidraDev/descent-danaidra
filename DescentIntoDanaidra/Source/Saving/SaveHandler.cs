﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Xml;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    public class SaveHandler
    {
        private readonly ObjectParser parser;
        private readonly string saveDirectory;
        private readonly string saveFilePath;
        private readonly string inventoryFilePath;

        public SaveHandler(ObjectParser parser)
        {
            this.parser = parser;
            saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\DescentIntoDanaidra";
            saveFilePath = saveDirectory + "\\save.xml";
            inventoryFilePath = saveDirectory + "\\saveItem.xml";
        }

        public void SaveGame(Player player, TimeOfDay timeOfDay)
        {
            if (player.Location is OutdoorLocation)
            {
                SaveData saveData = SaveData.CreateSaveData(player, timeOfDay);

                XmlSerializer writer = new XmlSerializer(typeof(SaveData));

                if (!SaveDirectoryExists())
                {
                    DirectoryInfo di = Directory.CreateDirectory(saveDirectory);
                }

                DeleteSaveFiles();

                System.IO.FileStream file = System.IO.File.Create(saveFilePath);
                writer.Serialize(file, saveData);
                file.Close();
                
                SaveInventories(player);
            }
        }

        public void SaveInventories(Player player)
        {
            using (XmlWriter writer = XmlWriter.Create(inventoryFilePath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Inventories");

                if (player.Equipment != null && player.Equipment.Any())
                {
                    var equipment = new List<IInventorable>();
                    equipment.AddRange(player.Equipment);
                    SaveInventory(equipment, "Equipment", writer);
                }

                if (player.Stash != null && player.Stash.Any())
                {
                    var stash = new List<IInventorable>();
                    stash.AddRange(player.Stash);
                    SaveInventory(stash, "Stash", writer);
                }

                if (player.Skills != null && player.Skills.Any())
                {
                    var skills = new List<IInventorable>();
                    skills.AddRange(player.Skills);
                    SaveInventory(skills, "Skills", writer);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public void SaveInventory(List<IInventorable> list, string startElementName, XmlWriter writer)
        {
            writer.WriteStartElement(startElementName);

            foreach (IInventorable i in list)
            {
                if (i is ISaveable saveable)
                {
                    saveable.Save(writer);
                }
            }

            writer.WriteEndElement();
        }

        public void DeleteSaveFiles()
        {
            if (SaveFileExists())
            {
                File.Delete(saveFilePath);
            }
            if (InventoryFileExists())
            {
                File.Delete(inventoryFilePath);
            }
        }

        public SavePackage LoadGame()
        {
            SavePackage savePackage = new SavePackage();
            
            if (SaveFileExists())
            {
                savePackage.SaveData = SaveData.Load(saveFilePath);
            }
            if (InventoryFileExists())
            {
                LoadInventories(savePackage);
            }

            return savePackage;
        }

        private void LoadInventories(SavePackage savePackage)
        {
            savePackage.Equipment = new List<Item>();
            savePackage.Stash = new List<Item>();
            savePackage.Skills = new List<Skill>();

            CurrentInventory currentInventory = CurrentInventory.None;

            using (XmlReader reader = XmlReader.Create(inventoryFilePath))
            {
                while (reader.Read())
                {
                    switch (reader.Name)
                    {
                        case "Equipment":
                            currentInventory = CurrentInventory.Equipment;
                            break;
                        case "Stash":
                            currentInventory = CurrentInventory.Stash;
                            break;
                        case "Skills":
                            currentInventory = CurrentInventory.Skills;
                            break;
                        default:
                            break;
                    }

                    switch (currentInventory)
                    {
                        case CurrentInventory.Equipment:
                            ReadEquipment(savePackage, reader);
                            break;
                        case CurrentInventory.Stash:
                            ReadStash(savePackage, reader);
                            break;
                        case CurrentInventory.Skills:
                            ReadSkills(savePackage, reader);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        enum CurrentInventory
        {
            None,
            Equipment,
            Stash,
            Skills,
        }
        
        private void ReadEquipment(SavePackage savePackage, XmlReader reader)
        {
            if (reader.IsStartElement())
            {
                Item item = Item.Load(reader, parser);

                if (item != null)
                {
                    savePackage.Equipment.Add(item);
                }
            }
            else return;
        }

        private void ReadStash(SavePackage savePackage, XmlReader reader)
        {
            if (reader.IsStartElement())
            {
                Item item = Item.Load(reader, parser);

                if (item != null)
                {
                    savePackage.Stash.Add(item);
                }
            }
        }

        private void ReadSkills(SavePackage savePackage, XmlReader reader)
        {
            if (reader.IsStartElement())
            {
                Skill skill = Skill.Load(reader, parser);

                if (skill != null)
                {
                    savePackage.Skills.Add(skill);
                }
            }
        }

        public bool SaveFileExists()
        {
            return SaveDirectoryExists() && File.Exists(saveFilePath);
        }

        public bool InventoryFileExists()
        {
            return SaveDirectoryExists() && File.Exists(inventoryFilePath);
        }

        private bool SaveDirectoryExists()
        {
            return Directory.Exists(saveDirectory);
        }
    }
}