﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public class SavePackage
    {
        public SaveData SaveData { get; set; }
        public List<Item> Equipment { get; set; }
        public List<Item> Stash { get; set; }
        public List<Skill> Skills { get; set; }
    }
}
