﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class SkillTypeList
    {
        private readonly StatusTypeList statuses;
        private readonly ProjectileTypeList projectiles;

        public SkillTypeList(ContentManager content, StatusTypeList statusTypes, ProjectileTypeList projectileTypes)
        {
            statuses = statusTypes;
            projectiles = projectileTypes;

            Healing = new SkillType("Healing", content.Load<Texture2D>("skills/healing"), 5, 7, true, HealingEffect)
            {
                Description = "This spell immediately restores some of the caster's health.",
                
            };

            RegenerationSpell = new SkillType("Regeneration", content.Load<Texture2D>("skills/regenerationSpell"), 10, 10, true, RegenerationSpellEffect)
            {
                Description = "Caster's health regenerates much faster for a period of time.",
            };

            CastFireball = new SkillType("Cast Fireball", content.Load<Texture2D>("skills/castFireball"), 10, 10, false, CastFireballEffect)
            {
                Description = "Creates a ball of fire that you can hurl at your enemies.",
            };

            Dictionary = new Dictionary<string, SkillType>()
            {
                { Healing.Name.ToLower(), Healing },
                { RegenerationSpell.Name.ToLower(), RegenerationSpell },
                { CastFireball.Name.ToLower(), CastFireball },
            };
        }

        public Dictionary<string, SkillType> Dictionary { get; private set; }

        public SkillType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, SkillType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out SkillType type))
                {
                    return type;
                }
            }
            return null;
        }

        public SkillType Healing { get; }
        void HealingEffect(Player player, Skill skill)
        {
            int heal = 10 + skill.Level.Current * 2 + Randomiser.RandomNumber(0,3);
            HUD.MessageLog.DisplayMessage(player.Name + "'s " + skill.Name + " restores " + heal + " health to " + player.Name + ".", Colors.LightGreen);
            player.Health.Current += heal;
        }
        
        public SkillType RegenerationSpell { get; }
        void RegenerationSpellEffect(Player player, Skill skill)
        {
            int lasts = 15 + skill.Level.Current * 2 + Randomiser.RandomNumber(0, 3);
            player.AddStatus(new Status(statuses.Regeneration, lasts));
        }

        public SkillType CastFireball { get; }
        void CastFireballEffect(Player player, Skill skill)
        {
            int power = skill.Level.Current + 3 + Randomiser.RandomNumber(0, 3);
            player.Aim(projectiles.Fireball, skill.Level.Current + 5, power, skill.ManaCost);
        }
    }
}
