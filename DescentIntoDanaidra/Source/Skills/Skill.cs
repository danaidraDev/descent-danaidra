﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Skill : IInventorable, IUsable, ISaveable
    {
        public Skill(SkillType type, int level)
        {
            Type = type;
            TypeName = "Skill";
            Level = new Stat("Level", level, type.MaxLevel);
            IsConsumable = false;
        }

        public Skill(SkillType type)
            : this(type, 1)
        {
        }

        public SkillType Type { get; private set; }
        public string Name { get { return Type.Name; } }
        public string TypeName { get; }
        public string Description { get { return Type.Description; } }
        public string PropertyLine1 { get { return "Mana cost: " + Type.ManaCost; } }
        public string PropertyLine2 { get { return "Max level: " + Type.MaxLevel; } }
        public string PropertyLine3 { get { return ""; } }
        public Texture2D Texture { get { return Type.Texture; } }
        public Stat Level { get; private set; }
        public virtual int ManaCost { get { return Type.ManaCost; } }
        public bool IsConsumable { get; }

        public bool HasEnoughMana(IFighter user)
        {
            return user.Mana.Current >= ManaCost;
        }

        public virtual bool CanUse(IFighter user)
        {
            if (!HasEnoughMana(user))
            {
                HUD.MessageLog.DisplayMessage("You do not have enough mana.", Colors.Orange);
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool Use(Player player)
        {
            if (CanUse(player))
            {
                Type.Effect(player, this);
                if (Type.TakesTurn)
                {
                    player.Mana.Current -= ManaCost;
                    return true;
                }
            }
            return false;
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            writer.WriteAttributeString("Level", Level.Current.ToString());
            writer.WriteEndElement();
        }

        public static Skill Load(XmlReader reader, ObjectParser parser)
        {
            string name = reader["Name"];
            string level = reader["Level"];
            if (name != null && level != null && int.TryParse(level, out int parsedLevel))
            {
                return new Skill(parser.SkilleTypeList.GetFromDictionary(name), parsedLevel);
            }
            return null;
        }
    }
}
