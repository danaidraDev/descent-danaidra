﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Monster : Creature, IFighter, ILightSource
    {
        public Monster(Location location, Point position, MonsterType type)
            : base (location, position)
        {
            Type = type;

            Statuses = new List<Status>();

            Level = new Stat("Level", 1, 1);
            Experience = new Stat("Experience", 0, 0);
            Vitality = new Stat("Vitality", Type.Vitality, 100);
            Magic = new Stat("Magic", Type.Magic, 100);
            Strength = new Stat("Strength", Type.Strength, 100);
            Focus = new Stat("Focus", Type.Focus, 100);
            Health = new Stat("Health", Type.Vitality * 5);
            Mana = new Stat("Magic", Type.Magic * 5);
            
            Light = new Light(0.3f, 2f, Color.CadetBlue);

            Sleep = new Stat("Sleep", 0);
            Food = new Stat("Food", 0);
        }

        public Monster(Location location, Point position, MonsterType type, MonsterSpawn spawn, int walkAround)
            : this(location, position, type)
        {
            Spawn = spawn;
            WalkAround = walkAround;
        }
        
        public MonsterType Type { get; private set; }
        public override string Name { get { return Type.Name; } }
        public Weapon EquippedWeapon { get; }
        public Armor EquippedArmor { get; }
        public Stat Vitality { get; private set; }
        public Stat Magic { get; private set; }
        public Stat Strength { get; private set; }
        public Stat Focus { get; private set; }
        public int HealthRegen { get { return 1 + (Vitality.Current + Strength.Current) / 4; } }
        public int ManaRegen { get { return 1 + (Magic.Current + Focus.Current) / 4; } }
        public Stat Health { get; private set; }
        public Stat Mana { get; private set; }
        public int BaseDamage { get { return Strength.Current; } }
        public int BaseDefense { get { return 0; } }
        public int DefenseBonus { get { return 0; } }
        public int DamageBonus { get { return 0; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public Stat Level { get; private set; }
        public Stat Experience { get; private set; }
        public MonsterSpawn Spawn { get; private set; }
        public int WalkAround { get; private set; }
        public List<Status> Statuses { get; private set; }
        public bool IsSpooked { get; set; }
        public Light Light { get; private set; }
        public Stat Sleep { get; private set; }
        public Stat Food { get; private set; }
        public int ExpGained { get { return Type.ExpGained; } }

        public override void Update(Player player)
        {
            Health.Regen(HealthRegen, 10);
            Mana.Regen(ManaRegen, 3);
            UpdateStatuses();
            MonsterAI.Behave(this, player);
        }

        public override bool OnCollision(Player player)
        {
            player.Attack(this);
            return true;
        }

        public void Die(IFighter attacker)
        {
            Die();
        }

        public void Die()
        {
            IsDead = true;
            DropLoot();
            HUD.MessageLog.DisplayMessage(Name + " dies.");
            Location.RemoveCreature(this);
        }

        public void UpdateStatuses()
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (Statuses[i].TurnsElapsed >= Statuses[i].Lasts)
                {
                    Statuses.RemoveAt(i);
                    i--;
                }
                else
                {
                    Statuses[i].Update(this);
                }
            }
        }

        public void AddStatus(Status status)
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (Statuses[i].Type == status.Type)
                {
                    Statuses.RemoveAt(i);
                    i--;
                }
            }
            Statuses.Add(status);
        }

        public void IncreaseExp(int amount)
        {
        }

        public void Kill(IFighter target)
        {
        }

        public void CheckLevelUp()
        {
            if (Experience.Current >= Experience.Max)
            {
                LevelUp();
            }
        }

        public void LevelUp()
        {
        }

        private void DropLoot()
        {
            Point? pos = NearestFreeToDrop(2);
            int typicalDropChance = 20;
            int randomDropChance = 80;

            if (pos != null)
            {
                if (Type.TypicalDrops != null && Type.TypicalDrops.Length > 0 && Randomiser.PassPercentileRoll(typicalDropChance))
                {
                    Type.TypicalDrops[Randomiser.RandomNumber(0, Type.TypicalDrops.Length - 1)].Clone(Location, pos.GetValueOrDefault());
                }
                else if (Randomiser.PassPercentileRoll(randomDropChance))
                {
                    Loot.RandomLoot(Location, pos.GetValueOrDefault(), Type.Difficulty);
                }
            }
        }
    }
}
