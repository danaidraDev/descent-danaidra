﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class MonsterTypeList
    {
        private StatusTypeList statuses;
        private FoodTypeList foods;

        public MonsterTypeList(ContentManager content, StatusTypeList statusTypes, FoodTypeList foodTypes)
        {
            statuses = statusTypes;
            foods = foodTypes;

            Slime = new MonsterType("Slime", content.Load<Texture2D>("monsters/slime"))
            {
                Vitality = 3,
                Magic = 0,
                Strength = 2,
                Focus = 2,
                DetectRange = 4,
                TypicalDrops = new Food[] {
                    new Food(null, Point.Zero, foodTypes.Fish),
                    new Food(null, Point.Zero, foodTypes.Peas),
                },
            };

            Homunculus = new MonsterType("Homunculus", content.Load<Texture2D>("monsters/homunculus"))
            {
                Vitality = 4,
                Magic = 0,
                Strength = 1,
                Focus = 2,
                DetectRange = 4,
                AttackEffect = CausePoisoning,
            };

            Pest = new MonsterType("Pest", content.Load<Texture2D>("monsters/pest"))
            {
                Vitality = 4,
                Magic = 0,
                Strength = 5,
                Focus = 2,
                DetectRange = 4,
            };

            Ghost = new MonsterType("Ghost", content.Load<Texture2D>("monsters/ghost"))
            {
                Vitality = 4,
                Magic = 0,
                Strength = 2,
                Focus = 5,
                DetectRange = 4,
            };

            Lizard = new MonsterType("Lizard", content.Load<Texture2D>("monsters/lizard"))
            {
                Vitality = 4,
                Magic = 0,
                Strength = 4,
                Focus = 1,
                DetectRange = 5,
            };

            Abomination = new MonsterType("Abomination", content.Load<Texture2D>("monsters/abomination"))
            {
                Vitality = 5,
                Magic = 0,
                Strength = 5,
                Focus = 1,
                DetectRange = 5,
            };

            ScorchedOne = new MonsterType("Scorched One", content.Load<Texture2D>("monsters/scorchedOne"))
            {
                Vitality = 6,
                Magic = 0,
                Strength = 7,
                Focus = 3,
                DetectRange = 5,
            };

            Imp = new MonsterType("Imp", content.Load<Texture2D>("monsters/imp"))
            {
                Vitality = 6,
                Magic = 0,
                Strength = 7,
                Focus = 4,
                DetectRange = 5,
            };

            Crustacean = new MonsterType("Crustacean", content.Load<Texture2D>("monsters/crustacean"))
            {
                Vitality = 5,
                Magic = 0,
                Strength = 5,
                Focus = 3,
                DetectRange = 5,
            };

            Deathbot = new MonsterType("Deathbot", content.Load<Texture2D>("monsters/deathbot"))
            {
                Vitality = 10,
                Magic = 0,
                Strength = 13,
                Focus = 5,
                DetectRange = 7,
            };
            
            Dictionary = new Dictionary<string, MonsterType>()
            {
                { "slime", Slime },
                { "pest", Pest },
                { "ghost", Ghost },
                { "lizard", Lizard },
                { "abomination", Abomination },
                { "homunculus", Homunculus },
                { "scorchedOne", ScorchedOne },
                { "crustacean", Crustacean },
                { "imp", Imp },
                { "deathbot", Deathbot },
            };

            All = new List<MonsterType>();
            All.AddRange(Dictionary.Values);
        }

        public MonsterType Slime { get; }
        public MonsterType Pest { get; }
        public MonsterType Ghost { get;}
        public MonsterType Lizard { get; }
        public MonsterType Abomination { get; }
        public MonsterType Homunculus { get; }
        public MonsterType ScorchedOne { get; }
        public MonsterType Crustacean { get; }
        public MonsterType Imp { get; }
        public MonsterType Deathbot { get; }
        public Dictionary<string, MonsterType> Dictionary { get; }
        public List<MonsterType> All { get; }

        public MonsterType GetRandomType(int difficultyFactor)
        {
            List<MonsterType> possibleTypes = new List<MonsterType>();

            int random = Randomiser.RandomNumber(All.Min(i => i.Difficulty), All.Max(i => i.Difficulty));
            for (int i = 0; i < All.Count(); i++)
            {
                if (random + difficultyFactor >= All[i].Difficulty)
                {
                    possibleTypes.Add(All[i]);
                }
            }

            if (possibleTypes.Any())
            {
                return possibleTypes[Randomiser.RandomNumber(0, possibleTypes.Count - 1)];
            }
            else return All[0];
        }

        public void CausePoisoning(IFighter attacker, IFighter target)
        {
            target.AddStatus(new Status(statuses.Poisoning, 25));
        }
    }
}
