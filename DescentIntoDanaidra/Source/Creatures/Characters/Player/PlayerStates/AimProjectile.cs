﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class AimProjectile : PlayerState
    {
        private readonly List<DirectionCue> cues;

        public AimProjectile(ProjectileType type, int lasts, int power, int manaCost, Player player, Camera camera)
        {
            Type = type;
            Lasts = lasts;
            Power = power;
            ManaCost = manaCost;

            Point playerPos = player.Position.GetValueOrDefault();
            cues = new List<DirectionCue>()
            {
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X - 1, playerPos.Y - 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()),  "7"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X, playerPos.Y - 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "8"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X + 1, playerPos.Y - 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "9"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X - 1, playerPos.Y), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "4"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X, playerPos.Y), camera),
                UIStateHandler.DefaultFont.MeasureString("Cancel").ToPoint()), "Cancel"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X + 1, playerPos.Y), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "6"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X - 1, playerPos.Y + 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "1"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X, playerPos.Y + 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "2"),
                new DirectionCue(Helper.CenterWithinTile(Helper.MapPositionToScreen(new Point(playerPos.X + 1, playerPos.Y + 1), camera),
                UIStateHandler.DefaultFont.MeasureString("0").ToPoint()), "3"),
            };
        }

        public ProjectileType Type { get; }
        public int Lasts { get; }
        public int Power { get; }
        public int ManaCost { get; }

        public override void Close()
        {
            End = true;
        }

        public override void Draw(SpriteBatch spriteBatch, Player player, Camera camera)
        {
            foreach (DirectionCue c in cues)
            {
                Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, c.Cue, c.Position, Colors.FontHoverColor);
            }
        }

        private void DisplayDirectionCue(Point position, string cue, SpriteBatch spriteBatch, Camera camera)
        {
            Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, cue, Helper.MapPositionToScreen(position, camera));
        }

        public override int Update(InputHandler input, GameTime gameTime, Player player)
        {
            if (KeyBinds.UpLeftKeyHit(input))
            {
                return CastProjectile(player, new Vector2(-1, -1));
            }
            else if (KeyBinds.UpKeyHit(input))
            {
                return CastProjectile(player, new Vector2(0, -1));
            }
            else if (KeyBinds.UpRightKeyHit(input))
            {
                return CastProjectile(player, new Vector2(1, -1));
            }
            else if (KeyBinds.LeftKeyHit(input))
            {
                return CastProjectile(player, new Vector2(-1, 0));
            }
            else if (KeyBinds.WaitKeyHit(input))
            {
                Close();
            }
            else if (KeyBinds.RightKeyHit(input))
            {
                return CastProjectile(player, new Vector2(1, 0));
            }
            else if (KeyBinds.DownLeftKeyHit(input))
            {
                return CastProjectile(player, new Vector2(-1, 1));
            }
            else if (KeyBinds.DownKeyHit(input))
            {
                return CastProjectile(player, new Vector2(0, 1));
            }
            else if (KeyBinds.DownRightKeyHit(input))
            {
                return CastProjectile(player, new Vector2(1, 1));
            }
            return 0;
        }

        private int CastProjectile(Player player, Vector2 direction)
        {
            new Projectile(player, player.Location, new Point(player.Position.GetValueOrDefault().X + (int)direction.X,
                player.Position.GetValueOrDefault().Y + (int)direction.Y), Type, direction, Lasts, Power);
            player.Mana.Current -= ManaCost;
            SoundHandler.PlaySound(Type.ThrowSound);
            HUD.MessageLog.DisplayMessage(player.Name + " throws " + Type.Name + ".");
            Close();
            return 1;
        }

        struct DirectionCue
        {
            public DirectionCue(Point position, string cue)
            {
                Position = position;
                Cue = cue;
            }
            public Point Position { get; }
            public string Cue { get; }
        }
    }
}
