﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class NormalState : PlayerState
    {
        private readonly UIStateHandler ui;

        public NormalState(UIStateHandler ui)
        {
            this.ui = ui;
            StepTimer = new Timer();
            PressDelay = new Timer();
        }

        public Timer StepTimer { get; private set; }
        public bool StepTimerActive { get; private set; }
        public Timer PressDelay { get; private set; }
        public bool PressDelayActive { get; private set; }

        public override int Update(InputHandler input, GameTime gameTime, Player player)
        {
            int turnsConsumed = 0;

            if (input.AnyKeyPressed() && !StepTimerActive && !PressDelayActive)
            {
                PressDelayActive = true;
                turnsConsumed = CheckKeys(input, player);
            }
            else if (PressDelayActive && PressDelay.Tick(240, gameTime))
            {
                StepTimerActive = true;
                StepTimer.Reset();
            }
            else if (!input.AnyKeyPressed())
            {
                PressDelay.Reset();
                StepTimer.Reset();
                StepTimerActive = false;
                PressDelayActive = false;
            }
            else if (StepTimerActive && StepTimer.Tick(100, gameTime))
            {
                turnsConsumed = CheckKeys(input, player);
            }

            return turnsConsumed;
        }

        public override void Draw(SpriteBatch spriteBatch, Player player, Camera camera)
        {
        }

        private int CheckKeys(InputHandler input, Player player)
        {
            int turnsConsumed = 0;

            Point pos = player.Position.GetValueOrDefault();

            if (KeyBinds.UpKeyPressed(input))
            {
                if (player.Move(new Point(pos.X, pos.Y - 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.DownKeyPressed(input))
            {
                if (player.Move(new Point(pos.X, pos.Y + 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.LeftKeyPressed(input))
            {
                if (player.Move(new Point(pos.X - 1, pos.Y)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.RightKeyPressed(input))
            {
                if (player.Move(new Point(pos.X + 1, pos.Y)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.UpLeftKeyPressed(input))
            {
                if (player.Move(new Point(pos.X - 1, pos.Y - 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.UpRightKeyPressed(input))
            {
                if (player.Move(new Point(pos.X + 1, pos.Y - 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.DownLeftKeyPressed(input))
            {
                if (player.Move(new Point(pos.X - 1, pos.Y + 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.DownRightKeyPressed(input))
            {
                if (player.Move(new Point(pos.X + 1, pos.Y + 1)))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.PickUpKeyPressed(input))
            {
                if (player.Location.Tilemap.GetItem(pos) is Item item && player.TryTakeItem(item))
                {
                    turnsConsumed++;
                }
            }
            else if (KeyBinds.DetailsKeyPressed(input))
            {
                if (player.Location.Tilemap.GetItem(pos) is Item item)
                {
                    ui.OpenInventorableDetails(item);
                }
            }
            else if (KeyBinds.WaitKeyPressed(input))
            {
                turnsConsumed++;
            }
            else
            {
                turnsConsumed = CheckQuickuses(input, player, turnsConsumed);
            }
            return turnsConsumed;
        }

        private static int CheckQuickuses(InputHandler input, Player player, int turnsConsumed)
        {
            int i = 0;
            bool used = false;
            while (i < player.QuickUses.Length && !used)
            {
                if (input.KeyHit(Keys.D1 + i) && player.QuickUses[i] != null && player.QuickUses[i] is IUsable usable)
                {
                    if (usable.Use(player))
                    {
                        turnsConsumed++;
                        if (usable is Item item && usable.IsConsumable)
                        {
                            player.Equipment.Remove(item);
                            player.QuickUses[i] = null;
                            i--;
                        }
                    }
                    used = true;
                }
                i++;
            }

            return turnsConsumed;
        }

        public override void Close()
        {
        }
    }
}
