﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra
{
    public class PlayerClass
    {
        public PlayerClass(string name, string textureFile)
        {
            Name = name;
            TextureFile = textureFile;
            Description = "No description.";
            StartingEquipment = new List<Item>();
            StartingSkills = new List<Skill>();
        }

        public string Name { get; private set; }
        public string Description { get; set; }
        public Texture2D Texture { get; set; }
        public string TextureFile { get; private set; }
        public int Vitality { get; set; }
        public int Magic { get; set; }
        public int Strength { get; set; }
        public int Focus { get; set; }
        public int VitalityGain { get; set; }
        public int MagicGain { get; set; }
        public int StrengthGain { get; set; }
        public int FocusGain { get; set; }
        public List<Item> StartingEquipment { get; set; }
        public List<Skill> StartingSkills { get; set; }
    }
}
