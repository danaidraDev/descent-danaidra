﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class NPCTypeList
    {
        private readonly CharacterAI characterAI;
        private readonly GuardAI guardAI;
        private readonly MercenaryAI companionAI;

        public NPCTypeList(ContentManager content, FoodTypeList foodTypes, ArmorTypeList armorTypes, ArmorModifierList armorModifiers,
            WeaponTypeList weaponTypes, WeaponModifierList weaponModifiers, PotionTypeList potionTypes, BookTypeList bookTypes)
        {
            characterAI = new CharacterAI();
            guardAI = new GuardAI();
            companionAI = new MercenaryAI();

            Guard = new NPCType("Guard", content.Load<Texture2D>("characters/guard"))
            {
                AI = guardAI,
                OpeningLines = new List<string>()
                {
                    "Keeping guard of the city is an important job, please don't distract me...",
                },
            };

            Merchant = new NPCType("Merchant", content.Load<Texture2D>("characters/discoMan"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Yeah?",
                },
                DialogueLines = new List<string>()
                {
                    "The dungeons in and around this city are filled with precious loot.",
                    "If you find anything that seems like it might be of value, bring it to me, and I promise I'll offer you a good deal.",
                },
                GoodbyeLines = new List<string>()
                {
                    "Stop by again when you can!",
                },
                SoldItems = new List<Item>()
                {
                    new Weapon(weaponTypes.Club, weaponModifiers.GetRandom()),
                    new Armor(armorTypes.Robe, armorModifiers.GetRandom()),
                    new Armor(armorTypes.Robe, armorModifiers.GetRandom()),
                    new Food(foodTypes.Fish),
                    new Food(foodTypes.Cabbage),
                    new Food(foodTypes.Meat),
                    new Food(foodTypes.Herbs),
                    new Food(foodTypes.Turnip),
                    new Food(foodTypes.Mushroom),
                },
            };

            Wizard = new NPCType("Wizard", content.Load<Texture2D>("characters/wizard"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Abracadabra!",
                    "I might turn you into a bug one day...",
                },
                DialogueLines = new List<string>()
                {
                    "Buy some potions from my store, you'll need them.",
                    "This world is filled with dangerous creatures, whose attacks can cause poisoning, bleeding or worse.",
                    "You can counteract many of these effects with the right potion.",
                },
                GoodbyeLines = new List<string>()
                {
                    "Make sure to come back with more money.",
                },
                SoldItems = new List<Item>()
                {
                    new Potion(potionTypes.HealthVial),
                    new Potion(potionTypes.HealthVial),
                    new Potion(potionTypes.HealthBottle),
                    new Potion(potionTypes.HealthBottle),
                    new Potion(potionTypes.ManaVial),
                    new Potion(potionTypes.ManaVial),
                    new Potion(potionTypes.ManaBottle),
                    new Potion(potionTypes.ManaBottle),
                    new Potion(potionTypes.Antidote),
                    new Potion(potionTypes.Antidote),
                    new Potion(potionTypes.CongealBlood),
                    new Potion(potionTypes.CongealBlood),
                },
            };

            Hermit = new NPCType("Hermit", content.Load<Texture2D>("characters/hermit"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    ". . .",
                },
                DialogueLines = new List<string>()
                {
                    "*Monk seems completely lost in thought*",
                },
            };

            Blacksmith = new NPCType("Blacksmith", content.Load<Texture2D>("characters/blacksmith"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Came to buy some armor!?",
                },
                DialogueLines = new List<string>()
                {
                    "Do you like my armor?",
                    "Man I love armor, makes me feel so safe. I wear it all the time, even in bed.",
                    "My skin itches pretty bad! HAha!",
                },
                GoodbyeLines = new List<string>()
                {
                    "Be safe! Remember to wear armor always!",
                },
                SoldItems = new List<Item>()
                {
                    new Armor(armorTypes.GetRandom(), armorModifiers.GetRandom()),
                    new Armor(armorTypes.GetRandom(), armorModifiers.GetRandom()),
                    new Armor(armorTypes.GetRandom(), armorModifiers.GetRandom()),
                    new Armor(armorTypes.GetRandom(), armorModifiers.GetRandom()),
                    new Weapon(weaponTypes.GetRandom(), weaponModifiers.GetRandom()),
                    new Weapon(weaponTypes.GetRandom(), weaponModifiers.GetRandom()),
                    new Weapon(weaponTypes.GetRandom(), weaponModifiers.GetRandom()),
                    new Weapon(weaponTypes.GetRandom(), weaponModifiers.GetRandom()),
                },
            };

            Guildmaster = new NPCType("Guildmaster", content.Load<Texture2D>("characters/guildmaster"))
            {
                AI = guardAI,
                OpeningLines = new List<string>()
                {
                    "What is it?",
                },
                DialogueLines = new List<string>()
                {
                    "As the Guildmaster I also have the role of the Captain of Guards.",
                    "Keeping monsters away from the city is all we do these days. It's a tough job, so make sure not to get in my guards' way. ",
                },
                GoodbyeLines = new List<string>()
                {
                    "Yeah, take care.",
                },
            };

            Priestess = new NPCType("Priestess", content.Load<Texture2D>("characters/priestess"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Greetings!",
                },
                DialogueLines = new List<string>()
                {
                    "If you are ever poisoned or wounded, you may always pray at this shrine.",
                    "The blessing of our divine mother, goddess Ahtara, is known to restore health to those who are ailing.",
                    "You can find altars like this one in and around the city, and pray at them when you are in need.",
                    "There is a chance your prayers will be answered if you provide a small, emm... offering.",
                },
                GoodbyeLines = new List<string>()
                {
                    "Be well, adventurer.",
                },
            };

            Mermaid = new NPCType("Mermaid", content.Load<Texture2D>("characters/mermaid"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Hmm?",
                },
                DialogueLines = new List<string>()
                {
                    "What are mermaids doing here you ask?",
                    "Well, this is a public bath, anyone can use it.",
                    "We spend most of our time here. The steamy, hot air does wonders for skin and scales.",
                    "Be careful though, the floor is slippery!",
                },
                GoodbyeLines = new List<string>()
                {
                    "Come hang out some time!",
                },
            };

            BathingMermaid = new NPCType("Mermaid", content.Load<Texture2D>("characters/bathingMermaid"))
            {
            };

            Dev = new NPCType("", content.Load<Texture2D>("characters/danaidraDev"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Oi! This is me, the dev.",
                    "Are you having enough fun playing my game? Huh?",
                    "If you want to complain about some bug, you can go to danaidradev.itch.io and leave me a comment.",
                    "If you have a suggestion for a feature, let me know as well. Who knows, I might add it in the next update.",
                },
            };

            Cook = new NPCType("Cook", content.Load<Texture2D>("characters/cook"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Mmm, this is gonna be delicious!",
                },
            };

            Waiter = new NPCType("Waiter", content.Load<Texture2D>("characters/waiter"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "What can I do for you?",
                },
                DialogueLines = new List<string>()
                {
                    "Women who come to this inn often stare at me.",
                    "I wonder if my face looks weird?",
                },
                GoodbyeLines = new List<string>()
                {
                    "Have a pleasant day.",
                },
                SoldItems = new List<Item>()
                {
                    new Food(foodTypes.Fish),
                    new Food(foodTypes.Meat),
                    new Food(foodTypes.Peas),
                    new Food(foodTypes.Herbs),
                    new Food(foodTypes.Turnip),
                    new Food(foodTypes.Mushroom),
                    new Food(foodTypes.Bread),
                    new Food(foodTypes.Cheese),
                },
            };

            Librarian = new NPCType("Librarian", content.Load<Texture2D>("characters/librarian"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Please keep quiet in the library.",
                },
                DialogueLines = new List<string>()
                {
                    "I wish I didn't have to mind these dusty books all day.",
                    "I just want to go to the inn and have the handsome waiter there serve me some wine. Is that too much to ask?",
                },
                GoodbyeLines = new List<string>()
                {
                    "What? Ah, yes, goodbye.",
                },
                SoldItems = new List<Item>()
                {
                    new Book(bookTypes.HealingBook),
                    new Book(bookTypes.FireballBook),
                },
            };

            Skeleton = new NPCType("Storage Master", content.Load<Texture2D>("characters/skeledude"))
            {
                AI = characterAI,
                OpeningLines = new List<string>()
                {
                    "Boy, thanks for finding me.",
                    "I started moving stuff around and got stuck in my own storage house.",
                    "What year is this?",
                },
                DialogueLines = new List<string>()
                {
                    "When I moved one of the barrels I found this interesting chest.",
                    "Whatever's inside, you can take it. I think it's no use to me anymore.",
                },
                GoodbyeLines = new List<string>()
                {
                    "Dang these barrels.",
                },
            };

            Frank = new NPCType("", content.Load<Texture2D>("characters/frank"))
            {
                AI = companionAI,
                Strength = 10,
                Magic = 0,
                Vitality = 10,
                Focus = 10,
            };

            Dictionary = new Dictionary<string, NPCType>()
            {
                { "guard", Guard },
                { "merchant", Merchant },
                { "wizard", Wizard },
                { "hermit", Hermit },
                { "blacksmith", Blacksmith },
                { "guildmaster", Guildmaster },
                { "priestess", Priestess },
                { "mermaid", Mermaid },
                { "bathingMermaid", BathingMermaid },
                { "dev", Dev },
                { "cook", Cook },
                { "waiter", Waiter },
                { "librarian", Librarian },
                { "skeleton", Skeleton },
                { "frank", Frank },
            };
        }

        public Dictionary<string, NPCType> Dictionary { get; }
        public NPCType Guard { get; private set; }
        public NPCType Merchant { get; private set; }
        public NPCType Wizard { get; private set; }
        public NPCType Hermit { get; private set; }
        public NPCType Blacksmith { get; private set; }
        public NPCType Guildmaster { get; private set; }
        public NPCType Priestess { get; private set; }
        public NPCType Mermaid { get; private set; }
        public NPCType BathingMermaid { get; private set; }
        public NPCType Dev { get; private set; }
        public NPCType Cook { get; private set; }
        public NPCType Waiter { get; }
        public NPCType Librarian { get; }
        public NPCType Skeleton { get; }
        public NPCType Frank { get; }
    }
}
