﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class Particle
    {
        public Particle(int maxMs, Texture2D texture, Point position, Color color)
        {
            Milliseconds = new Stat("Turns", 0, maxMs);
            Texture = texture;
            Position = position;
            Color = color;
        }

        public Stat Milliseconds { get; private set; }
        public Texture2D Texture { get; private set; }
        public Point Position { get; private set; }
        public Color Color { get; private set; }

        public void Update(GameTime gameTime)
        {
            Milliseconds.Current += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position.ToVector2(), Color);
        }
    }
}
