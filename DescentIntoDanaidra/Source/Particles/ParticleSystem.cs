﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DescentIntoDanaidra
{
    public class ParticleSystem
    {
        private readonly List<Particle> particles = new List<Particle>();

        public ParticleSystem()
        {
        }

        public void AddParticle(Particle particle)
        {
            if (particles.Count < 100)
            {
                particles.Add(particle);
            }
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                Particle p = particles[i];

                if (p.Milliseconds.Current >= p.Milliseconds.Max)
                {
                    particles.RemoveAt(i);
                    i--;
                }
                else
                {
                    p.Update(gameTime);
                }
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                particles[i].Draw(spriteBatch);
            }
        }
    }
}
