﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace DescentIntoDanaidra
{
    public class Camera
    {
        private Point targetPosition;

        public Camera(Location location, Point position, Viewport viewport)
        {
            Viewport = viewport;
            Location = location;
            Position = position;
        }
        
        public Viewport Viewport { get; private set; }
        public Location Location { get; private set; }
        public Point Position { get; private set; }
        
        public Matrix Transform()
        {
            return Matrix.CreateTranslation(-Position.X, -Position.Y, 0);
        }

        public Rectangle Frame()
        {
            return new Rectangle(Position, new Point(Viewport.Width, Viewport.Height));
        }

        public Rectangle FrameInTiles()
        {
            return new Rectangle(Helper.SizeInTiles(Position.X), Helper.SizeInTiles(Position.Y),
                Helper.SizeInTiles(Viewport.Width), Helper.SizeInTiles(Viewport.Height));
        }

        public void SnapToObject(IMapObject mapObject)
        {
            Location = mapObject.Location;
            targetPosition = MindLocationBounds(CenteredToObject(mapObject));
            Position = targetPosition;
        }
        
        public void UpdateByTurn(Player player)
        {
            SnapToObject(player);
        }

        private Point MindLocationBounds(Point position)
        {
            int xPos = position.X;
            int yPos = position.Y;

            int locationWidth = Helper.SizeInPixels(Location.Tilemap.Width);
            if (xPos < 0)
            {
                xPos = 0;
            }
            else if (xPos + Viewport.Width > locationWidth)
            {
                xPos = locationWidth - Viewport.Width;
            }

            int locationHeight = Helper.SizeInPixels(Location.Tilemap.Height);
            if (yPos < 0)
            {
                yPos = 0;
            }
            else if (yPos + Viewport.Height > locationHeight)
            {
                yPos = locationHeight - Viewport.Height;
            }

            return new Point(xPos, yPos);
        }

        private Point CenteredToObject(IMapObject mapObject)
        {
            int xOffset = Game1.TileSize / 2 - Viewport.Width / 2;
            int yOffset = Game1.TileSize / 2 - Viewport.Height / 2;

            return new Point(Helper.SizeInPixels(mapObject.Position.GetValueOrDefault().X) + xOffset,
                Helper.SizeInPixels(mapObject.Position.GetValueOrDefault().Y) + yOffset);
        }
    }
}
