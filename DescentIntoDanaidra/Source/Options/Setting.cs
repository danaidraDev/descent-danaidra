﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Setting
    {
        public Setting(string name, Func<string> displayedValue, Action leftPressed, Action rightPressed)
        {
            Name = name;
            DisplayedValue = displayedValue;
            LeftPressed = leftPressed;
            RightPressed = rightPressed;
        }

        public string Name { get; private set; }
        public Func<string> DisplayedValue { get; private set; }
        public Action LeftPressed { get; private set; }
        public Action RightPressed { get; private set; }
    }
}
