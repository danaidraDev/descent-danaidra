﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Options
    {
        public Options()
        {
            InGameSettings = new List<Setting>()
            {
                new Setting("Sfx volume", new Func<string>(() => { return (Math.Round(SoundHandler.SfxVolume * 10) * 10).ToString(); }),
                    new Action(() => { SoundHandler.DecreaseSfxVolume(); SoundHandler.PlaySound(SoundLibrary.Shimmer); }),
                    new Action(() => { SoundHandler.IncreaseSfxVolume(); SoundHandler.PlaySound(SoundLibrary.Shimmer); })),
                new Setting("Music volume", new Func<string>(() => { return (Math.Round(SoundHandler.MusicVolume * 10) * 10).ToString(); }),
                    new Action(() => SoundHandler.DecreaseMusicVolume()), new Action(() => SoundHandler.IncreaseMusicVolume())),
            };
        }

        public static List<Setting> InGameSettings { get; private set; }
    }
}
