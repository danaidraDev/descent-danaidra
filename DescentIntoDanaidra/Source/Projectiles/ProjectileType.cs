﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ProjectileType
    {
        public ProjectileType(string name, Texture2D texture, Action<Player, Projectile, Monster> hitEffect, Action<Player, Projectile> endEffect)
        {
            Name = name;
            Texture = texture;
            HitEffect = hitEffect;
            EndEffect = endEffect;
        }

        public string Name { get; }
        public Texture2D Texture { get; }
        public Action<Player, Projectile, Monster> HitEffect { get; }
        public Action<Player, Projectile> EndEffect { get; }
        public SoundEffect ThrowSound { get; set; }
        public SoundEffect HitSound { get; set; }
        public Light Light { get; set; }
    }
}
