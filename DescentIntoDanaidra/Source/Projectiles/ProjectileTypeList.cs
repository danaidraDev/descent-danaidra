﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ProjectileTypeList
    {
        public ProjectileTypeList(ContentManager content)
        {
            Fireball = new ProjectileType("Fireball", content.Load<Texture2D>("projectiles/fireball"), FireballHitEffect, null)
            {
                ThrowSound = SoundLibrary.PageTurnB,
                HitSound = SoundLibrary.PageTurnB,
                Light = new Light(0.75f, 2.5f, Color.OrangeRed),
            };
        }

        public ProjectileType Fireball { get; }
        void FireballHitEffect(Player player, Projectile projectile, Monster monster)
        {
            int damage = projectile.Power;
            Combat.SpellHit(player, monster, projectile.Name, damage);
        }
    }
}
