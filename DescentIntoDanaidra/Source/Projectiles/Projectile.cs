﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Projectile : IMapObject, ILightSource
    {
        private int age;

        public Projectile(Player player, Location location, Point position, ProjectileType type, Vector2 direction, int lasts, int power)
        {
            location.Projectiles.Add(this);
            Location = location;
            Position = position;
            Type = type;
            Direction = direction;
            Lasts = lasts;
            Power = power;
            age = 0;
            CheckCollisions(position, player);
        }

        public ProjectileType Type { get; private set; }
        public Vector2 Direction { get; set; }
        public int Lasts { get; set; }
        public int Power { get; set; }
        public string Name { get { return Type.Name; } }
        public Texture2D Texture { get { return Type.Texture; } }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public bool IsDead { get; set; }
        public Light Light { get { return Type.Light; } }

        public bool OnCollision(Player player)
        {
            return false;
        }

        public void Update(Player player)
        {
            CheckCollisions(Position.GetValueOrDefault(), player);
            if (age > 0 && age < Lasts)
            {
                CheckCollisions(new Point(Position.GetValueOrDefault().X + (int)Direction.X,
                    Position.GetValueOrDefault().Y + (int)Direction.Y), player);
            }
            else if (age >= Lasts)
            {
                EndProjectile(player);
            }
            age++;
        }

        private void EndProjectile(Player player)
        {
            Type.EndEffect?.Invoke(player, this);
            Location.Projectiles.Remove(this);
        }

        private void CheckCollisions(Point nextPosition, Player player)
        {
            if (Location.Tilemap.IsWalkable(nextPosition) &&
                Location.Tilemap.GetObject(nextPosition) != null &&
                Location.Tilemap.GetObject(nextPosition) is Monster monster)
            {
                HitTarget(player, monster);
            }
            else if (!Location.Tilemap.IsWalkable(nextPosition) ||
                (Location.Tilemap.GetObject(nextPosition) != null &&
                Location.Tilemap.GetObject(nextPosition) is Obstacle))
            {
                EndProjectile(player);
            }
            else
            {
                Position = nextPosition;
            }
        }

        private void HitTarget(Player player, Monster monster)
        {
            Type.HitEffect?.Invoke(player, this, monster);
            SoundHandler.PlaySound(Type.HitSound);
            EndProjectile(player);
        }
    }
}
