﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class WeaponType : ILootableType
    {
        public WeaponType(string name, Texture2D texture)
        {
            Name = name;
            Texture = texture;
        }

        public string Name { get; private set; }
        public string Description { get; set; }
        public Texture2D Texture { get; private set; }
        public int Damage { get; set; }
        public int Value { get; set; }
        public int Weight { get; set; }
        public int Durability { get; set; }
        public int DropChance { get; set; }
    }
}
