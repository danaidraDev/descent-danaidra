﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Weapon : Item, ISaveable
    {
        public Weapon(Location location, Point? position, WeaponType type, WeaponModifier modifier)
            : base(location, position)
        {
            Type = type;
            Durability = new Stat("Durability", type.Durability);
            Damage = type.Damage;

            Modifier = modifier;
            Name = CreateName();
            Description = CreateDescription();
        }

        public Weapon(WeaponType type, WeaponModifier modifier)
            : this(null, null, type, modifier)
        {
        }

        public WeaponType Type { get; private set; }
        public WeaponModifier Modifier { get; }
        public override string Name { get; set; }
        public override string TypeName { get { return "Weapon"; } }
        public override string Description { get; set; }
        public override string PropertyLine1 { get { return "Damage: " + Damage; } }
        public override string PropertyLine2 { get { return "Durab.: " + Durability.Current + "/" + Durability.Max; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public override int Weight { get { return Type.Weight; } }
        public override int Value { get { return CalculateValue(); } }
        public Stat Durability { get; private set; }
        public int Damage { get; private set; }
        
        public void InflictEffects(IFighter target, IFighter attacker)
        {
            if (Modifier != null)
            {
                Modifier.AttackEffect?.Invoke(target, attacker);
            }
        }

        public override Item Clone(Location location, Point? position)
        {
            return new Weapon(location, position, Type, Modifier);
        }

        public string CreateName()
        {
            string name = Type.Name;

            if (Modifier != null)
            {
                if (Modifier.Name != "")
                {
                    name = Modifier.Name + " " + name;
                }
            }

            return Helper.CapitalizeFirstChar(name);
        }

        public string CreateDescription()
        {
            string description = Type.Description;

            if (Modifier != null)
            {
                if (Modifier.Description != "")
                {
                    description += " br " + Modifier.Description;
                }
            }
            description += " br ";
            description += " br " + "Damage: " + Damage;
            description += " br " + "Durab.: " + Durability.Current + "/" + Durability.Max;

            return description;
        }

        public int CalculateValue()
        {
            int value;

            value = Type.Value;

            if (Modifier != null)
            {
                value = (int)(value * (1 + Modifier.AddedValue));
            }
            return value * Durability.Current / Durability.Max;
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            if (Modifier != null)
            {
                writer.WriteAttributeString("Modifier", Modifier.Name);
            }
            writer.WriteEndElement();
        }

        public new static Weapon Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.WeaponTypeList.GetFromDictionary(reader["Name"]);
            var modifier = parser.WeaponModifierList.GetFromDictionary(reader["Modifier"]);

            if (type != null)
            {
                return new Weapon(type, modifier);
            }

            return null;
        }
    }
}
