﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class WeaponTypeList
    {
        public WeaponTypeList(ContentManager content)
        {
            Club = new WeaponType("Club", content.Load<Texture2D>("items/club"))
            {
                Description = "A heavy spiked cudgel.",
                Value = 24,
                Weight = 4,
                Damage = 1,
                Durability = 5,
                DropChance = 20,
            };

            Sword = new WeaponType("Sword", content.Load<Texture2D>("items/sword"))
            {
                Description = "A short sword.",
                Value = 50,
                Weight = 5,
                Damage = 3,
                Durability = 7,
                DropChance = 10,
            };

            Axe = new WeaponType("Axe", content.Load<Texture2D>("items/axe"))
            {
                Description = "Crude, heavy axe.",
                Value = 35,
                Weight = 6,
                Damage = 2,
                Durability = 7,
                DropChance = 15,
            };

            Dictionary = new Dictionary<string, WeaponType>()
            {
                { Club.Name.ToLower(), Club },
                { Sword.Name.ToLower(), Sword },
                { Axe.Name.ToLower(), Axe },
            };
        }

        public Dictionary<string, WeaponType> Dictionary { get; private set; }

        public WeaponType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, WeaponType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out WeaponType type))
                {
                    return type;
                }
            }
            return null;
        }

        public WeaponType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public WeaponType Club { get; private set; }
        public WeaponType Sword { get; private set; }
        public WeaponType Axe { get; private set; }
    }
}
