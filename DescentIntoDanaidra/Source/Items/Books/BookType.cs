﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class BookType : ILootableType
    {
        public BookType(string name, Texture2D texture, Func<Player, bool> onRead)
        {
            Name = name;
            Texture = texture;
            OnRead = onRead;
        }

        public string Name { get; private set; }
        public virtual string TypeName { get { return "Book"; } }
        public Texture2D Texture { get; private set; }
        public virtual string Description { get; set; }
        public int Weight { get { return 2; } }
        public int Value { get; set; }
        public Func<Player, bool> OnRead { get; private set; }
        public int DropChance { get; set; }

        public virtual void Read(Player player)
        {
            OnRead?.Invoke(player);
        }
    }
}