﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class SkillbookType : BookType
    {
        public SkillbookType(SkillType skillType, Texture2D texture)
            : base("Book of Learn " + skillType.Name, texture, null)
        {
            SkillType = skillType;
        }

        public SkillType SkillType { get; }
        public override string TypeName { get { return "Skillbook"; } }
        public override string Description { get { return GetDescription(); } }

        public string GetDescription()
        {
            string desc = "This book contains knowledge about the " + SkillType.Name + " skill.";
            desc += " br br ";
            desc += "Skill description:";
            desc += " br ";
            desc += SkillType.Description;
            return desc;
        }

        public override void Read(Player player)
        {
            player.AskLearnSkill(SkillType);
        }
    }
}
