﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ScrapTypeList
    {
        public ScrapTypeList(ContentManager content)
        {
            Rose = new ScrapType("Rose", content.Load<Texture2D>("items/rose"))
            {
                Description = "A sublime blossom.",
                Weight = 2,
                Value = 30,
                DropChance = 10,
            };

            LoveLetter = new ScrapType("Love letter", content.Load<Texture2D>("items/loveLetter"))
            {
                Description = "Sadly it's not addressed to you.",
                Weight = 2,
                Value = 30,
                DropChance = 10,
            };

            Dictionary = new Dictionary<string, ScrapType>()
            {
                { Rose.Name.ToLower(), Rose },
                { LoveLetter.Name.ToLower(), LoveLetter },
            };
        }

        public Dictionary<string, ScrapType> Dictionary { get; private set; }

        public ScrapType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, ScrapType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out ScrapType type))
                {
                    return type;
                }
            }
            return null;
        }

        public ScrapType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public ScrapType Rose { get; }
        public ScrapType LoveLetter { get; }
    }
}
