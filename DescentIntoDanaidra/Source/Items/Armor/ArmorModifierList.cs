﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ArmorModifierList
    {
        private readonly StatusTypeList statusTypes;

        public ArmorModifierList(StatusTypeList statusTypes)
        {
            this.statusTypes = statusTypes;

            Spiritual = new ArmorModifier("spiritual")
            {
                Description = "25% chance of restoring some of wearer's life.",
                Effect = SpiritualEffect,
                AddedValue = 0.20f,
            };

            Debilitating = new ArmorModifier("debilitating")
            {
                Description = "Drains some of wearer's mana when hit.",
                Effect = DebilitatingEffect,
                AddedValue = -0.10f,
            };

            Mirroring = new ArmorModifier("mirroring")
            {
                Description = "Reflects part of the damage back onto attacker.",
                Effect = OfMirrorsEffect,
                AddedValue = 0.24f,
            };

            Resilient = new ArmorModifier("resilient")
            {
                Description = "1% chance of self-repair when hit.",
                Effect = OfResilienceEffect,
                AddedValue = 0.24f,
            };

            Dictionary = new Dictionary<string, ArmorModifier>()
            {
                { Spiritual.Name.ToLower(), Spiritual },
                { Debilitating.Name.ToLower(), Debilitating },
                { Mirroring.Name.ToLower(), Mirroring },
                { Resilient.Name.ToLower(), Resilient },
            };
        }

        public Dictionary<string, ArmorModifier> Dictionary { get; private set; }

        public ArmorModifier GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, ArmorModifier> pair in Dictionary)
            {
                if (key != null && Dictionary.TryGetValue(key.ToLower(), out ArmorModifier type))
                {
                    return type;
                }
            }
            return null;
        }

        public ArmorModifier GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public ArmorModifier Spiritual { get; private set; }
        void SpiritualEffect(IFighter wearer, IFighter attacker)
        {
            if (Randomiser.PassPercentileRoll(25))
            {
                int restore = Randomiser.RandomNumber(1, 3);
                wearer.Health.Current += restore;
                HUD.MessageLog.DisplayMessage(wearer.Name + "'s " + wearer.EquippedArmor.Name + " restores " +
                    restore + " life to " + wearer.Name + ".", Colors.SteelGray);
            }
        }

        public ArmorModifier Debilitating { get; private set; }
        void DebilitatingEffect(IFighter wearer, IFighter attacker)
        {
            int drain = Randomiser.RandomNumber(1, 3);
            wearer.Mana.Current -= drain;
            HUD.MessageLog.DisplayMessage(wearer.Name + "'s " + wearer.EquippedArmor.Name + " drains " +
                drain + " mana from " + wearer.Name + ".", Colors.SteelGray);
        }

        public ArmorModifier Mirroring { get; private set; }
        void OfMirrorsEffect(IFighter wearer, IFighter attacker)
        {
            int damage = Combat.GetMinDamage(attacker);
            attacker.Health.Current -= damage;
            HUD.MessageLog.DisplayMessage(wearer.Name + "'s " + wearer.EquippedArmor.Name + " reflects " +
                damage + " damage onto " + attacker.Name + ".", Colors.SteelGray);
        }

        public ArmorModifier Resilient { get; private set; }
        void OfResilienceEffect(IFighter wearer, IFighter attacker)
        {
            if (Randomiser.PassPercentileRoll(1))
            {
                wearer.EquippedArmor.Durability.Current += 1;
                HUD.MessageLog.DisplayMessage(wearer.Name + "'s " + wearer.EquippedArmor.Name + " repairs itself.", Colors.SteelGray);
            }
        }
    }
}
