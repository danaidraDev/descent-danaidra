﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public class ArmorModifier
    {
        public ArmorModifier(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public Action<IFighter, IFighter> Effect { get; set; }
        public string Description { get; set; }
        public float AddedValue { get; set; }
    }
}
