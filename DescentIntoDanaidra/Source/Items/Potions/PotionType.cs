﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class PotionType : ILootableType
    {
        public PotionType(string name, Texture2D texture, int restore, Action<Player, int> onDrink)
        {
            Name = name;
            Texture = texture;
            Restore = restore;
            OnDrink = onDrink;
        }

        public string Name { get; private set; }
        public Texture2D Texture { get; private set; }
        public string Description { get; set; }
        public string PropertyLine1 { get; set; }
        public int Weight { get; set; }
        public int Value { get; set; }
        public int Restore { get; private set; }
        public Action<Player, int> OnDrink { get; private set; }
        public int DropChance { get; set; }
    }
}
