﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DescentIntoDanaidra
{
    public class FoodType : ILootableType
    {
        public FoodType(string name, Texture2D texture)
        {
            Name = name;
            Texture = texture;
        }

        public string Name { get; private set; }
        public string Description { get; set; }
        public Texture2D Texture { get; private set; }
        public int FillFood { get; set; }
        public StatusType EatingEffect { get; set; }
        public int EffectLasts { get; set; }
        public int Value { get; set; }
        public int Weight { get; set; }
        public int DropChance { get; set; }
    }
}
