﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Loot
    {
        private static WeaponModifierList weaponModifiers;
        private static ArmorModifierList armorModifiers;
        private static List<ILootableType> drops;

        public Loot(FoodTypeList food, PotionTypeList potion, ArmorTypeList armor, ArmorModifierList armorMods,
            WeaponTypeList weapon, WeaponModifierList weaponMods, BookTypeList books, ScrapTypeList scraps)
        {
            armorModifiers = armorMods;
            weaponModifiers = weaponMods;

            drops = new List<ILootableType>();
            drops.AddRange(food.Dictionary.Values);
            drops.AddRange(potion.Dictionary.Values);
            drops.AddRange(armor.Dictionary.Values);
            drops.AddRange(weapon.Dictionary.Values);
            drops.AddRange(books.Dictionary.Values);
            drops.AddRange(scraps.Dictionary.Values);
        }

        public static Item RandomLoot(Location location, Point? position, int rarityBonus)
        {
            Item loot = null;
            ILootableType type = GetRandomType(rarityBonus);
            
            if (type is FoodType foodType)
            {
                loot = new Food(location, position, foodType);
            }
            else if (type is PotionType potionType)
            {
                loot = new Potion(location, position, potionType);
            }
            else if (type is WeaponType weaponType)
            {
                loot = new Weapon(location, position, weaponType, weaponModifiers.GetRandom());
            }
            else if (type is ArmorType armorType)
            {
                loot = new Armor(location, position, armorType, armorModifiers.GetRandom());
            }
            else if (type is BookType bookType)
            {
                loot = new Book(location, position, bookType);
            }
            else if (type is ScrapType scrapType)
            {
                loot = new Scrap(location, position, scrapType);
            }

            return loot;
        }

        private static ILootableType GetRandomType(int rarityBonus)
        {
            List<ILootableType> possibleDrops = new List<ILootableType>();

            int random = Randomiser.RandomNumber(drops.Min(i => i.DropChance), drops.Max(i => i.DropChance));
            for (int i = 0; i < drops.Count(); i++)
            {
                if (random - rarityBonus <= drops[i].DropChance)
                {
                    possibleDrops.Add(drops[i]);
                }
            }

            if (possibleDrops.Any())
            {
                return possibleDrops[Randomiser.RandomNumber(0, possibleDrops.Count - 1)];
            }
            else return null;
        }

        public static Item RandomLoot(int rarityBonus)
        {
            return RandomLoot(null, null, rarityBonus);
        }
    }
}
