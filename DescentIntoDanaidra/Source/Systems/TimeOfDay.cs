﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class TimeOfDay
    {
        private static readonly Color naturalAmbientLight = Color.LightSteelBlue;
        private int time;
        public const int TurnsPerCycle = 1200;

        public TimeOfDay(int time)
        {
            this.Time = time;
        }
        
        public int Zenith { get { return TurnsPerCycle; } }
        public int Dawn { get { return TurnsPerCycle * 3 / 4; } }
        public int Night { get { return TurnsPerCycle * 2 / 4; } }
        public int Evening { get { return TurnsPerCycle * 1 / 4; } }
        public int Time
        {
            get { return time; }

            private set
            {
                if (value >= TurnsPerCycle)
                {
                    value -= TurnsPerCycle;
                }
                else if (value < 0)
                {
                    value = 0;
                }
                time = value;
            }
        }
        public event EventHandler NightComes;
        public event EventHandler DawnComes;
        public event EventHandler EveningComes;
        public event EventHandler DayBegins;
        
        protected virtual void OnNightComes()
        {
            HUD.MessageLog.DisplayMessage("The night has fallen.", Colors.Yellow);
            NightComes?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnDawnComes()
        {
            HUD.MessageLog.DisplayMessage("The sun is dawning.", Colors.Yellow);
            DawnComes?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnEveningComes()
        {
            HUD.MessageLog.DisplayMessage("The evening approaches.", Colors.Yellow);
            EveningComes?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnDayBegins()
        {
            HUD.MessageLog.DisplayMessage("A new day begins.", Colors.Yellow);
            DayBegins?.Invoke(this, EventArgs.Empty);
        }

        public void Update()
        {
            Time++;

            if (time == Dawn) OnDawnComes();
            else if (time == Night) OnNightComes();
            else if (time == Evening) OnEveningComes();
            else if (time == 0) OnDayBegins();
        }

        public bool IsDay()
        {
            return Time >= 0 && Time < Night;
        }

        public Color AtmosphericLight()
        {
            double red = naturalAmbientLight.R;
            double green = naturalAmbientLight.G;
            double blue = naturalAmbientLight.B;

            if (Time > Evening && Time < Zenith)
            {
                double x = Time - Evening;
                red -= Math.Sin(x * Math.PI / (Zenith - Evening)) * 350;
                green -= Math.Sin(x * Math.PI / (Zenith - Evening)) * 300;
                blue -= Math.Sin(x * Math.PI / (Zenith - Evening)) * 50;
            }
            if (Time > Evening && Time < Night)
            {
                double x = Time - Evening;
                red += Math.Sin(x * Math.PI / (Night - Evening)) * 75;
            }
            return new Color((int)red, (int)green, (int)blue);
        }
    }
}
