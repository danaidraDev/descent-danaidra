﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    static class Progression
    {
        public const int LevelCap = 111;
        public const int ExpCap = 99999999;
        public const int StatCap = 100;
        public const int SkillPointCap = 999;
        public const int StatPointCap = 999;
        public const int SkillPointsPerLevel = 1;
        public const int StatPointsPerLevel = 2;

        public static int GetMaxExp(int level)
        {
            double newMax = 250 + (level * 33) * Math.Log(level) / (double)(level * 0.025d);
            return (int)newMax;
        }

        public static void PrintMaxExpCurve()
        {
            for (int i = 1; i < LevelCap; i++)
            {
                Debug.WriteLine(i + " = " + Progression.GetMaxExp(i));
            }
        }

        public static int GetMaxHealth(int vitality)
        {
            return vitality * 5;
        }

        public static int GetMaxMana(int magic)
        {
            return magic * 5;
        }

        public static int GetMaxFood()
        {
            return 100;
        }

        public static int GetMaxSleep()
        {
            return 100;
        }
    }
}
