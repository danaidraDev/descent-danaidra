﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public static class Combat
    {
        public static void MeleeHit(IFighter attacker, IFighter target, Action<IFighter, IFighter> attackEffect)
        {
            if (IsHitLanded(attacker, target))
            {
                InflictDamage(attacker.Name, target, CalculateDamage(attacker, target));
                attackEffect?.Invoke(attacker, target);
                InflictArmorStatuses(target, attacker);
                CheckWeaponDamage(attacker);
                CheckArmorDamage(target);
                CheckIfDead(attacker, target);
                PlayHitSound(attacker);
            }
            else
            {
                FloatingTextHandler.DisplayMapText(new FloatingText("Miss!", Colors.SteelGray, target.Position.GetValueOrDefault()));
                HUD.MessageLog.DisplayMessage(attacker.Name + " misses " + target.Name + ".");
            }
        }

        public static void SpellHit(IFighter attacker, IFighter target, string source, int damage)
        {
            InflictDamage(attacker.Name + "'s " + source, target, damage);
            InflictArmorStatuses(target, attacker);
            CheckArmorDamage(target);
            CheckIfDead(attacker, target);
        }

        public static int GetMinDamage(IFighter fighter)
        {
            return fighter.BaseDamage / 2;
        }

        public static int GetMaxDamage(IFighter fighter)
        {
            return fighter.BaseDamage + fighter.DamageBonus;
        }

        public static int GetMinDefense(IFighter fighter)
        {
            return fighter.BaseDefense / 2;
        }

        public static int GetMaxDefense(IFighter fighter)
        {
            return fighter.BaseDefense + fighter.DefenseBonus;
        }

        private static bool IsHitLanded(IFighter attacker, IFighter target)
        {
            return Randomiser.PassPercentileRoll(50 + (attacker.Focus.Current - target.Focus.Current));
        }

        private static int CalculateDamage(IFighter attacker, IFighter target)
        {
            int resulting = Randomiser.RandomNumber(GetMinDamage(attacker), GetMaxDamage(attacker));
            resulting -= Randomiser.RandomNumber(GetMinDefense(target), GetMaxDefense(target));
            return resulting > 0 ? resulting : 0;
        }

        private static void InflictDamage(string source, IFighter target, int damage)
        {
            target.Health.Current -= damage;
            FloatingTextHandler.DisplayMapText(new FloatingText("-" + damage, Colors.Red, target.Position.GetValueOrDefault()));
            HUD.MessageLog.DisplayMessage(source + " deals " + damage + " damage to " + target.Name + ".", Colors.Orange);
        }

        private static void CheckWeaponDamage(IFighter attacker)
        {
            if (attacker is Player player)
            {
                if (Randomiser.PassPercentileRoll(2))
                {
                    player.DamageWeapon();
                }
            }
        }

        private static void CheckArmorDamage(IFighter target)
        {
            if (target is Player player)
            {
                if (Randomiser.PassPercentileRoll(2))
                {
                    player.DamageArmor();
                }
            }
        }

        private static void PlayHitSound(IFighter attacker)
        {
            if (Randomiser.PassPercentileRoll(50))
            {
                if (attacker is Player player)
                {
                    switch (Randomiser.RandomNumber(1, 2))
                    {
                        case 1:
                            SoundHandler.PlaySound(SoundLibrary.ClangSquelch);
                            break;
                        case 2:
                            SoundHandler.PlaySound(SoundLibrary.Clang);
                            break;
                        default:
                            break;
                    }

                }
                else
                {
                    SoundHandler.PlaySound(SoundLibrary.HitSquelch);
                }
            }
        }

        private static void InflictArmorStatuses(IFighter wearer, IFighter attacker)
        {
            if (wearer.EquippedArmor != null)
            {
                wearer.EquippedArmor.InflictEffects(wearer, attacker);
            }
        }

        private static void CheckIfDead(IFighter attacker, IFighter target)
        {
            if (target.Health.Current <= 0)
            {
                target.Die(attacker);
                attacker.Kill(target);
            }
        }
    }
}
