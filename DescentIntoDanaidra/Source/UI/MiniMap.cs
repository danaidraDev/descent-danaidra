﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DescentIntoDanaidra.UI
{
    public class MiniMap
    {
        public MiniMap(int width, int height, Point position, ContentManager content, SpriteFont font, GraphicsDevice graphics)
        {
            Window = new Window(new Rectangle(position, new Point(width, height)), content, font)
            {
                Title = "Map",
            };
            MapRender = new Texture2D(graphics, Window.Rectangle.Width, Window.Rectangle.Height);
            Rectangle = new Rectangle(position, new Point(Window.Rectangle.Width, Window.Rectangle.Height));
        }

        public Window Window { get; private set; }
        public Texture2D MapRender { get; private set; }
        public Rectangle Rectangle { get; private set; }

        public void Update(Player player, TileTypeList tileTypes)
        {
            Color[] colorData = new Color[Window.Rectangle.Width * Window.Rectangle.Height];
            Tilemap tilemap = player.Location.Tilemap;

            for (int y = 0; y < Window.Rectangle.Height; y++)
                for (int x = 0; x < Window.Rectangle.Width; x++)
                {
                    Point pos = new Point(player.Position.GetValueOrDefault().X + (x - Window.Rectangle.Width / 2) / 2,
                        player.Position.GetValueOrDefault().Y + (y - Window.Rectangle.Height / 2) / 2);

                    if (tilemap.Discovered(pos.X, pos.Y))
                    {
                        AssignColor(player, tileTypes, colorData, tilemap, y, x, pos);
                    }
                    else
                    {
                        colorData[y * Window.Rectangle.Width + x] = Colors.Black;
                    }
                }

            MapRender.SetData<Color>(colorData);
        }

        private void AssignColor(Player player, TileTypeList tileTypes, Color[] colorData, Tilemap tilemap, int y, int x, Point pos)
        {
            if (tilemap.IsPositionWithinTilemap(pos) && pos == player.Position)
            {
                colorData[y * Window.Rectangle.Width + x] = Colors.Red;
            }
            else if (tilemap.IsPositionWithinTilemap(pos) && tilemap.IsWalkable(pos) && !(tilemap.GetObject(pos) is Obstacle))
            {
                if (tilemap.GetTile(pos).Type == tileTypes.Path || tilemap.GetTile(pos).Type == tileTypes.SnowPath)
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.Tan;
                }
                else if (tilemap.GetTile(pos).Type == tileTypes.Floor || tilemap.GetTile(pos).Type == tileTypes.Pavement)
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.Silver;
                }
                else if (tilemap.GetTile(pos).Type == tileTypes.Goo)
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.RosyBrown;
                }
                else if (tilemap.GetTile(pos).Type == tileTypes.Snow)
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.White;
                }
                else if (tilemap.GetTile(pos).Type == tileTypes.StairsDown)
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.DimGray;
                }
                else
                {
                    colorData[y * Window.Rectangle.Width + x] = Color.OliveDrab;
                }
            }
            else
            {
                colorData[y * Window.Rectangle.Width + x] = Color.DimGray;
            }
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphics, Player player)
        {
            Window.Draw(spriteBatch);
            spriteBatch.Draw(MapRender, Rectangle, Color.White);
        }
    }
}
