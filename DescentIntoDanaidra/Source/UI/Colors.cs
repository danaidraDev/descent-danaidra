﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class Colors
    {
        public static readonly Color LightGreen = new Color(70, 220, 10);
        public static readonly Color Green = new Color(0, 160, 25);
        public static readonly Color LightBlue = new Color(160, 230, 235);
        public static readonly Color Blue = new Color(15, 180, 220);
        public static readonly Color DeepBlue = new Color(30, 30, 150);
        public static readonly Color SeaBlue = new Color(0, 135, 125);
        public static readonly Color Purple = new Color(120, 50, 70);
        public static readonly Color Orange = new Color(220, 180, 0);
        public static readonly Color Red = new Color(235, 50, 40);
        public static readonly Color DeepRed = new Color(150, 0, 0);
        public static readonly Color Yellow = new Color(235, 235, 0);
        public static readonly Color DarkYellow = new Color(200, 175, 0);
        public static readonly Color FontWhite = new Color(240, 240, 240, 255);
        public static readonly Color FontShadowColor = new Color(5, 5, 5, 200);
        public static readonly Color FontHoverColor = new Color(255, 220, 80);
        public static readonly Color TransparentBarColor = new Color(50, 50, 50, 1);
        public static readonly Color Black = new Color(0, 0, 0);
        public static readonly Color SteelGray = new Color(160, 185, 225);
    }
}
