﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class ListView
    {
        public const int RowHeight = 25;
        public const int TopRowOffset = 35;
        private const int ScrollBarWidth = 25;
        private List<ListElement> list;
        private List<ListColumn> columns;
        private readonly int maxVisible;
        private int cursor;
        private int topVisible;
        private readonly Point upScrollPos;
        private readonly Point downScrollPos;

        public ListView(Point position, List<ListElement> list, ListColumnDefinition[] columnDefinitions, int displayedElements, bool isActive)
        {
            this.IsActive = isActive;
            RefreshList(list);
            Position = position;
            Cursor = 0;
            topVisible = 0;
            maxVisible = displayedElements;
            upScrollPos = new Point(position.X, position.Y + TopRowOffset);
            downScrollPos = new Point(upScrollPos.X, upScrollPos.Y + (maxVisible - 1) * RowHeight);
            AddColumns(columnDefinitions);
        }

        public ListView(Point position, List<ListElement> list, ListColumnDefinition[] columnDefinitions, int displayedElements)
            : this(position, list, columnDefinitions, displayedElements, true)
        {
        }

        public int Cursor { get { return cursor; } set { cursor = MathHelper.Clamp(value, 0, list.Count - 1); }  }
        public Point Position { get; }
        public bool IsActive { get; set; }

        public void Update(InputHandler input)
        {
            if (IsActive)
            {
                if (KeyBinds.UpKeyHit(input))
                {
                    MoveCursorUp();
                }
                else if (KeyBinds.DownKeyHit(input))
                {
                    MoveCursorDown();
                }
            }
        }

        public void RefreshList(List<ListElement> list)
        {
            this.list = list;

            Cursor = cursor;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            DrawColumnLabels(spriteBatch);
            DrawColumnContent(spriteBatch);
            DrawScrollIndicators(spriteBatch);
        }

        private void DrawColumnLabels(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < columns.Count; i++)
            {
                columns[i].Draw(spriteBatch, list, Cursor);
            }
        }

        private void DrawColumnContent(SpriteBatch spriteBatch)
        {
            for (int c = 0; c < columns.Count; c++)
            {
                int displayed = (topVisible + maxVisible < list.Count()) ? topVisible + maxVisible : list.Count();
                for (int r = topVisible; r < displayed; r++)
                {
                    columns[c].DrawRow(spriteBatch, list, Cursor, topVisible, r, IsActive);
                }
            }
        }

        private void DrawScrollIndicators(SpriteBatch spriteBatch)
        {
            Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, "^", upScrollPos, CanScrollUp() ? Colors.FontWhite : Color.Gray);
            Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, "v", downScrollPos, CanScrollDown() ? Colors.FontWhite : Color.Gray);
        }

        private bool CanScrollUp()
        {
            return topVisible > 0;
        }

        private bool CanScrollDown()
        {
            return list.Count - 1 >= topVisible + maxVisible;
        }

        private void ScrollToBottom()
        {
            if (IsListScrollable())
            {
                topVisible = list.Count - maxVisible;
            }
            Cursor = list.Count - 1;
        }

        private void ScrollToTop()
        {
            Cursor = 0;
            topVisible = 0;
        }

        private void MoveCursorUp()
        {
            if (Cursor > 0)
            {
                Cursor--;

                if (Cursor < topVisible)
                {
                    topVisible--;
                }
            }
        }

        private void MoveCursorDown()
        {
            if (Cursor < list.Count() - 1)
            {
                Cursor++;

                if (Cursor >= topVisible + maxVisible)
                {
                    topVisible++;
                }
            }
        }

        private bool IsListScrollable()
        {
            return list.Count() > maxVisible;
        }

        private void AddColumns(ListColumnDefinition[] columnDefinitions)
        {
            int xPos = Position.X + ScrollBarWidth;

            columns = new List<ListColumn>();

            for (int i = 0; i < columnDefinitions.Length; i++)
            {
                columns.Add(new ListColumn(columnDefinitions[i].Label, new Point(xPos, Position.Y), i));
                xPos += columnDefinitions[i].Width;
            }
        }
    }
}
