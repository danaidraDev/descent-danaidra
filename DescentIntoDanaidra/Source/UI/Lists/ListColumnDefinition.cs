﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra.UI
{
    public class ListColumnDefinition
    {
        public ListColumnDefinition(string label, int width)
        {
            Label = label;
            Width = width;
        }

        public string Label { get; }
        public int Width { get; }
    }
}
