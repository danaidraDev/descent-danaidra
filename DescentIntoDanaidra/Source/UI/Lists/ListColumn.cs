﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class ListColumn
    {
        public ListColumn(string label, Point position, int id)
        {
            Label = label;
            Position = position;
            ID = id;
        }

        public string Label { get; }
        public Point Position { get; }
        public int ID { get; }

        public void Draw(SpriteBatch spriteBatch, List<ListElement> list, int cursor)
        {
            Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, Label, Position, Colors.SteelGray);
        }

        public void DrawRow(SpriteBatch spriteBatch, List<ListElement> list, int cursor, int topVisible, int i, bool isHighlightVisible)
        {
            Point pos = new Point(Position.X, Position.Y + ListView.RowHeight * (i - topVisible) + ListView.TopRowOffset);
            Color color = (i == cursor && isHighlightVisible) ? Colors.FontHoverColor : Colors.FontWhite;
            Output.DrawText(spriteBatch, UIStateHandler.DefaultFont, GetPropToDisplay(list, i), pos, color);
        }

        private string GetPropToDisplay(List<ListElement> list, int i)
        {
            return list[i].Props.Length > ID ? list[i].Props[ID] : "--";
        }
    }
}
