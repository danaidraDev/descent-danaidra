﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class ListElement
    {
        public ListElement(string[] props)
        {
            Props = props;
        }
        
        public string[] Props { get; }
    }
}
