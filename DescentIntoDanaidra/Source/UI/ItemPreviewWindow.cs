﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class ItemPreviewWindow
    {
        private readonly Window window;
        private const int maxNameLength = 14;
        private readonly Point propertyPos1;
        private readonly Point propertyPos2;
        private readonly Point propertyPos3;

        public ItemPreviewWindow(Window window)
        {
            this.window = window;
            propertyPos1 = new Point(window.Box.Left, window.Box.Top + 80);
            propertyPos2 = new Point(window.Box.Left, window.Box.Top + 95);
            propertyPos3 = new Point(window.Box.Left, window.Box.Top + 110);
        }

        public void Draw(SpriteBatch spriteBatch, Item item)
        {
            window.Draw(spriteBatch);

            if (item != null)
            {
                spriteBatch.Draw(item.Texture, new Point(window.Box.Left + (window.Box.Width - Game1.TileSize) / 2,
                    window.Box.Top).ToVector2(), Color.White);
                Output.DrawText(spriteBatch, window.Font, Helper.TruncateString(item.Name, maxNameLength), new Point(window.Box.Left, window.Box.Top + 65));
                Output.DrawText(spriteBatch, window.Font, item.PropertyLine1, new Point(window.Box.Left, window.Box.Top + 80));
                Output.DrawText(spriteBatch, window.Font, item.PropertyLine2, new Point(window.Box.Left, window.Box.Top + 95));
                Output.DrawText(spriteBatch, window.Font, item.PropertyLine3, new Point(window.Box.Left, window.Box.Top + 110));
            }
            else
            {
                Output.DrawText(spriteBatch, window.Font, "none", new Point(window.Box.Left +
                    (window.Box.Width - (int)window.Font.MeasureString("none").X) / 2, window.Box.Top));
            }
        }
    }
}
