﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra.UI
{
    public class MenuOption
    {
        public MenuOption(string label, bool active, OnClick action)
        {
            Label = label;
            IsActive = active;
            Action = action;
        }

        public string Label { get; private set; }
        public bool IsActive { get; private set; }
        public delegate void OnClick();
        public OnClick Action { get; private set; }
    }
}
