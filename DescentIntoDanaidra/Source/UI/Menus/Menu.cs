﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class Menu
    {
        private int highlighted;
        private readonly bool centeredHorizontally;
        private readonly List<MenuOption> options;

        public Menu(List<MenuOption> menuOptions, Point position, SpriteFont font, int spacing, bool centeredHorizontally, int highlighted)
        {
            this.highlighted = MathHelper.Clamp(highlighted, 0, menuOptions.Count - 1);
            this.centeredHorizontally = centeredHorizontally;
            options = menuOptions;
            Position = position;
            Font = font;
            Spacing = spacing;
        }

        public Menu(List<MenuOption> menuOptions, Point position, SpriteFont font, int spacing, bool centered)
            : this(menuOptions, position, font, spacing, centered, 0)
        {
        }

        public Point Position { get; private set; }
        public SpriteFont Font { get; private set; }
        public int Spacing { get; private set; }

        public void Update(InputHandler input)
        {
            if (options.Any())
            {
                if (KeyBinds.ConfirmKeyHit(input))
                {
                    TryActivateAction();
                }
                else if (KeyBinds.UpKeyHit(input))
                {
                    MoveCursorUp();
                }
                else if (KeyBinds.DownKeyHit(input))
                {
                    MoveCursorDown();
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (options.Any())
            {
                for (int i = 0; i < options.Count; i++)
                {
                    Color color = options[i].IsActive ? i == highlighted ? Colors.FontHoverColor : Colors.FontWhite : i == highlighted ? Color.DarkGoldenrod : Color.Gray;
                    Point pos = centeredHorizontally ? new Point(Position.X - (int)Font.MeasureString(options[i].Label).X / 2, Position.Y + i * Spacing)
                        : new Point(Position.X, Position.Y + i * Spacing);
                    Output.DrawText(spriteBatch, Font, options[i].Label, pos, color);
                }
            }
        }

        private void MoveCursorDown()
        {
            if (highlighted < options.Count - 1)
            {
                highlighted++;
            }
        }

        private void MoveCursorUp()
        {
            if (highlighted > 0)
            {
                highlighted--;
            }
        }

        private void TryActivateAction()
        {
            if (options[highlighted].IsActive)
            {
                options[highlighted].Action();
            }
        }
    }
}
