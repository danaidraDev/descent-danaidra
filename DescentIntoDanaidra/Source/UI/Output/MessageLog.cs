﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra.UI
{
    public class MessageLog
    {
        const int MaxMessages = 7;
        const int MaxMessageLenght = 60;
        private readonly List<Text> messages = new List<Text>();

        public MessageLog()
        {
        }

        public void Initialize(string startingMessage)
        {
            ClearMessages();
            DisplayMessage(startingMessage);
        }

        public void ClearMessages()
        {
            messages.Clear();
        }

        public void Draw(SpriteBatch spriteBatch, Point position, SpriteFont font)
        {
            for (int i = 0; i < messages.Count; i++)
            {
                Point pos = new Point(position.X, position.Y - 20 * (messages.Count - i));
                Output.DrawText(spriteBatch, font, messages[i].String, pos, messages[i].Color);
            }
        }

        public void DisplayMessage(string message)
        {
            DisplayMessage(message, Colors.FontWhite);
        }

        public void DisplayMessage(string message, Color color)
        {
            if (message.Length > MaxMessageLenght)
            {
                SplitMessage(message, color);
            }
            else
            {
                DisplayMessage(new Text(message, color));
            }
        }

        private void DisplayMessage(Text message)
        {
            messages.Add(message);

            if (messages.Count > MaxMessages)
            {
                messages.RemoveAt(0);
            }
        }

        private void SplitMessage(string message, Color color)
        {
            if (message != "" && message != null)
            {
                List<string> words;
                string currentLine = "";

                words = message.Split(' ').ToList<string>();

                for (int i = 0; i < words.Count; i++)
                {
                    if (currentLine != "")
                    {
                        currentLine += " ";
                    }

                    if (currentLine.Length + words[i].Length <= MaxMessageLenght)
                    {
                        currentLine += words[i];
                    }
                    else
                    {
                        DisplayMessage(new Text(currentLine, color));
                        currentLine = words[i];
                    }
                }

                if (currentLine != "")
                {
                    DisplayMessage(new Text(currentLine, color));
                }
            }
        }
    }
}
