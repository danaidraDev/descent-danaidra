﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra.UI
{
    public class TextBox
    {
        private readonly List<string> lines;
        private Rectangle _box;

        public TextBox(string text, Rectangle box, SpriteFont font)
        {
            lines = new List<string>();
            Box = box;
            Font = font;
            DisplayText(text);
        }

        public TextBox(string text, Rectangle box, SpriteFont font, Texture2D texture) :
            this(text, box, font)
        {
            Texture = texture;
        }

        public string Text { get; private set; }
        public Rectangle Box {
            get => _box;
            set
            {
                _box = value;
                DisplayText(Text);
            }
        }
        public Texture2D Texture { get; private set; }
        public SpriteFont Font { get; private set; }

        public int Height()
        {
            return lines.Count() * (int)Font.MeasureString("A").Y;
        }

        public void DisplayText(string text)
        {
            lines.Clear();
            Text = text;

            if (text != "" && text != null)
            {
                List<string> words;
                string currentLine = "";
                int currentLineWidth = 0;

                words = text.Split(' ').ToList<string>();

                for (int i = 0; i < words.Count; i++)
                {
                    if (words[i] == "br")
                    {
                        lines.Add(currentLine);
                        currentLine = "";
                    }
                    else
                    {
                        if (currentLine != "")
                        {
                            currentLine += " ";
                        }

                        currentLineWidth = (int)(Font.MeasureString(currentLine + words[i]).X);

                        if (currentLineWidth <= Box.Width)
                        {
                            currentLine += words[i];
                        }
                        else
                        {
                            lines.Add(currentLine);
                            currentLine = words[i];
                        }
                    }
                }

                if (currentLine != "")
                {
                    lines.Add(currentLine);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Texture != null)
            {
                spriteBatch.Draw(Texture, Box, Color.White);
            }

            if (lines.Any())
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    Point pos = new Point(Box.Left, Box.Top + (Font.LineSpacing * i));
                    Output.DrawText(spriteBatch, Font, lines[i], pos);
                }
            }
        }
    }
}
