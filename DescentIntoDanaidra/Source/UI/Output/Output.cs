﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DescentIntoDanaidra.UI
{
    public class Output 
    {
        public static void DrawText(SpriteBatch spriteBatch, SpriteFont font, string text, Point position, Color color)
        {
            if (text != null)
            {
                Color shadow = new Color(Colors.FontShadowColor.R, Colors.FontShadowColor.G, Colors.FontShadowColor.B, color.A);
                Vector2[] vectors = new Vector2[7];
                vectors[0] = position.ToVector2();
                vectors[1] = position.ToVector2() + new Vector2(1, 1);
                vectors[2] = position.ToVector2() + new Vector2(2, 0);
                vectors[3] = position.ToVector2() + new Vector2(0, -1);
                vectors[4] = position.ToVector2() + new Vector2(-1, 0);
                vectors[5] = position.ToVector2() + new Vector2(1, 1);
                vectors[6] = position.ToVector2() + new Vector2(1, -1);

                for (int i = 0; i < vectors.Length; i++)
                {
                    spriteBatch.DrawString(font, text, vectors[i], shadow);
                }
                spriteBatch.DrawString(font, text, position.ToVector2(), new Color(color.R - 70, color.G - 70, color.B - 70, color.A));
                spriteBatch.DrawString(font, text, position.ToVector2() + new Vector2(1, 0), color);
            }
        }

        public static void DrawText(SpriteBatch spriteBatch, SpriteFont font, string text, Point position)
        {
            DrawText(spriteBatch, font, text, position, Colors.FontWhite);
        }
    }
}
