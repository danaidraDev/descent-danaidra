﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra.UI
{
    public class Text
    {
        public Text(string text)
        {
            String = text;
            Color = Colors.FontWhite;
        }

        public Text(string text, Color color)
            : this(text)
        {
            Color = color;
        }

        public string String { get; private set; }
        public Color Color { get; set; }
    }
}
