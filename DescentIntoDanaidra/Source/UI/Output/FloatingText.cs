﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class FloatingText
    {
        private const int MaxLife = 50;

        public FloatingText(string text, Color color, Point position)
        {
            Text = text;
            Color = color;
            Position = position;
            LifeSpan = new Stat("life", 0, MaxLife);
        }

        public string Text { get; }
        public Color Color { get; set; }
        public Point Position { get; set; }
        public Stat LifeSpan { get; }
        
        public void Update()
        {
            LifeSpan.Current++;
        }
    }
}
