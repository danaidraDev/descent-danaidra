﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class FloatingTextHandler
    {
        const int MaxPerList = 50;
        private static readonly List<FloatingText> mapTexts = new List<FloatingText>();
        private static readonly List<FloatingText> uiTexts = new List<FloatingText>();

        public FloatingTextHandler()
        {
        }

        public static void DisplayMapText(FloatingText floatingText)
        {
            if (mapTexts.Count() < MaxPerList)
            {
                mapTexts.Add(floatingText);
            }
        }

        public static void DisplayUIText(FloatingText floatingText)
        {
            if (uiTexts.Count() < MaxPerList)
            {
                uiTexts.Add(floatingText);
            }
        }

        public void Update()
        {
            UpdateList(mapTexts);
            UpdateList(uiTexts);
        }

        private static void UpdateList(List<FloatingText> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                FloatingText t = list[i];

                if (t.LifeSpan.Current >= t.LifeSpan.Max)
                {
                    list.RemoveAt(i);
                    i--;
                }
                else
                {
                    t.Update();
                }
            }
        }

        public void DrawUITexts(SpriteBatch spriteBatch, SpriteFont font)
        {
            for (int i = 0; i < uiTexts.Count; i++)
            {
                FloatingText t = uiTexts[i];
                Point position = new Point(t.Position.X, t.Position.Y - t.LifeSpan.Current);
                Color color = t.LifeSpan.Current > 3 ? t.Color : Colors.FontHoverColor;
                Output.DrawText(spriteBatch, font, t.Text, position, color);
            }
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font, Camera camera)
        {
            for (int i = 0; i < mapTexts.Count; i++)
            {
                FloatingText t = mapTexts[i];
                Point position = new Point(Helper.MapPositionToScreen(t.Position, camera).X + 20,
                    Helper.MapPositionToScreen(t.Position, camera).Y - t.LifeSpan.Current);
                Color color = t.LifeSpan.Current > 3 ? t.Color : Colors.FontHoverColor;
                Output.DrawText(spriteBatch, font, t.Text, position, color);
            }

            DrawUITexts(spriteBatch, font);
        }
    }
}
