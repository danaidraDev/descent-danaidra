﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class Window
    {
        public Window(Rectangle rectangle, ContentManager content, SpriteFont font)
        {
            Rectangle = rectangle;
            Margin = 10;
            Font = font;
            Fill = content.Load<Texture2D>("ui/bar");
            UpLeft = content.Load<Texture2D>("ui/windowUL");
            UpRight = content.Load<Texture2D>("ui/windowUR");
            Up = content.Load<Texture2D>("ui/windowUP");
            Down = content.Load<Texture2D>("ui/windowDW");
            DownLeft = content.Load<Texture2D>("ui/windowDL");
            DownRight = content.Load<Texture2D>("ui/windowDR");
            Side = content.Load<Texture2D>("ui/windowSD");
        }

        public Rectangle Rectangle { get; set; }
        public int Margin { get; set; }
        public Rectangle Box { get { return new Rectangle(Rectangle.Left + Margin, Rectangle.Top + Margin,
            Rectangle.Width - Margin * 2, Rectangle.Height - Margin * 2); } }
        public SpriteFont Font { get; set; }
        public string Title { get; set; }
        public Texture2D Fill { get; set; }
        public Texture2D UpLeft { get; set; }
        public Texture2D UpRight { get; set; }
        public Texture2D Up { get; set; }
        public Texture2D Down { get; set; }
        public Texture2D DownLeft { get; set; }
        public Texture2D DownRight { get; set; }
        public Texture2D Side { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Fill, Rectangle, new Color(50, 50, 50));
            spriteBatch.Draw(UpLeft, new Vector2(Rectangle.Left - UpLeft.Width, Rectangle.Top - UpLeft.Height), Color.White);
            spriteBatch.Draw(UpRight, new Vector2(Rectangle.Right, Rectangle.Y - UpRight.Height), Color.White);
            spriteBatch.Draw(Up, new Rectangle(Rectangle.Left, Rectangle.Top - Up.Height, Rectangle.Width, Up.Height), Color.White);
            spriteBatch.Draw(Down, new Rectangle(Rectangle.Left, Rectangle.Bottom, Rectangle.Width, Down.Height), Color.White);
            spriteBatch.Draw(DownLeft, new Vector2(Rectangle.Left - DownLeft.Width, Rectangle.Bottom), Color.White);
            spriteBatch.Draw(DownRight, new Vector2(Rectangle.Right, Rectangle.Bottom), Color.White);
            spriteBatch.Draw(Side, new Rectangle(Rectangle.Left - Side.Width, Rectangle.Top, Side.Width, Rectangle.Height), Color.White);
            spriteBatch.Draw(Side, new Rectangle(Rectangle.Right, Rectangle.Top, Side.Width, Rectangle.Height), Color.White);

            if(Title != null)
            {
                Vector2 titlePos = new Vector2(Rectangle.Center.X - Font.MeasureString(Title).X / 2,
                    Rectangle.Top - Up.Height + Font.MeasureString(Title).Y / 2);
                Output.DrawText(spriteBatch, Font, Title, titlePos.ToPoint());
            }
        }
    }
}
