﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class InventorableDetailWindow : UIState
    {
        private const int LineOffset = 25;
        private readonly Point instructionsPosition;
        private readonly Point iconPosition;
        private readonly Point namePosition;
        private readonly TextBox descriptionTextBox;
        private readonly IInventorable item;

        public InventorableDetailWindow(ContentManager content, IInventorable inventorable)
        {
            this.item = inventorable;

            Font = UIStateHandler.DefaultFont;

            Point windowSize = new Point(300, 350);
            Window = new Window(new Rectangle(Helper.CenterOnScreen(windowSize), windowSize), content, Font)
            {
                Title = "Item details",
            };

            namePosition = new Point(Helper.CenterHorizontally((int)Font.MeasureString(inventorable.Name).X, Window.Box), Window.Box.Top);
            iconPosition = new Point(Helper.CenterHorizontally(Game1.TileSize, Window.Box), namePosition.Y + LineOffset);
            descriptionTextBox = new TextBox(inventorable.Description,
                new Rectangle(Window.Box.Left, iconPosition.Y + Game1.TileSize + LineOffset, Window.Box.Width, 100), Font);
            descriptionTextBox.DisplayText("Type: " + inventorable.TypeName + " br " + " br " + inventorable.Description);
            instructionsPosition = new Point(Window.Box.Left, Window.Box.Bottom - 15);

            Open();
        }

        public Window Window { get; }
        public SpriteFont Font { get; }

        public override void Update(InputHandler input, Player player)
        {
            if (KeyBinds.CancelKeyHit(input) || KeyBinds.DetailsKeyHit(input))
            {
                Close();
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            Window.Draw(spriteBatch);
            Output.DrawText(spriteBatch, Font, item.Name, namePosition);
            if (item.Texture != null)
            {
                spriteBatch.Draw(item.Texture, iconPosition.ToVector2(), Color.White);
            }
            descriptionTextBox.Draw(spriteBatch);
            DrawInstructions(spriteBatch, instructionsPosition);
        }
        
        private void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[.] [-] Close", position);
        }

        private void Open()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurn);
        }

        private void Close()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurnB);
            End = true;
        }
    }
}