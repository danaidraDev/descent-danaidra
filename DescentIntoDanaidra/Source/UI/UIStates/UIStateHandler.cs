﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class UIStateHandler
    {
        private readonly ContentManager content;
        private readonly GraphicsDevice graphics;

        public UIStateHandler(ContentManager content, GraphicsDevice graphics)
        {
            this.content = content;
            this.graphics = graphics;

            ButtonSize = new Point(150, 20);
            PlayerBarSize = new Point(150, 18);
            WindowSize = new Point(500, 320);
            ItemWindowSize = new Point(130, 140);
            PromptSize = new Point(300, 100);
            Font = content.Load<SpriteFont>("fonts/font");
            DefaultFont = Font;
            BoxTexture = content.Load<Texture2D>("ui/button");
            ButtonTexture = content.Load<Texture2D>("ui/button");
            BarTexture = content.Load<Texture2D>("ui/bar");
            
            uiStates = new Stack<UIState>();
        }
        
        public Stack<UIState> uiStates { get; }
        public static Point ButtonSize { get; private set; }
        public static Point PlayerBarSize { get; private set; }
        public static Point WindowSize { get; private set; }
        public static Point ItemWindowSize { get; private set; }
        public static Point PromptSize { get; private set; }
        public SpriteFont Font { get; private set; }
        public static SpriteFont DefaultFont { get; private set; }
        public Texture2D BoxTexture { get; private set; }
        public Texture2D ButtonTexture { get; private set; }
        public Texture2D BarTexture { get; private set; }

        public void OpenMessagePrompt(string title, string message)
        {
            uiStates.Push(new MessagePrompt(Helper.CenterOnScreen(PromptSize), PromptSize, title, message, content, Font));
        }

        public void OpenYesNoPrompt(string title, string message, Action whenYes, Action whenNo)
        {
            uiStates.Push(new YesNoPrompt(Helper.CenterOnScreen(PromptSize), PromptSize, title, message, content, Font, whenYes, whenNo));
        }

        public void OpenInputPrompt(string title, InputPrompt.Foo foo, int maxChars, bool acceptsEmpty, string error)
        {
            Point size = new Point(300, 30);
            InputPrompt prompt = new InputPrompt(Helper.CenterOnScreen(size), size, title, content, Font, foo, maxChars)
            {
                AcceptsEmpty = acceptsEmpty,
                EmptyError = error,
            };
            uiStates.Push(prompt);
        }

        public void OpenDialogueMenu(NPC npc, Player player, TimeOfDay timeOfDay)
        {
            uiStates.Push(new DialogueMenu(npc, player, timeOfDay, content, this));
        }

        public void OpenDialogueLines(List<string> lines, string title)
        {
            uiStates.Push(new ReadLines(lines, title, content));
        }

        public void OpenTrade(Player player, IVendor vendor)
        {
            uiStates.Push(new TradeWindow(player, vendor, content, "Trade", this));
        }

        public void OpenContainer(Player player, IVendor container)
        {
            uiStates.Push(new ContainerWindow(player, container, content, "Container", this));
        }

        public void OpenStash(Player player, Texture2D stashTexture)
        {
            uiStates.Push(new StashWindow(player, stashTexture, content, this));
        }

        public void OpenInventorableDetails(IInventorable inventorable)
        {
            uiStates.Push(new InventorableDetailWindow(content, inventorable));
        }

        public void Update(InputHandler input, Player player)
        {
            if (uiStates.Any())
            {
                if (uiStates.Peek().End)
                {
                    uiStates.Pop();
                }
                else
                {
                    uiStates.Peek().Update(input, player);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, Player player)
        {
            for (int i = uiStates.Count() - 1; i >= 0; i--)
            {
                if (!uiStates.ElementAt(i).End)
                {
                    uiStates.ElementAt(i).Draw(spriteBatch, player);
                }
            }
        }

        public bool IsOtherInputBlocked()
        {
            return uiStates.Where(i => !(i is HUD)).Any();
        }

        public UIState TopState()
        {
            return uiStates.Peek();
        }

        public void ClearStates()
        {
            uiStates.Clear();
        }

        public void ResetToHUD()
        {
            ClearStates();
            uiStates.Push(new HUD(content, graphics, this));
        }
    }
}
