﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class HUD : UIState
    {
        public static readonly MessageLog MessageLog = new MessageLog();
        private readonly ContentManager content;
        private readonly GraphicsDevice graphics;
        private readonly UIStateHandler ui;

        public HUD(ContentManager content, GraphicsDevice graphics, UIStateHandler ui)
        {
            this.content = content;
            this.graphics = graphics;
            this.ui = ui;

            Font = content.Load<SpriteFont>("fonts/font");
            BoxTexture = content.Load<Texture2D>("ui/button");
            ButtonTexture = content.Load<Texture2D>("ui/button");
            BarTexture = content.Load<Texture2D>("ui/bar");

            MiniMap = new MiniMap(140, 140, new Point(Game1.BufferWidth - 160, 60), content, Font, graphics);
            IsMapDisplayed = true;
            PlayerHealthBar = new PercentBar(UIStateHandler.PlayerBarSize, BarTexture, BarTexture, Colors.Green, Colors.TransparentBarColor);
            PlayerMagicBar = new PercentBar(UIStateHandler.PlayerBarSize, BarTexture, BarTexture, Colors.DeepBlue, Colors.TransparentBarColor);
            PlayerFoodBar = new PercentBar(UIStateHandler.PlayerBarSize, BarTexture, BarTexture, Colors.Purple, Colors.TransparentBarColor);
            PlayerRestBar = new PercentBar(UIStateHandler.PlayerBarSize, BarTexture, BarTexture, Colors.SeaBlue, Colors.TransparentBarColor);
            MonsterHealthBar = new PercentBar(new Point(40, 5), BarTexture, BarTexture, Colors.Green, Colors.TransparentBarColor);

            AddShortcuts();
        }

        public EventHandler QuitToMenu;
        public List<Button> Shortcuts { get; private set; }
        public static MiniMap MiniMap { get; private set; }
        public PercentBar PlayerHealthBar { get; private set; }
        public PercentBar PlayerMagicBar { get; private set; }
        public PercentBar PlayerFoodBar { get; private set; }
        public PercentBar PlayerRestBar { get; private set; }
        public PercentBar MonsterHealthBar { get; private set; }
        public SpriteFont Font { get; private set; }
        public Texture2D BoxTexture { get; private set; }
        public Texture2D ButtonTexture { get; private set; }
        public Texture2D BarTexture { get; private set; }
        public bool IsMapDisplayed { get; private set; }

        public override void Update(InputHandler input, Player player)
        {
            HandleKeys(input, player);
        }

        public static void UpdateMinimap(Player player, TileTypeList tileTypeList)
        {
            if (player != null && MiniMap != null)
            {
                MiniMap.Update(player, tileTypeList);
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            DrawShortcuts(spriteBatch);

            if (IsMapDisplayed)
            {
                MiniMap.Draw(spriteBatch, graphics, player);
            }

            DrawQuickUse(spriteBatch, player);

            if (IsBottomHUDDisplayed())
            {
                DrawPlayerBars(player, spriteBatch);
                DrawPlayerStatuses(spriteBatch, player, new Point(50, Game1.BufferHeight - 150));
                MessageLog.Draw(spriteBatch, new Point(250, Game1.BufferHeight - 19), Font);
            }
        }

        private void HandleKeys(InputHandler input, Player player)
        {
            if (input.KeyHit(Keys.M))
            {
                IsMapDisplayed = !IsMapDisplayed;
            }
            else if (input.KeyHit(Keys.C))
            {
                ui.uiStates.Push(new CharacterWindow(content, Font, ui, player));
            }
            else if (input.KeyHit(Keys.E))
            {
                ui.uiStates.Push(new EquipmentWindow(content, Font, player, ui));
            }
            else if (input.KeyHit(Keys.O))
            {
                ui.uiStates.Push(new OptionsWindow(content, Font, ui, player));
            }
            else if (input.KeyHit(Keys.S))
            {
                ui.uiStates.Push(new SkillsWindow(content, Font, player, ui));
            }
            else if (input.KeyHit(Keys.Escape))
            {
                AskQuit(player);
            }
        }

        private void AskQuit(Player player)
        {
            if(player.Location is OutdoorLocation)
            {
                ui.OpenYesNoPrompt("Save and exit", "Do you want save and to quit to menu?", OnQuitToMenu, null);
            }
            else
            {
                ui.OpenYesNoPrompt("Exit", "Saving is not possible in dungeons. br br " +
                    " If you exit now, you'll go back to the entrance of the dungeon, and some of your progress will be lost." +
                    " br br Do you want to quit to menu?", OnQuitToMenu, null);
            }
        }

        private bool IsBottomHUDDisplayed()
        {
            return !ui.uiStates.Where(i => i is DialogueMenu || i is ReadLines).Any();
        }

        protected virtual void OnQuitToMenu()
        {
            QuitToMenu?.Invoke(this, EventArgs.Empty);
        }

        private void AddShortcuts()
        {
            Shortcuts = new List<Button>();
            string[] strings = new string[] { "E - Equipment", "S - Skills", "C - Character", "O - Options", "M - Map" };
            Point menuPosition = new Point(Game1.BufferWidth / 2 - (strings.Length * UIStateHandler.ButtonSize.X) / 2, 0);
            for (int i = 0; i < strings.Length; i++)
            {
                Point pos = new Point(menuPosition.X + i * UIStateHandler.ButtonSize.X, 0);
                Shortcuts.Add(new Button(strings[i], new Rectangle(pos, UIStateHandler.ButtonSize), Font, ButtonTexture));
            }
        }

        private void DrawPlayerBars(Player player, SpriteBatch spriteBatch)
        {
            int spacing = 26;
            Point bars = new Point(50, Game1.BufferHeight - 120);
            DrawPlayerBar(PlayerHealthBar, new Point(bars.X, bars.Y), spriteBatch, player.Health);
            DrawPlayerBar(PlayerMagicBar, new Point(bars.X, bars.Y + spacing), spriteBatch, player.Mana);
            DrawPlayerBar(PlayerFoodBar, new Point(bars.X, bars.Y + spacing * 2), spriteBatch, player.Food);
            DrawPlayerBar(PlayerRestBar, new Point(bars.X, bars.Y + spacing * 3), spriteBatch, player.Sleep);
        }

        private void DrawPlayerBar(PercentBar bar, Point barPos, SpriteBatch spriteBatch, Stat stat)
        {
            bar.Draw(spriteBatch, barPos, stat.Current, stat.Max);
            string barStr = stat.Name + " " + stat.Current + "/" + stat.Max;
            Point strPos = new Point(barPos.X + (int)(PlayerMagicBar.Size.X - Font.MeasureString(barStr).X) / 2,
                barPos.Y + (int)(PlayerMagicBar.Size.Y - Font.MeasureString(barStr).Y) / 2 + 1);
            Output.DrawText(spriteBatch, Font, barStr, strPos, Colors.FontWhite);
        }

        private void DrawFighterHealthBar(IFighter fighter, SpriteBatch spriteBatch, Point position)
        {
            MonsterHealthBar.Draw(spriteBatch, position, fighter.Health.Current, fighter.Health.Max);
        }

        public void DrawOverhead(IFighter fighter, SpriteBatch spriteBatch, Rectangle creatureRect, Color nameColor)
        {
            Point overheadPos = new Point(creatureRect.Center.X - (int)Font.MeasureString(fighter.Name).X / 2 + 1, creatureRect.Top - 24);
            Output.DrawText(spriteBatch, Font, fighter.Name, overheadPos, nameColor);
            DrawFighterHealthBar(fighter, spriteBatch, new Point(creatureRect.Center.X - MonsterHealthBar.Size.X / 2, creatureRect.Top - 8));
        }

        private void DrawQuickUse(SpriteBatch spriteBatch, Player player)
        {
            for (int i = 0; i < player.QuickUses.Length; i++)
            {
                if (player.QuickUses[i] != null)
                {
                    Point pos = new Point(Game1.BufferWidth - Game1.TileSize - 20, 220 + i * 70);
                    int sum = i + 1;
                    spriteBatch.Draw(player.QuickUses[i].Texture, pos.ToVector2(), Color.White);
                    Output.DrawText(spriteBatch, Font, sum.ToString(), new Point(pos.X + 55, pos.Y + 55), Colors.FontWhite);
                }
            }
        }

        private void DrawPlayerStatuses(SpriteBatch spriteBatch, Player player, Point pos)
        {
            for (int i = 0; i < player.Statuses.Count; i++)
            {
                int offset = player.Statuses.Count <= 6 ? 26 : UIStateHandler.PlayerBarSize.X / player.Statuses.Count + 1;
                spriteBatch.Draw(player.Statuses[i].Icon, new Rectangle(pos.X + offset * i, pos.Y, 20, 20), Color.White);
            }
        }

        private void DrawShortcuts(SpriteBatch spriteBatch)
        {
            foreach (Button b in Shortcuts)
            {
                b.Draw(spriteBatch);
            }
        }
    }
}
