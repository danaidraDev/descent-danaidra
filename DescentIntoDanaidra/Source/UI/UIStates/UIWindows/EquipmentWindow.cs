﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class EquipmentWindow : InventoryWindow
    {
        private readonly Texture2D gold;
        private readonly Texture2D weight;
        private readonly ItemPreviewWindow weaponWindow;
        private readonly ItemPreviewWindow armorWindow;
        private readonly Point goldPos;
        private readonly Point weightPos;
        private readonly ListView listView;
        private readonly UIStateHandler ui;

        public EquipmentWindow(ContentManager content, SpriteFont font, Player player, UIStateHandler ui)
            : base(content, font, "Equipment", player)
        {
            this.ui = ui;
            ShortcutKey = Keys.E;
            gold = content.Load<Texture2D>("ui/gold");
            weight = content.Load<Texture2D>("ui/weight");

            armorWindow = new ItemPreviewWindow(new Window(new Rectangle(new Point(Window.Rectangle.Left - 150, Window.Rectangle.Top),
                UIStateHandler.ItemWindowSize), content, font) { Title = "Current armor" });
            weaponWindow = new ItemPreviewWindow(new Window(new Rectangle(new Point(Window.Rectangle.Left - 150, Window.Rectangle.Top + 180),
                UIStateHandler.ItemWindowSize), content, font) { Title = "Current weapon" });

            goldPos = new Point(Window.Box.Left, Window.Box.Bottom - BottomOffset);
            weightPos = new Point(Window.Box.Left + 125, Window.Box.Bottom - BottomOffset);

            listView = new ListView(new Point(Window.Box.Left + 30, Window.Box.Top), GetListElements(player), GetListColumnDefinitions(), 8);
        }

        public override void Update(InputHandler input, Player player)
        {
            base.Update(input, player);

            listView.Update(input);

            if (player.Equipment.Count() > listView.Cursor && player.Equipment[listView.Cursor] is Item item)
            {
                if (KeyBinds.ConfirmKeyHit(input))
                {
                    TryUseEquip(player, item);
                }
                else if (input.KeyHit(Keys.D1))
                {
                    player.AddQuickUse(1, item);
                }
                else if (input.KeyHit(Keys.D2))
                {
                    player.AddQuickUse(2, item);
                }
                else if (input.KeyHit(Keys.D3))
                {
                    player.AddQuickUse(3, item);
                }
                else if (input.KeyHit(Keys.D4))
                {
                    player.AddQuickUse(4, item);
                }
                else if (KeyBinds.DetailsKeyHit(input))
                {
                    ui.OpenInventorableDetails(item);
                }
                else if (KeyBinds.DropKeyHit(input))
                {
                    TryDropItem(player, item);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);

            listView.Draw(spriteBatch);

            Point goldPos = new Point(Window.Box.Left, Window.Box.Bottom - BottomOffset);
            spriteBatch.Draw(gold, goldPos.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, "Gold: " + player.Gold.Current,
                new Point(goldPos.X + 25, goldPos.Y + 5));

            Point weightPos = new Point(Window.Box.Left + 125, Window.Box.Bottom - BottomOffset);
            spriteBatch.Draw(weight, weightPos.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, "Weight: " + player.Weight + "/" + player.MaxWeight,
                new Point(weightPos.X + 25, weightPos.Y + 5));

            armorWindow.Draw(spriteBatch, player.EquippedArmor);
            weaponWindow.Draw(spriteBatch, player.EquippedWeapon);
        }

        protected override void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[Enter] Use/Equip  [1][2][3][4] Bind  [Num0] Drop  [.] See details", position);
        }

        private void TryUseEquip(Player player, Item item)
        {
            if (item is IUsable usable && usable.Use(player))
            {
                if (usable.IsConsumable)
                {
                    player.RemoveItem(item);
                    listView.RefreshList(GetListElements(player));
                }
            }
            else if (item is Armor armor)
            {
                player.EquipArmor(armor);
            }
            else if (item is Weapon weapon)
            {
                player.EquipWeapon(weapon);
            }
        }

        private void TryDropItem(Player player, Item item)
        {
            if (player.TryDropItem(item))
            {
                listView.RefreshList(GetListElements(player));
            }
        }

        private ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Name", 250),
                new ListColumnDefinition("Weight", 75),
                new ListColumnDefinition("Value", 75),
            };
        }

        private List<ListElement> GetListElements(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            foreach (Item item in player.Equipment)
            {
                listElements.Add(new ListElement(new string[] { item.Name, item.Weight.ToString(), item.Value.ToString() }));
            }

            return listElements;
        }
    }
}
