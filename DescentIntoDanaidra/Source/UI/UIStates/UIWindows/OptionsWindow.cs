﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class OptionsWindow : UIWindow
    {
        private readonly ListView listView;

        public OptionsWindow(ContentManager content, SpriteFont font, UIStateHandler ui, Player player)
            : base(content, font, "Options")
        {
            ShortcutKey = Keys.O;
            Open(player);
            listView = new ListView(new Point(Window.Box.Left, Window.Box.Top), GetListElements(player), GetListColumnDefinitions(), 2);
        }

        public override void Update(InputHandler input, Player player)
        {
            base.Update(input, player);
            listView.Update(input);

            if (Options.InGameSettings.Count > listView.Cursor)
            {
                if (KeyBinds.LeftKeyHit(input))
                {
                    Options.InGameSettings[listView.Cursor].LeftPressed();
                    listView.RefreshList(GetListElements(player));
                }
                else if (KeyBinds.RightKeyHit(input))
                {
                    Options.InGameSettings[listView.Cursor].RightPressed();
                    listView.RefreshList(GetListElements(player));
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            DrawInstructions(spriteBatch, new Point(Window.Box.Left, Window.Box.Bottom - 15));
        }

        private void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            listView.Draw(spriteBatch);
            Output.DrawText(spriteBatch, Font, "[-] Close", position);
        }

        private ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Settings", 200),
                new ListColumnDefinition("", 25),
                new ListColumnDefinition("", 40),
                new ListColumnDefinition("", 25),
            };
        }

        private List<ListElement> GetListElements(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            foreach (Setting setting in Options.InGameSettings)
            {
                listElements.Add(new ListElement(new string[] { setting.Name, "<", setting.DisplayedValue(), ">" }));
            }

            return listElements;
        }
    }
}
