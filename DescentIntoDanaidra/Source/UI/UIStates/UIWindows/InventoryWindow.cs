﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class InventoryWindow : UIWindow
    {
        protected const int BottomOffset = 50;

        public InventoryWindow(ContentManager content, SpriteFont font, string name, Player player)
            : base(content, font, name)
        {
            int descLeft = Window.Box.Center.X + 50;
            DescriptionBox = new TextBox("", new Rectangle(descLeft, Window.Box.Top +
                Game1.TileSize + 10, Window.Box.Right - descLeft, 120), Font);

            Open(player);
        }

        public TextBox DescriptionBox { get; private set; }

        public override void Open(Player player)
        {
            base.Open(player);
        }

        public override void Update(InputHandler input, Player player)
        {
            base.Update(input, player);
        }

        protected virtual void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            
            DrawInstructions(spriteBatch, new Point(Window.Box.Left, Window.Box.Bottom - 15));
        }
    }
}
