﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class InputPrompt : Prompt
    {
        private readonly Foo foo;
        private readonly int maxChars;
        private readonly Dictionary<Keys, char> keys;

        public InputPrompt(Point position, Point size, string title, ContentManager content, SpriteFont font, Foo foo, int maxChars)
            : base(position, size, title, content, font)
        {
            TextPosition = new Point(Window.Box.Left, Window.Box.Top);
            this.maxChars = maxChars;
            this.foo = foo;
            EnteredString = "";

            keys = new Dictionary<Keys, char>()
            {
                { Keys.D0, '0' },
                { Keys.D1, '1' },
                { Keys.D2, '2' },
                { Keys.D3, '3' },
                { Keys.D4, '4' },
                { Keys.D5, '5' },
                { Keys.D6, '6' },
                { Keys.D7, '7' },
                { Keys.D8, '8' },
                { Keys.D9, '9' },
                { Keys.OemComma, ',' },
                { Keys.OemPeriod, '.' },
            };
        }
        
        public Point TextPosition { get; private set; }
        public string EnteredString { get; private set; }
        public delegate void Foo(string value);
        public bool AcceptsEmpty { get; set; }
        public string EmptyError { get; set; }

        public override void Update(InputHandler input, Player player)
        {
            if (input.KeyHit(Keys.Enter))
            {
                if (!AcceptsEmpty && EnteredString == "")
                {
                    HUD.MessageLog.DisplayMessage(EmptyError, Colors.Orange);
                }
                else
                {
                    Close();
                    foo(EnteredString);
                }
            }
            else if (input.KeyHit(Keys.Back))
            {
                if (EnteredString.Length > 0)
                {
                    EnteredString = EnteredString.Remove(EnteredString.Length - 1);
                }
            }
            else if (input.KeyHit(Keys.Space) && EnteredString.Length <= maxChars)
            {
                EnteredString += " ";
            }
            else
            {
                foreach (Keys key in Enum.GetValues(typeof(Keys)))
                {
                    if (input.KeyHit(key) && EnteredString.Length <= maxChars)
                    {
                        string parsed = key.ToString();
                        if (parsed.Length == 1)
                        {
                            char c = parsed[0];

                            if (input.KeyPressed(Keys.LeftShift) || input.KeyPressed(Keys.RightShift))
                            {
                                EnteredString += c;
                            }
                            else
                            {
                                EnteredString += char.ToLower(c);
                            }
                        }
                        else
                        {
                            foreach (KeyValuePair<Keys, char> k in keys)
                            {
                                if (key == k.Key)
                                {
                                    EnteredString += k.Value;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            if (EnteredString != null)
            {
                Output.DrawText(spriteBatch, Font, EnteredString + "_", TextPosition);
            }
        }
    }
}
