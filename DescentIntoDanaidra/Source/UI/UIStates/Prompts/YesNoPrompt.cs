﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class YesNoPrompt : Prompt
    {
        public YesNoPrompt(Point position, Point size, string title, string message, ContentManager content, SpriteFont font, Action whenYes, Action whenNo)
            : base(position, size, title, content, font)
        {
            TextBox = new TextBox(message + " (Y/N)", Window.Box, font);
            DoWhenYes = whenYes;
            DoWhenNo = whenNo;
            FitSizeToText();
        }

        public TextBox TextBox { get; private set; }
        public Action DoWhenYes;
        public Action DoWhenNo;

        protected override void FitSizeToText()
        {
            Point newSize = new Point(Window.Rectangle.Width, TextBox.Height() + 20);
            Window.Rectangle = new Rectangle(Helper.CenterOnScreen(newSize), newSize);
            TextBox.Box = Window.Box;
        }

        public override void Update(InputHandler input, Player player)
        {
            if (input.KeyHit(Keys.Y))
            {
                Close();
                DoWhenYes?.Invoke();
            }
            else if (input.KeyHit(Keys.N))
            {
                Close();
                DoWhenNo?.Invoke();
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            TextBox.Draw(spriteBatch);
        }
    }
}
