﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class MessagePrompt : Prompt
    {
        public MessagePrompt(Point position, Point size, string title, string message, ContentManager content, SpriteFont font)
            : base (position, size, title, content, font)
        {
            TextBox = new TextBox(message, Window.Box, font);
            ConfirmButton = new Button("OK", new Rectangle(ButtonPosition(), UIStateHandler.ButtonSize), font, content.Load<Texture2D>("ui/button"));
            FitSizeToText();
        }

        private Point ButtonPosition()
        {
            return new Point(Window.Box.Center.X - UIStateHandler.ButtonSize.X / 2, Window.Box.Bottom - UIStateHandler.ButtonSize.Y);
        }

        public TextBox TextBox { get; private set; }
        public Button ConfirmButton { get; private set; }

        protected override void FitSizeToText()
        {
            Point newSize = new Point(Window.Rectangle.Width, TextBox.Height() + 50);
            Window.Rectangle = new Rectangle(Helper.CenterOnScreen(newSize), newSize);
            TextBox.Box = Window.Box;
            ConfirmButton.SetPosition(ButtonPosition());
        }

        public override void Update(InputHandler input, Player player)
        {
            if (KeyBinds.ConfirmKeyHit(input) || KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            TextBox.Draw(spriteBatch);
            ConfirmButton.Draw(spriteBatch);
        }
    }
}
