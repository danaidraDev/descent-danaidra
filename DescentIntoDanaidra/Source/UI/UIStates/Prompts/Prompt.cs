﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace DescentIntoDanaidra.UI
{
    public class Prompt : UIState
    {
        public Prompt(Point position, Point size, string title, ContentManager content, SpriteFont font)
        {
            Font = font;
            Window = new Window(new Rectangle(position, size), content, font)
            {
                Title = title
            };
        }

        public Window Window { get; private set; }
        public SpriteFont Font { get; private set; }

        protected void Close()
        {
            End = true;
        }

        protected virtual void FitSizeToText()
        {
        }

        public override void Update(InputHandler input, Player player)
        {
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            Window.Draw(spriteBatch);
        }
    }
}
