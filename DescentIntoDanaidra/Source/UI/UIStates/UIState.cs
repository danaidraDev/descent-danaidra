﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public abstract class UIState
    {
        public bool End { get; protected set; }

        public abstract void Update(InputHandler input, Player player);

        public abstract void Draw(SpriteBatch spriteBatch, Player player);
    }
}
