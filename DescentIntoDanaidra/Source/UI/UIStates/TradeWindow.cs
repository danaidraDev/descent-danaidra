﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class TradeWindow : UIState
    {
        private const short ListTopOffset = 70;
        private const short DisplayedItems = 6;
        protected const short MaxItemNameLength = 18;
        private readonly UIStateHandler ui;
        protected readonly Point playerIconPosition;
        protected readonly Point vendorIconPosition;
        protected readonly Point playerNamePosition;
        protected readonly Point vendorNamePosition;
        protected readonly Point playerPropertyPosition;
        protected readonly Point vendorPropertyPosition;

        public TradeWindow(Player player, IVendor vendor, ContentManager content, string windowTitle, UIStateHandler ui)
        {
            this.ui = ui;
            Font = content.Load<SpriteFont>("fonts/font");
            Player = player;
            Vendor = vendor;
            HighlightedList = 0;
            Exchangers = new IVendor[2];
            Exchangers[0] = player;
            Exchangers[1] = vendor;

            Point tradeWindowSize = UIStateHandler.WindowSize;
            Point tradeWindowPosition = new Point((Game1.BufferWidth - tradeWindowSize.X) / 2, (Game1.BufferHeight - tradeWindowSize.Y) / 2);
            Window = new Window(new Rectangle(tradeWindowPosition, tradeWindowSize), content, Font)
            {
                Title = windowTitle,
            };
            Gold = content.Load<Texture2D>("ui/gold");
            
            playerIconPosition = new Point(Window.Box.Left, Window.Box.Top);
            vendorIconPosition = new Point(Window.Box.Center.X, Window.Box.Top);
            playerNamePosition = new Point(playerIconPosition.X + 80, playerIconPosition.Y);
            vendorNamePosition = new Point(vendorIconPosition.X + 80, vendorIconPosition.Y);
            playerPropertyPosition = new Point(playerNamePosition.X, playerNamePosition.Y + 20);
            vendorPropertyPosition = new Point(vendorNamePosition.X, vendorNamePosition.Y + 20);

            ResetProperties();

            PlayerItemListView = new ListView(new Point(Window.Box.Left, Window.Box.Top + ListTopOffset), GetPlayerListElements(player), GetListColumnDefinitions(), DisplayedItems);
            VendorItemListView = new ListView(new Point(Window.Box.Center.X, Window.Box.Top + ListTopOffset), GetVendorListElements(vendor), GetListColumnDefinitions(), DisplayedItems, false);
            
            Open();
        }
        
        public Window Window { get; }
        public SpriteFont Font { get; }
        protected Player Player { get; }
        protected IVendor Vendor { get; }
        protected int HighlightedList { get; set; }
        protected IVendor[] Exchangers { get; }
        protected Texture2D Gold { get; }
        protected ListView PlayerItemListView { get; }
        protected ListView VendorItemListView { get; }
        protected PropertyDisplay PlayerProperty { get; set; }
        protected PropertyDisplay VendorProperty { get; set; }

        public override void Update(InputHandler input, Player player)
        {
            if (KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
            else if (KeyBinds.RightKeyHit(input))
            {
                HighlightedList = 1;
                PlayerItemListView.IsActive = false;
                VendorItemListView.IsActive = true;
                RefreshListElements(player);
            }
            else if (KeyBinds.LeftKeyHit(input))
            {
                HighlightedList = 0;
                PlayerItemListView.IsActive = true;
                VendorItemListView.IsActive = false;
                RefreshListElements(player);
            }
            switch (HighlightedList)
            {
                case 0:
                    UpdatePlayerList(input, player);
                    break;
                case 1:
                    UpdateVendorList(input, Vendor);
                    break;
                default:
                    break;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            Window.Draw(spriteBatch);
            DrawExchangerInfo(spriteBatch, playerIconPosition, Player.Texture, playerNamePosition, Player.Name,
                playerPropertyPosition, PlayerProperty);
            DrawExchangerInfo(spriteBatch, vendorIconPosition, Vendor.Texture, vendorNamePosition, Vendor.Name,
                vendorPropertyPosition, VendorProperty);
            PlayerItemListView.Draw(spriteBatch);
            VendorItemListView.Draw(spriteBatch);
            DrawInstructions(spriteBatch, new Point(Window.Box.Left, Window.Box.Bottom - 15));
        }

        protected virtual void DrawExchangerInfo(SpriteBatch spriteBatch, Point iconPosition, Texture2D icon,
            Point namePosition, string name, Point propertyPosition, PropertyDisplay property)
        {
            spriteBatch.Draw(icon, iconPosition.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, name, namePosition);
            property.Draw(spriteBatch, Font, propertyPosition);
        }

        private int CalculatePrice(Item item)
        {
            return item.Value * 2;
        }

        protected bool IsSlotEmptyOrNull(List<IInventorable> list, int slot)
        {
            return list.Count <= slot || list[slot] == null;
        }

        protected virtual void UpdatePlayerList(InputHandler input, Player player)
        {
            PlayerItemListView.Update(input);

            if (player.Equipment.Count() > PlayerItemListView.Cursor && player.Equipment[PlayerItemListView.Cursor] is Item item)
            {
                HandlePlayerListInput(input, player, item);
            }
        }

        protected virtual void HandlePlayerListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TrySellItem(item, player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected virtual void UpdateVendorList(InputHandler input, IVendor vendor)
        {
            VendorItemListView.Update(input);

            if (vendor.Equipment.Count() > VendorItemListView.Cursor && vendor.Equipment[VendorItemListView.Cursor] is Item item)
            {
                HandleVendorListInput(input, Player, item);
            }
        }

        protected virtual void HandleVendorListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryBuyItem(item, Player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected virtual void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[Enter] Buy/Sell [.] Details [-] Close", position);
        }

        private void TrySellItem(Item item, Player player)
        {
            if (item != player.EquippedArmor && item != player.EquippedWeapon)
            {
                SellItem(item, player);
                RefreshListElements(player);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Item equipped!", Colors.Orange, playerIconPosition));
            }
        }

        protected void RefreshListElements(Player player)
        {
            PlayerItemListView.RefreshList(GetPlayerListElements(player));
            VendorItemListView.RefreshList(GetVendorListElements(Vendor));
            ResetProperties();
        }

        protected virtual void ResetProperties()
        {
            PlayerProperty = new PropertyDisplay(Gold, "Gold: " + Player.Gold.Current);
            VendorProperty = new PropertyDisplay(Gold, "Gold: " + Vendor.Gold.Current);
        }

        private void SellItem(Item item, Player player)
        {
            if (Vendor.Gold.Current >= item.Value)
            {
                player.Gold.Current += item.Value;
                Vendor.Gold.Current -= item.Value;
                player.RemoveItem(item);
                Vendor.Equipment.Add(item);
                SoundHandler.PlaySound(SoundLibrary.CoinJingle);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Not enough gold!", Colors.Orange, vendorIconPosition));
            }
        }

        private void TryBuyItem(Item item, Player player)
        {
            if (Player.CanTakeItem(item))
            {
                BuyItem(item);
                RefreshListElements(player);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Overburdened!", Colors.Orange, playerIconPosition));
            }
        }

        private void BuyItem(Item item)
        {
            if (Player.Gold.Current >= CalculatePrice(item))
            {
                Player.Gold.Current -= CalculatePrice(item);
                Vendor.Gold.Current += CalculatePrice(item);
                Player.Equipment.Add(item);
                Vendor.Equipment.Remove(item);
                SoundHandler.PlaySound(SoundLibrary.BagRustle);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Not enough gold!", Colors.Orange, playerIconPosition));
            }
        }
        
        protected virtual ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Item", 150),
                new ListColumnDefinition("Price", 75),
            };
        }

        protected virtual List<ListElement> GetPlayerListElements(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            if (player.Equipment.Any())
            {
                foreach (Item item in player.Equipment)
                {
                    listElements.Add(new ListElement(new string[] { Helper.TruncateString(item.Name, MaxItemNameLength), item.Value.ToString() }));
                }
            }
            else
            {
                listElements.Add(new ListElement(new string[] { "empty", }));
            }

            return listElements;
        }

        protected virtual List<ListElement> GetVendorListElements(IVendor vendor)
        {
            List<ListElement> listElements = new List<ListElement>();

            if (vendor.Equipment.Any())
            {
                foreach (Item item in vendor.Equipment)
                {
                    listElements.Add(new ListElement(new string[] { Helper.TruncateString(item.Name, MaxItemNameLength), CalculatePrice(item).ToString() }));
                }
            }
            else
            {
                listElements.Add(new ListElement(new string[] { "empty", }));
            }

            return listElements;
        }

        private void Open()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurn);
        }

        private void Close()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurnB);
            End = true;
        }
    }
}
