﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace DescentIntoDanaidra.UI
{
    public class Button
    {
        public Button(string text, Rectangle box, SpriteFont font, Texture2D texture)
        {
            Text = text;
            Box = box;
            Texture = texture;
            Font = font;
            SetTextPosition();
        }

        public string Text { get; private set; }
        public Rectangle Box { get; private set; }
        public Texture2D Texture { get; private set; }
        public SpriteFont Font { get; private set; }
        public Point TextPosition { get; private set; }

        public void SetPosition(Point position)
        {
            Box = new Rectangle(position, Box.Size);
            SetTextPosition();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if(Texture != null)
            {
                spriteBatch.Draw(Texture, Box, Color.White);
            }

            if(Text != null)
            {
                Output.DrawText(spriteBatch, Font, Text, TextPosition);
            }
        }

        private void SetTextPosition()
        {
            TextPosition = new Point(Helper.CenterHorizontally((int)Font.MeasureString(Text).X, Box),
                Helper.CenterVertically((int)Font.MeasureString(Text).Y, Box));
        }
    }
}
