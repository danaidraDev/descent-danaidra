﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    [Serializable]
    public class LocationData
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string MusicFile { get; set; }
        public string MapFile { get; set; }
        public string StaticsFile { get; set; }
        public string ObjectsFile { get; set; }

        public static LocationData LoadFromFile(string fileName)
        {
            string filePath = "Content/locations/" + fileName.ToLower() + ".xml";
            if (File.Exists(filePath))
            {
                XmlSerializer reader = new XmlSerializer(typeof(LocationData));
                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                LocationData loadedData = (LocationData)reader.Deserialize(file);
                file.Close();
                return loadedData;
            }
            return null;
        }
    }
}
