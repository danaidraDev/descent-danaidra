﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Tilemap
    {
        #region colors
        static readonly Color Black = new Color(0, 0, 0);
        static readonly Color White = new Color(255, 255, 255);
        static readonly Color DarkGrey = new Color(127, 127, 127);
        static readonly Color LightGray = new Color(195, 195, 195);
        static readonly Color DarkRed = new Color(136, 0, 21);
        static readonly Color Brown = new Color(185, 122, 87);
        static readonly Color Red = new Color(237, 28, 36);
        static readonly Color Pink = new Color(255, 174, 201);
        static readonly Color Orange = new Color(255, 127, 39);
        static readonly Color LightOrange = new Color(255, 201, 14);
        static readonly Color Yellow = new Color(255, 242, 0);
        static readonly Color LightYellow = new Color(239, 228, 176);
        static readonly Color Green = new Color(34, 177, 76);
        static readonly Color LightGreen = new Color(181, 230, 29);
        static readonly Color Blue = new Color(0, 162, 232);
        static readonly Color LightBlue = new Color(153, 217, 234);
        static readonly Color Indigo = new Color(63, 72, 204);
        static readonly Color BlueGray = new Color(112, 146, 190);
        static readonly Color Purple = new Color(163, 73, 164);
        static readonly Color LightPurple = new Color(200, 191, 231);
        static readonly Color JungleGreen = new Color(150, 150, 40);
        static readonly Color DarkJungleGreen = new Color(115, 115, 40);
        #endregion

        public Tilemap(Location location, int width, int height, TileTypeList tileTypes)
        {
            Location = location;
            TileTypes = tileTypes;

            SetLevelSize(width, height);
        }

        private void SetLevelSize(int width, int height)
        {
            Width = width;
            Height = height;
            Tiles = new Tile[Width + 1, Height + 1];
            FogOfWar = new bool[Width + 1, Height + 1];
            FillFogOfWar();
            Creatures = new IMapObject[Width, Height];
            Items = new IMapObject[Width, Height];
        }

        public Location Location { get; private set; }
        public TileTypeList TileTypes { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Tile[,] Tiles { get; private set; }
        public bool [,] FogOfWar { get; private set; }
        public IMapObject[,] Creatures { get; private set; }
        public IMapObject[,] Items { get; private set; }

        // The ReadFromFile method creates a new tilemap from a png file.
        public void ReadFromFile(Texture2D levelFile, TileTypeList tileTypes)
        {
            SetLevelSize(levelFile.Width, levelFile.Height);
            Color[] colorData = new Color[levelFile.Width * levelFile.Height];
            levelFile.GetData<Color>(colorData);

            var colorToType = new Dictionary<Color, TileType>()
            {
                { LightGreen, tileTypes.Grass},
                { LightGray, tileTypes.Floor},
                { DarkGrey, tileTypes.Wall},
                { White, tileTypes.Pavement},
                { Yellow, tileTypes.Sandstone},
                { Black, tileTypes.Pit},
                { LightYellow, tileTypes.Path },
                { Blue, tileTypes.Water },
                { LightBlue, tileTypes.Snow },
                { LightPurple, tileTypes.SnowPath },
                { BlueGray, tileTypes.SnowWall },
            };

            for (int y = 0; y < levelFile.Height; y++)
                for (int x = 0; x < levelFile.Width; x++)
                {
                    if(IsPositionWithinTilemap(new Point(x, y)))
                    {
                        Color c = colorData[y * levelFile.Width + x];
                        
                        if (colorToType.ContainsKey(c))
                        {
                            Tiles[x, y] = new Tile(colorToType[c]);
                        }
                    }
                }
        }

        public void ReadObjectsFromImage(Texture2D levelFile, ContentManager content, TileTypeList tileTypes,
            ObstacleTypeList obstacleTypes)
        {
            Color[] colorData = new Color[levelFile.Width * levelFile.Height];
            levelFile.GetData<Color>(colorData);

            var colorToObstacle = new Dictionary<Color, ObstacleType>()
            {
                { Green, obstacleTypes.Tree },
                { LightGreen, obstacleTypes.Shrub },
                { JungleGreen, obstacleTypes.JungleShrub },
                { DarkJungleGreen, obstacleTypes.JungleTree },
                { DarkGrey, obstacleTypes.Rock },
            };

            for (int y = 0; y < levelFile.Height; y++)
                for (int x = 0; x < levelFile.Width; x++)
                {
                    Point pos = new Point(x, y);

                    if (IsPositionWithinTilemap(pos))
                    {
                        Color c = colorData[y * levelFile.Width + x];

                        if (colorToObstacle.ContainsKey(c))
                        {
                            new Obstacle(Location, pos, colorToObstacle[c]);
                        }
                    }
                }
        }
        
        public void ReadObjectsFromTxt(string filePath, ObjectParser parser)
        {
            string line = string.Empty;
            using (StreamReader sr = new StreamReader(filePath))
            {
                int lineID = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    parser.ParseObject(line, Location);
                    lineID++;
                }
            }
        }

        public void ReadObjectsFromStringArray(string[] lines, ObjectParser parser)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                parser.ParseObject(lines[i], Location);
            }
        }

        public void DiscoverFog(Point position)
        {
            FogOfWar[position.X, position.Y] = false;
        }

        public bool Discovered(int x, int y)
        {
            return IsPositionWithinTilemap(new Point(x, y)) && FogOfWar[x, y] == false;
        }

        public IMapObject GetObject(Point position)
        {
            if (IsPositionWithinTilemap(position))
            {
                return Creatures[position.X, position.Y];
            }
            else
            {
                return null;
            }
        }

        public void SetCreature(IMapObject entity, Point position)
        {
            if (IsPositionWithinTilemap(position))
            {
                Creatures[position.X, position.Y] = entity;
            }
        }

        public void ChangeObjectPosition(IMapObject entity, Point? targetPosition)
        {
            if (entity.Position != null)
            {
                entity.Location.Tilemap.SetCreature(null, entity.Position.GetValueOrDefault());
            }
            if (targetPosition != null)
            {
                entity.Location.Tilemap.SetCreature(entity, targetPosition.GetValueOrDefault());
            }
            entity.Position = targetPosition;
        }

        public void ChangeObjectLocation(IMapObject entity, Location targetLocation, Point? targetPosition)
        {
            if (entity.Location != null && entity.Position != null)
            {
                entity.Location.Tilemap.SetCreature(null, entity.Position.GetValueOrDefault());
            }
            if (targetLocation != null && targetPosition != null)
            {
                targetLocation.Tilemap.SetCreature(entity, targetPosition.GetValueOrDefault());
            }
            entity.Location = targetLocation;
            entity.Position = targetPosition;
        }

        public IMapObject GetItem(Point position)
        {
            if (IsPositionWithinTilemap(position))
            {
                return Items[position.X, position.Y];
            }
            else
            {
                return null;
            }
        }

        public void SetItem(IMapObject entity, Point position)
        {
            if (IsPositionWithinTilemap(position))
            {
                Items[position.X, position.Y] = entity;
            }
        }

        public void ChangeItemPosition(IMapObject entity, Point? targetPosition)
        {
            if (entity.Position != null)
            {
                entity.Location.Tilemap.SetItem(null, entity.Position.GetValueOrDefault());
            }
            if (targetPosition != null)
            {
                Location.Tilemap.SetItem(entity, targetPosition.GetValueOrDefault());
            }
            entity.Position = targetPosition;
        }

        public void ChangeItemLocation(IMapObject entity, Location targetLocation, Point? targetPosition)
        {
            if (entity.Location != null && entity.Position != null)
            {
                entity.Location.Tilemap.SetItem(null, entity.Position.GetValueOrDefault());
            }
            if (targetLocation != null && targetPosition != null)
            {
                targetLocation.Tilemap.SetItem(entity, targetPosition.GetValueOrDefault());
            }
            entity.Location = targetLocation;
            entity.Position = targetPosition;
        }

        public Tile GetTile(Point position)
        {
            if (IsPositionWithinTilemap(position))
            {
                return Tiles[position.X, position.Y];
            }
            else
            {
                return null;
            }
        }

        public void SetTile(Point position, TileType type)
        {
            if (IsPositionWithinTilemap(position))
            {
                Tiles[position.X, position.Y] = new Tile(type);
            }
        }

        public void FillWithType(int left, int top, int right, int bottom, TileType type)
        {
            for (int y = top; y < bottom; y++)
                for (int x = left; x < right; x++)
                {
                    Tiles[x, y] = new Tile(type);
                }
        }

        public void FillWithType(Rectangle rectangle, TileType type)
        {
            FillWithType(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom, type);
        }

        public bool IsPositionWithinTilemap(Point position)
        {
            return position.X >= 0 && position.X < Width && position.Y >= 0 && position.Y < Height;
        }

        public bool IsWalkable(Point position)
        {
            return GetTile(position) != null && Tiles[position.X, position.Y].TileKind == TileType.Kind.floor;
        }

        public IMapObject FindClosest(Type type, Point position)
        {
            int distance = 0;
            const int maxDistance = 6;

            while (distance < maxDistance)
            {
                int y = -distance;
                while (y <= distance)
                {
                    int x = -distance;

                    while (x <= distance)
                    {
                        Point pos = new Point(position.X + x, position.Y + y);
                        IMapObject gottenObject = GetObject(pos);
                        if (gottenObject != null && gottenObject.GetType() == type)
                        {
                            return gottenObject;
                        }
                        IMapObject gottenItem = GetItem(pos);
                        if (gottenItem != null && gottenItem.GetType() == type)
                        {
                            return gottenItem;
                        }
                        x++;
                    }
                    y++;
                }
                distance++;
            }
            return null;
        }

        public void Draw(SpriteBatch spriteBatch, Player player, Camera camera, Lighting lighting, UIStateHandler ui)
        {
            int tileSize = Game1.TileSize;

            for (int y = camera.Position.Y / tileSize; y < Math.Ceiling((double)(camera.Position.Y + camera.Viewport.Height) / tileSize); y++)
                for (int x = camera.Position.X / tileSize; x < Math.Ceiling((double)(camera.Position.X + camera.Viewport.Width) / tileSize); x++)
                {
                    DrawPosition(spriteBatch, camera, lighting, ui, tileSize, y, x);
                }
        }

        private void DrawPosition(SpriteBatch spriteBatch, Camera camera, Lighting lighting, UIStateHandler ui, int tileSize, int y, int x)
        {
            Point position = new Point(x, y);
            Rectangle rectangle = new Rectangle(new Point(x * tileSize, y * tileSize), new Point(tileSize, tileSize));

            Color color = lighting.GetCalculatedColor(x - camera.Position.X / tileSize, y - camera.Position.Y / tileSize);

            DrawTileAtPosition(position, spriteBatch, rectangle, color);

            DrawItemAtPosition(position, spriteBatch, rectangle, color);

            DrawCreatureAtPosition(position, spriteBatch, rectangle, color, ui);

            DrawProjectileAtPosition(spriteBatch, position, rectangle, color);
        }

        private void DrawProjectileAtPosition(SpriteBatch spriteBatch, Point position, Rectangle rectangle, Color color)
        {
            Rectangle rect = new Rectangle(new Point(rectangle.Left, rectangle.Top - Game1.TileSize / 4), new Point(rectangle.Width, rectangle.Height));

            foreach (Projectile p in Location.Projectiles)
            {
                if (p.Position == position && p.Texture != null)
                {
                    spriteBatch.Draw(p.Texture, rect, color);
                }
            }
        }

        private void DrawTileAtPosition(Point position, SpriteBatch spriteBatch, Rectangle rectangle, Color color)
        {
            Tile tile = GetTile(position);

            if (tile != null && tile.Texture != null)
            {
                var above = new Point(position.X, position.Y - 1);
                var below = new Point(position.X, position.Y + 1);

                if (IsWallSideDrawn(tile, below))
                {
                    spriteBatch.Draw(tile.SideTexture, rectangle, color);
                }
                else if (IsPitSideDrawn(tile, above))
                {
                    spriteBatch.Draw(tile.SideTexture, rectangle, color);
                }
                else
                {
                    spriteBatch.Draw(tile.Texture, rectangle, color);
                }
            }
        }

        private bool IsPitSideDrawn(Tile tile, Point above)
        {
            return tile.TileKind == TileType.Kind.pit &&
                IsPositionWithinTilemap(above) &&
                GetTile(above).TileKind != TileType.Kind.pit &&
                tile.SideTexture != null;
        }

        private bool IsWallSideDrawn(Tile tile, Point below)
        {
            return tile.TileKind == TileType.Kind.wall &&
                IsPositionWithinTilemap(below) &&
                GetTile(below).TileKind != TileType.Kind.wall &&
                tile.SideTexture != null;
        }

        private void DrawItemAtPosition(Point position, SpriteBatch spriteBatch, Rectangle rectangle, Color color)
        {
            IMapObject item = GetItem(position);

            if (item != null && item.Texture != null)
            {
                spriteBatch.Draw(item.Texture, rectangle, color);
            }
        }

        private void DrawCreatureAtPosition(Point position, SpriteBatch spriteBatch, Rectangle rectangle, Color color, UIStateHandler ui)
        {
            IMapObject creature = GetObject(position);
            Rectangle creatureRect = new Rectangle(new Point(rectangle.Left, rectangle.Top - Game1.TileSize / 4), new Point(rectangle.Width, rectangle.Height));
            if (creature != null && creature.Texture != null)
            {
                spriteBatch.Draw(creature.Texture, creatureRect, color);
            }

            DrawCreatureOverhead(spriteBatch, ui, creature, creatureRect);
        }

        private static void DrawCreatureOverhead(SpriteBatch spriteBatch, UIStateHandler ui, IMapObject creature, Rectangle creatureRect)
        {
            if (ui.TopState() is HUD hud)
            {
                if (creature is Monster monster && monster.Health.Current < monster.Health.Max)
                {
                    hud.DrawOverhead(monster, spriteBatch, creatureRect, Colors.FontWhite);
                }
                if (creature is NPC npc && npc.Type.AI is MercenaryAI)
                {
                    hud.DrawOverhead(npc, spriteBatch, creatureRect, Colors.LightBlue);
                }
            }
        }

        private void FillFogOfWar()
        {
            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                {
                    FogOfWar[x, y] = true;
                }
        }
    }
}
