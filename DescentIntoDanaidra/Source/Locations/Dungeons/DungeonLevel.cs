﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class DungeonLevel : Location
    {
        private readonly Dungeon dungeon;
        private readonly ContentManager content;
        private readonly TileTypeList tileTypes;
        private readonly MonsterTypeList monsterTypes;
        private readonly ObstacleTypeList obstacleTypes;
        private readonly ContainerTypeList containerTypes;
        private readonly UIStateHandler ui;

        public DungeonLevel(Dungeon dungeon, int width, int height, ContentManager content, int levelIndex, TileTypeList tileTypes,
            MonsterTypeList monsterTypes, ObstacleTypeList obstacleTypes, ContainerTypeList containerTypes, UIStateHandler ui)
            : base(width, height, dungeon.AmbientLight, tileTypes)
        {
            this.dungeon = dungeon;
            this.content = content;
            this.tileTypes = tileTypes;
            this.monsterTypes = monsterTypes;
            this.obstacleTypes = obstacleTypes;
            this.containerTypes = containerTypes;
            this.ui = ui;
            LevelIndex = levelIndex;
            GenerateLevel();
        }

        public ChamberTree ChamberTree { get; private set; }
        public int LevelIndex { get; private set; }
        public Point Entrance { get; private set; }
        public Point Exit { get; private set; }

        private void GenerateLevel()
        {
            Tilemap.FillWithType(Bounds, dungeon.WallType);
            ChamberTree = new ChamberTree(Bounds.Size, dungeon.MinRooms, dungeon.MaxRooms, dungeon.MinRoomWidth, dungeon.MaxRoomWidth,
                dungeon.MinRoomHeight, dungeon.MaxRoomHeight, dungeon.ForceRegularRooms);

            ChamberTree.FillChambersWithTile(dungeon.FloorType, Tilemap);
            ChamberTree.FillPassagesWithTile(dungeon.FloorType, Tilemap);

            AddEntrance(ChamberTree.StartingChamber);

            if (dungeon.Cavelike)
            {
                CellularAutomata.ErodeTiles(Tilemap, dungeon.FloorType, dungeon.WallType);
            }
            if (HasLevelsBelow())
            {
                PlaceObject(TryPlaceStairsDown, 1);
            }
            PlaceObject(TryPlaceChest, dungeon.ChestsPerLevel);
            PlaceObject(TryPlaceMonster, dungeon.MonstersPerLevel);
            PlaceObject(TryPlaceItem, dungeon.ItemsPerLevel);
        }

        void AddEntrance(Rectangle startingRoom)
        {
            Entrance = startingRoom.Center;
            StairsUp stairsUp = new StairsUp(this, new Point(Entrance.X, Entrance.Y), dungeon, obstacleTypes.Stairs);
        }
        
        void PlaceObject(Func<bool> tryPlace, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                bool objectPlaced = false;
                int attempts = 0;
                while (!objectPlaced && attempts < 30)
                {
                    objectPlaced = tryPlace();
                    attempts++;
                }
            }
        }

        private bool TryPlaceStairsDown()
        {
            Point pos = ChamberTree.GetRandomChamber().Center;

            if (Tilemap.GetTile(new Point(pos.X - 1, pos.Y)).TileKind != TileType.Kind.wall)
            {
                StairsDown stairsDown = new StairsDown(this, pos, dungeon, tileTypes);
                Exit = new Point(pos.X - 1, pos.Y);
                return true;
            }
            return false;
        }

        private bool TryPlaceMonster()
        {
            Point pos = RandomPosition(ChamberTree.GetRandomChamber());

            if (CanPlaceObject(pos) && dungeon.HasMonsters())
            {
                if (Randomiser.PassPercentileRoll(75))
                {
                    new Monster(this, pos, dungeon.GetRandomMonsterType());
                }
                else
                {
                    new Monster(this, pos, monsterTypes.GetRandomType(LevelIndex * 5));
                }
                return true;
            }
            return false;
        }

        private bool TryPlaceItem()
        {
            Point pos = RandomPosition(ChamberTree.GetRandomChamber());

            if (CanPlaceItem(pos))
            {
                Loot.RandomLoot(this, pos, LevelIndex * 5);
                return true;
            }
            return false;
        }

        private bool TryPlaceChest()
        {
            Point position = ChamberTree.GetRandomChamber().Center;
            if (CanPlaceObject(position))
            {
                new Container(this, position, containerTypes.Chest, ui, 75, LevelIndex * 50);
                return true;
            }
            return false;
        }

        bool CanPlaceObject(Point pos)
        {
            return Tilemap.IsPositionWithinTilemap(pos) && Tilemap.IsWalkable(pos) &&
                Tilemap.GetObject(pos) == null && Tilemap.GetTile(pos).Type != tileTypes.StairsDown;
        }

        bool CanPlaceItem(Point pos)
        {
            return Tilemap.IsPositionWithinTilemap(pos) && Tilemap.IsWalkable(pos) &&
                (Tilemap.GetObject(pos) == null || Tilemap.GetObject(pos) is Monster) &&
                Tilemap.GetTile(pos).Type != tileTypes.StairsDown;
        }

        public void GoDownOneLevel(Player player)
        {
            DungeonLevel below = LevelBelowSpawned() ? dungeon.GetLevel(LevelIndex + 1) :
                dungeon.NewLevel(LevelIndex + 1);

            Tilemap.ChangeObjectLocation(player, below, new Point(below.Entrance.X + 1, below.Entrance.Y));

            HUD.MessageLog.DisplayMessage("You enter " + below.Name + ".");
        }

        public void GoUpOneLevel(Player player)
        {
            if (LevelIndex > 0)
            {
                DungeonLevel above = dungeon.GetLevel(LevelIndex - 1);
                Tilemap.ChangeObjectLocation(player, above, above.Exit);
                HUD.MessageLog.DisplayMessage("You enter " + above.Name + ".");
            }
            else
            {
                dungeon.ExitDungeon(player);
            }
        }

        private bool LevelBelowSpawned()
        {
            return LevelIndex + 1 < dungeon.LevelCount;
        }

        public bool HasLevelsBelow()
        {
            return LevelIndex + 1 < dungeon.TotalLevels;
        }
    }
}
