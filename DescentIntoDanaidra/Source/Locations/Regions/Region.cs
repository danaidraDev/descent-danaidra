﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Region
    {
        private readonly List<RegionLevel> levels;
        private readonly ContentManager content;
        private readonly TileTypeList tileTypes;
        private readonly MonsterTypeList monsterTypes;
        private readonly ObstacleTypeList obstacleTypes;
        private readonly ContainerTypeList containerTypes;
        private readonly UIStateHandler ui;
        private readonly TimeOfDay timeOfDay;
        private int totalLevels;
        private int width;
        private int height;

        public Region(Location entranceLocation, Point entrancePosition, Helper.Direction entranceDirection, int width, int height,
            ContentManager content, TileTypeList tileTypes, MonsterTypeList monsterTypes, ObstacleTypeList obstacleTypes,
            ContainerTypeList containerTypes, UIStateHandler ui, TimeOfDay timeOfDay)
        {
            EntranceLocation = entranceLocation;
            EntrancePosition = entrancePosition;
            Width = width;
            Height = height;

            this.content = content;
            this.ui = ui;
            this.tileTypes = tileTypes;
            this.monsterTypes = monsterTypes;
            this.obstacleTypes = obstacleTypes;
            this.containerTypes = containerTypes;
            this.timeOfDay = timeOfDay;

            levels = new List<RegionLevel>();
            Entrance = new Entrance(entranceLocation, entrancePosition, this, -1, 0, entranceDirection);
        }

        public string Name { get; set; }
        public SoundEffect Music { get; set; }
        public Entrance Entrance { get; private set; }
        public Location EntranceLocation { get; private set; }
        public Point EntrancePosition { get; private set; }
        public int LevelCount { get { return levels.Count; } }
        public int TotalLevels { get => totalLevels; set => totalLevels = MathHelper.Clamp(value, 1, 20); }
        public int Width { get => width; private set => width = MathHelper.Clamp(value, 10, 100); }
        public int Height { get => height; private set => height = MathHelper.Clamp(value, 10, 100); }

        public void EnterLevel(Entrance entrance, Player player)
        {
            if (entrance.TargetLevelIndex == -1)
            {
                Vector2 targetPosition = EntrancePosition.ToVector2() + Helper.NormalizedDirection(entrance.Direction);
                player.Location.Tilemap.ChangeObjectLocation(player, EntranceLocation, targetPosition.ToPoint());
                ClearLevels();
            }
            else
            {
                RegionLevel targetLocation = GetEntranceTargetLocation(entrance);
                Point targetPosition = GetEntranceTargetPosition(entrance, targetLocation);
                player.Location.Tilemap.ChangeObjectLocation(player, targetLocation, targetPosition);
            }
        }

        public RegionLevel GetLevel(int levelIndex)
        {
            if (levelIndex >= 0 && levelIndex < levels.Count())
            {
                return levels[levelIndex];
            }
            return null;
        }

        public RegionLevel NewLevel(Entrance entrance)
        {
            RegionLevel newLevel = new RegionLevel(Width, Height, timeOfDay, tileTypes, entrance.TargetLevelIndex, this, entrance)
            {
                Name = this.Name,
                Music = this.Music,
            };
            return newLevel;
        }

        public void AddEmptyLevelPointer()
        {
            levels.Add(null);
        }

        public int GetNextIndex()
        {
            return LevelCount;
        }

        public void ClearLevels()
        {
            levels.Clear();
        }

        private Point GetEntranceTargetPosition(Entrance entrance, RegionLevel targetLocation)
        {
            Point targetPosition = Point.Zero;

            switch (entrance.Direction)
            {
                case Helper.Direction.Up:
                    targetPosition = new Point(targetLocation.Bounds.Width / 2, targetLocation.Bounds.Bottom - 2);
                    break;
                case Helper.Direction.Down:
                    targetPosition = new Point(targetLocation.Bounds.Width / 2, targetLocation.Bounds.Top + 1);
                    break;
                case Helper.Direction.Left:
                    targetPosition = new Point(targetLocation.Bounds.Right - 2, targetLocation.Bounds.Height / 2);
                    break;
                case Helper.Direction.Right:
                    targetPosition = new Point(targetLocation.Bounds.Left + 1, targetLocation.Bounds.Height / 2);
                    break;
                default:
                    break;
            }

            return targetPosition;
        }

        private RegionLevel GetEntranceTargetLocation(Entrance entrance)
        {
            RegionLevel targetLocation = null;

            if (GetLevel(entrance.TargetLevelIndex) == null)
            {
                targetLocation = NewLevel(entrance);

                if (entrance.TargetLevelIndex >= LevelCount)
                {
                    levels.Add(targetLocation);
                }
                else
                {
                    levels[entrance.TargetLevelIndex] = targetLocation;
                }
            }
            else
            {
                targetLocation = GetLevel(entrance.TargetLevelIndex);
            }

            return targetLocation;
        }
    }
}