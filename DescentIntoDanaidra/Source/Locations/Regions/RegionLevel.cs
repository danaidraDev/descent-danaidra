﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class RegionLevel : OutdoorLocation
    {
        private readonly TileTypeList tileTypes;

        public RegionLevel(int width, int height, TimeOfDay timeOfDay, TileTypeList tileTypes, int levelIndex, Region region, Entrance entrance)
            : base(width, height, timeOfDay, tileTypes)
        {
            this.tileTypes = tileTypes;
            LevelIndex = levelIndex;
            Region = region;
            Entrance = entrance;
            ParentIndex = entrance.LocationIndex;

            GenerateLevel();
        }

        public Entrance Entrance { get; private set; }
        public Entrance Exit { get; private set; }
        public Region Region { get; private set; }
        public int LevelIndex { get; private set; }
        public int ParentIndex { get; private set; }

        private void GenerateLevel()
        {
            Tilemap.FillWithType(Bounds, tileTypes.Grass);

            AddExitToParent();

            AddEntrancesToChildren(Randomiser.RandomNumber(1, 3));
        }

        private void AddEntrancesToChildren(int entrances)
        {
            for (int i = 0; i < entrances; i++)
            {
                bool isLocated = false;
                int attempts = 0;

                while (!isLocated && attempts < 30 && Region.GetNextIndex() < Region.TotalLevels - 1)
                {
                    Entrance entrance;
                    Helper.Direction entranceDirection = Helper.RandomDirection();
                    Point position = EntrancePositionFromDirection(entranceDirection);

                    if (entranceDirection != Exit.Direction && Tilemap.IsWalkable(position) && Tilemap.GetObject(position) == null)
                    {
                        Region.AddEmptyLevelPointer();
                        int index = Region.GetNextIndex();
                        entrance = new Entrance(this, position, Region, LevelIndex, index, entranceDirection);
                        Tilemap.SetTile(entrance.Position.GetValueOrDefault(), tileTypes.Path);
                        isLocated = true;
                    }
                    attempts++;
                }
            }
        }

        private Point EntrancePositionFromDirection(Helper.Direction direction)
        {
            switch (direction)
            {
                case Helper.Direction.Up: return new Point(Bounds.Width / 2, 0);
                case Helper.Direction.Down: return new Point(Bounds.Width / 2, Bounds.Bottom - 1);
                case Helper.Direction.Left: return new Point(Bounds.Left, Bounds.Height / 2);
                case Helper.Direction.Right: return new Point(Bounds.Right - 1, Bounds.Height / 2);
                default: return Point.Zero;
            }
        }

        private void AddExitToParent()
        {
            Helper.Direction exitDirection = Helper.OppositeDirection(Entrance.Direction);
            Exit = new Entrance(this, EntrancePositionFromDirection(exitDirection), Region, LevelIndex,
                ParentIndex, exitDirection);
            Tilemap.SetTile(Exit.Position.GetValueOrDefault(), tileTypes.Path);
        }
    }
}
