﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace DescentIntoDanaidra
{
    public class Location
    {
        private List<IMapObject> toUpdate;
        private List<MonsterSpawn> spawnsToUpdate;

        public Location(int width, int height, Color ambientLight, TileTypeList tileTypes)
        {
            Tilemap = new Tilemap(this, width, height, tileTypes);
            Bounds = new Rectangle(0, 0, width, height);
            Spawns = new List<MonsterSpawn>();
            Projectiles = new List<Projectile>();
            AmbientLight = ambientLight;
            toUpdate = new List<IMapObject>();
            spawnsToUpdate = new List<MonsterSpawn>();
        }
        
        public string Name { get; set; }
        public string Id { get; set; }
        public Tilemap Tilemap { get; private set; }
        public List<MonsterSpawn> Spawns { get; private set; }
        public List<Projectile> Projectiles { get; private set; }
        public virtual Color AmbientLight { get; private set; }
        public SoundEffect Music { get; set; }
        public Rectangle Bounds { get; private set; }

        public Point RandomPosition(Rectangle rect)
        {
            return new Point(Randomiser.RandomNumber(rect.Left, rect.Right), Randomiser.RandomNumber(rect.Top, rect.Bottom));
        }
        
        public void UpdateObjects(Player player, Camera camera)
        {
            toUpdate.Clear();

            for (int y = 0; y < Tilemap.Height; y++)
            {
                for (int x = 0; x < Tilemap.Width; x++)
                {
                    AddRemoveCreature(y, x);
                    AddRemoveItem(y, x);
                }
            }

            for (int u = 0; u < toUpdate.Count(); u++)
            {
                if (!toUpdate[u].IsDead)
                {
                    toUpdate[u].Update(player);
                }
            }
            
            UpdateProjectiles(player);
            UpdateSpawns(player);
            UpdateFog(camera);
        }

        private void AddRemoveItem(int y, int x)
        {
            IMapObject o = Tilemap.GetItem(new Point(x, y));
            if (o != null)
            {
                if (o.IsDead)
                {
                    RemoveItem(o);
                }
                else
                {
                    toUpdate.Add(o);
                }
            }
        }

        public void RemoveItem(IMapObject o)
        {
            Tilemap.SetItem(null, new Point(o.Position.GetValueOrDefault().X, o.Position.GetValueOrDefault().Y));
        }

        private void AddRemoveCreature(int y, int x)
        {
            IMapObject o = Tilemap.GetObject(new Point(x, y));
            if (o != null)
            {
                if (o.IsDead)
                {
                    RemoveCreature(o);
                }
                else
                {
                    toUpdate.Add(o);
                }
            }
        }

        public void RemoveCreature(IMapObject o)
        {
            Tilemap.SetCreature(null, new Point(o.Position.GetValueOrDefault().X, o.Position.GetValueOrDefault().Y));
        }

        public void UpdateFog(Camera camera)
        {
            for (int x = camera.FrameInTiles().Left; x < camera.FrameInTiles().Right; x++)
                for (int y = camera.FrameInTiles().Top; y < camera.FrameInTiles().Bottom; y++)
                {
                    if (!Tilemap.Discovered(x, y))
                    {
                        Tilemap.DiscoverFog(new Point(x, y));
                    }
                }
        }

        public void UpdateProjectiles(Player player)
        {
            for (int i = Projectiles.Count - 1; i >= 0; i--)
            {
                Projectiles[i].Update(player);
            }
        }

        public void UpdateSpawns(Player player)
        {
            for (int s = 0; s < Spawns.Count; s++)
            {
                if (Spawns[s].IsDead)
                {
                    Spawns.RemoveAt(s);
                    s--;
                }
                else
                {
                    spawnsToUpdate.Add(Spawns[s]);
                }
            }

            for (int u = 0; u < spawnsToUpdate.Count(); u++)
            {
                spawnsToUpdate[u].Update(player);
            }

            spawnsToUpdate.Clear();
        }
    }
}
