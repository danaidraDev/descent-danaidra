﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Obstacle : IMapObject, ILightSource
    {
        public Obstacle(Location location, Point position, ObstacleType type)
        {
            Type = type;
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }

        public ObstacleType Type { get; private set; }
        public virtual Texture2D Texture
        {
            get
            {
                if (Type == null)
                {
                    return null;
                }
                if (Type.SnowTexture != null && Location.Tilemap.GetTile(Position.GetValueOrDefault()).Type == Location.Tilemap.TileTypes.Snow)
                {
                    return Type.SnowTexture;
                }
                else
                {
                    return Type.Texture;
                }
            }
        }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public bool IsDead { get; set; }
        public Light Light { get { return Type.Light; } }

        public virtual bool OnCollision(Player player)
        {
            return false;
        }

        public virtual void Update(Player player)
        {
            // does nothing
        }
    }
}
