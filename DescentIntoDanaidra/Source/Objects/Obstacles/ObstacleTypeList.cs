﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ObstacleTypeList
    {
        public ObstacleTypeList(ContentManager content)
        {
            Tree = new ObstacleType(content.Load<Texture2D>("objects/tree"),
                content.Load<Texture2D>("objects/snow_tree"));
            JungleTree = new ObstacleType(content.Load<Texture2D>("objects/jungle_tree"), null);
            Rock = new ObstacleType(content.Load<Texture2D>("objects/rock"),
                content.Load<Texture2D>("objects/snow_rock"));
            Shrub = new ObstacleType(content.Load<Texture2D>("objects/shrub"), null);
            JungleShrub = new ObstacleType(content.Load<Texture2D>("objects/jungle_shrub"), null);
            Stairs = new ObstacleType(content.Load<Texture2D>("objects/stairsUp"), null);
            Table = new ObstacleType(content.Load<Texture2D>("objects/table"), null);
            Chair = new ObstacleType(content.Load<Texture2D>("objects/chair"), null);
            Chair1 = new ObstacleType(content.Load<Texture2D>("objects/chair1"), null);
            Chair2 = new ObstacleType(content.Load<Texture2D>("objects/chair2"), null);
            Bookcase = new ObstacleType(content.Load<Texture2D>("objects/bookcase"), null);
            BookcaseEmpty = new ObstacleType(content.Load<Texture2D>("objects/bookcaseEmpty"), null);
            AlchemyTable = new ObstacleType(content.Load<Texture2D>("objects/alchemyTable"), null);
            Anvil = new ObstacleType(content.Load<Texture2D>("objects/anvil"), null);
            Furnace = new ObstacleType(content.Load<Texture2D>("objects/furnace"), null)
            {
                Light = new Light(0.75f, 2.5f, Color.Orange),
            };
            SteelBars = new ObstacleType(content.Load<Texture2D>("objects/steelBars"), null);
            Brazier = new ObstacleType(content.Load<Texture2D>("objects/brazier"), null)
            {
                Light = new Light(0.75f, 2.5f, Color.Orange),
            };
            Gravestone = new ObstacleType(content.Load<Texture2D>("objects/gravestone"), null);
            Stove = new ObstacleType(content.Load<Texture2D>("objects/stove"), null);
            Sign = new ObstacleType(content.Load<Texture2D>("objects/sign"), null);
            Barrel = new ObstacleType(content.Load<Texture2D>("objects/barrelB"), null);
            FenceLeft = new ObstacleType(content.Load<Texture2D>("objects/fenceLeft"), null);
            FenceMid = new ObstacleType(content.Load<Texture2D>("objects/fenceMid"), null);
            FenceRight = new ObstacleType(content.Load<Texture2D>("objects/fenceRight"), null);
            FenceUp = new ObstacleType(content.Load<Texture2D>("objects/fenceUp"), null);

            Dictionary = new Dictionary<string, ObstacleType>()
            {
                { "tree", Tree },
                { "shrub", Shrub },
                { "jungleTree", JungleTree },
                { "jungleShrub", JungleShrub },
                { "stairs", Stairs },
                { "rock", Rock },
                { "invisible", Invisible },
                { "table", Table },
                { "chair", Chair },
                { "chairE", Chair1 },
                { "chairW", Chair2 },
                { "bookcase", Bookcase },
                { "bookcaseEmpty", BookcaseEmpty },
                { "alchemyTable", AlchemyTable },
                { "anvil", Anvil },
                { "furnace", Furnace },
                { "steelBars", SteelBars },
                { "brazier", Brazier },
                { "gravestone", Gravestone },
                { "stove", Stove },
                { "sign", Sign },
                { "barrel", Barrel },
                { "fenceLeft", FenceLeft },
                { "fenceMid", FenceMid },
                { "fenceRight", FenceRight },
                { "fenceUp", FenceUp },
            };
        }

        public Dictionary<string, ObstacleType> Dictionary { get; }
        public ObstacleType Tree { get; }
        public ObstacleType JungleTree { get; }
        public ObstacleType Rock { get; }
        public ObstacleType Shrub { get; }
        public ObstacleType JungleShrub { get; }
        public ObstacleType Stairs { get; }
        public ObstacleType Table { get; }
        public ObstacleType Chair { get; }
        public ObstacleType Chair1 { get; }
        public ObstacleType Chair2 { get; }
        public ObstacleType Invisible { get; }
        public ObstacleType Bookcase { get; }
        public ObstacleType BookcaseEmpty { get; }
        public ObstacleType AlchemyTable { get; }
        public ObstacleType Anvil { get; }
        public ObstacleType Furnace { get; }
        public ObstacleType SteelBars { get; }
        public ObstacleType Brazier { get; }
        public ObstacleType Gravestone { get; }
        public ObstacleType Stove { get; }
        public ObstacleType Sign { get; }
        public ObstacleType Barrel { get; }
        public ObstacleType FenceLeft { get; }
        public ObstacleType FenceMid { get; }
        public ObstacleType FenceRight { get; }
        public ObstacleType FenceUp { get; }
    }
}
