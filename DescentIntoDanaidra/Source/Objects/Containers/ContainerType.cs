﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ContainerType
    {
        public ContainerType(string name, int slots, Texture2D texture)
        {
            Name = name;
            Slots = slots;
            Texture = texture;
        }

        public string Name { get; private set; }
        public int Slots { get; private set; }
        public Texture2D Texture { get; private set; }
    }
}
