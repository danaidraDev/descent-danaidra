﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Container : IMapObject, IVendor
    {
        private readonly UIStateHandler ui;
        private readonly int itemChance;
        private readonly int rarityBonus;

        public Container(Location location, Point position, ContainerType type, UIStateHandler ui, int itemChance, int rarityBonus)
        {
            this.ui = ui;
            Type = type;
            Equipment = new List<Item>();
            Gold = new Stat("Gold", 0, 0);
            location.Tilemap.ChangeObjectLocation(this, location, position);
            this.itemChance = itemChance;
            this.rarityBonus = rarityBonus;
            FillWithItems();
        }

        public ContainerType Type { get; private set; }
        public string Name { get { return Type.Name; } }
        public int EquipmentSlots { get { return Type.Slots; } }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return Type.Texture; } }
        public List<Item> Equipment { get; }
        public Stat Gold { get; private set; }
        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            ui.OpenContainer(player, this);
            return false;
        }

        public virtual void Update(Player player)
        {
            // does nothing
        }

        void FillWithItems()
        {
            for (int i = 0; i < EquipmentSlots; i++)
            {
                if (Randomiser.PassPercentileRoll(itemChance))
                {
                    Equipment.Add(Loot.RandomLoot(rarityBonus));
                }
            }
        }
    }
}
