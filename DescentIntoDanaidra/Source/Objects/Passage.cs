﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Passage : IMapObject
    {
        public Passage(Location location, Point position, Location targetLocation, Point targetPosition)
        {
            TargetLocation = targetLocation;
            TargetPosition = targetPosition;
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }
        
        public Location TargetLocation { get; private set; }
        public Point TargetPosition { get; private set; }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return null; } }
        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            if (TargetLocation != null)
            {
                Location.Tilemap.ChangeObjectLocation(player, TargetLocation, TargetPosition);
                HUD.MessageLog.DisplayMessage("You enter " + TargetLocation.Name + ".");
                return true;
            }
            return false;
        }

        public virtual void Update(Player player)
        {
            // does nothing
        }
    }
}
