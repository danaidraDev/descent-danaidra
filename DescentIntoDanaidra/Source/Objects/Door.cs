﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Door : IMapObject
    {
        public Door(Location location, Point position, TileTypeList tileTypeList)
        {
            location.Tilemap.ChangeObjectLocation(this, location, position);
            Texture = null;
            ChangeTile(location, position, tileTypeList.Door);
        }

        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get; private set; }
        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            HUD.MessageLog.DisplayMessage("This door is locked.", Colors.Orange);
            return false;
        }

        public virtual void Update(Player player)
        {
        }

        private void ChangeTile(Location location, Point position, TileType tiletype)
        {
            if (tiletype != null)
            {
                location.Tilemap.SetTile(position, tiletype);
            }
        }
    }
}
