﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Pushable : IMapObject
    {
        public Pushable(Location location, Point position, ObstacleType type)
        {
            Type = type;
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }

        public ObstacleType Type { get; }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return Type.Texture; } }

        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            GetPushed(player);
            return false;
        }

        private void GetPushed(Player player)
        {
            Point previousPosition = Position.GetValueOrDefault();
            Point targetPosition = new Point(previousPosition.X + (previousPosition.X - player.Position.GetValueOrDefault().X),
                previousPosition.Y + (previousPosition.Y - player.Position.GetValueOrDefault().Y));

            if (CanMoveToPosition(targetPosition))
            {
                SoundHandler.PlaySound(SoundLibrary.PushRumble);
                Location.Tilemap.ChangeObjectPosition(this, targetPosition);
            }
        }

        private bool CanMoveToPosition(Point targetPosition)
        {
            return Location.Tilemap.IsWalkable(targetPosition) && Location.Tilemap.GetObject(targetPosition) == null;
        }

        public virtual void Update(Player player)
        {
        }
    }
}
