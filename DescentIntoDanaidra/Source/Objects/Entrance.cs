﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Entrance : IMapObject
    {
        public Entrance(Location location, Point position, Region region, int currentLocationIndex, int targetLevelIndex, Helper.Direction direction)
        {
            location.Tilemap.ChangeObjectLocation(this, location, position);
            Position = position;
            LocationIndex = currentLocationIndex;
            TargetLevelIndex = targetLevelIndex;
            Direction = direction;
            Region = region;
        }
        
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public int LocationIndex { get; set; }
        public int TargetLevelIndex { get; set; }
        public Texture2D Texture { get { return null; } }
        public Region Region { get; private set; }
        public bool IsDead { get; set; }
        public Helper.Direction Direction { get; private set; }

        public bool OnCollision(Player player)
        {
            Region.EnterLevel(this, player);
            return true;
        }

        public virtual void Update(Player player)
        {
        }
    }
}
