﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Altar : IMapObject
    {
        UIStateHandler ui;
        Player player;

        public Altar(Location location, Point position, AltarType type, UIStateHandler ui)
        {
            Type = type;
            this.ui = ui;
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }

        public AltarType Type { get; private set; }
        public virtual Texture2D Texture { get { return Type.Texture; } }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public bool IsDead { get; set; }
        public Light Light { get { return Type.Light; } }

        public virtual bool OnCollision(Player player)
        {
            HUD.MessageLog.DisplayMessage("You approach the altar of " + Type.Name + ".", Colors.SteelGray);
            this.player = player;
            ui.OpenYesNoPrompt("Offering", "Do you wish to make a small (5 gold) donation at this altar?", ReceiveBlessing, null);
            return false;
        }

        void ReceiveBlessing()
        {
            if (player.Gold.Current > 5)
            {
                player.Gold.Current -= 5;
                SoundHandler.PlaySound(SoundLibrary.CoinJingle);
                Type.Blessing(player);
                HUD.MessageLog.DisplayMessage("You receive the blessing.", Colors.SteelGray);
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You do not have enough gold.", Colors.Orange);
            }
        }

        public virtual void Update(Player player)
        {
            // does nothing
        }
    }
}
