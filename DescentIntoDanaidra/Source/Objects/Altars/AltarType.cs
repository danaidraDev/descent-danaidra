﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class AltarType
    {
        public AltarType(string name, Texture2D texture, Action<Player> blessing)
        {
            Name = name;
            Texture = texture;
            Blessing = blessing;
        }

        public string Name { get; private set; }
        public Texture2D Texture { get; private set; }
        public Action<Player> Blessing { get; private set; }
        public Light Light { get; set; }
    }
}
