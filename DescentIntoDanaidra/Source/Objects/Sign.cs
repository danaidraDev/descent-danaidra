﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Sign : IMapObject
    {
        UIStateHandler ui;

        public Sign(Location location, Point position, ObstacleType type, string text, UIStateHandler ui)
        {
            this.ui = ui;
            Type = type;
            Text = text;
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }

        public ObstacleType Type { get; }
        public string Text { get; }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return Type.Texture; } }

        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            if (!string.IsNullOrEmpty(Text))
            {
                ui.OpenMessagePrompt("Sign", "The sign reads <<" + Text + ">>.");
            }
            return false;
        }

        public virtual void Update(Player player)
        {
        }
    }
}
