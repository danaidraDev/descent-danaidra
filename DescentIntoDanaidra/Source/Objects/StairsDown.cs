﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class StairsDown : IMapObject
    {
        public StairsDown(Location location, Point position, Dungeon dungeon, TileTypeList tileTypes)
        {
            Dungeon = dungeon;
            TileType = tileTypes.StairsDown;
            location.Tilemap.ChangeObjectLocation(this, location, position);
            ChangeTile(location, position);
        }

        public TileType TileType { get; private set; }
        public Dungeon Dungeon { get; private set; }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return null; } }
        public bool IsDead { get; set; }

        private void ChangeTile(Location location, Point position)
        {
            if(TileType != null)
            {
                location.Tilemap.SetTile(position, TileType);
            }
        }

        public bool OnCollision(Player player)
        {
            if(player.Location is DungeonLevel dungeonLevel && dungeonLevel.HasLevelsBelow())
            {
                dungeonLevel.GoDownOneLevel(player);
            }
            else
            {
                Dungeon.EnterDungeon(player);
            }
            return true;
        }

        public virtual void Update(Player player)
        {
        }
    }
}
