﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class StairsUp : Obstacle
    {
        public StairsUp(Location location, Point position, Dungeon dungeon, ObstacleType type)
            : base (location, position, type)
        {
            Dungeon = dungeon;
        }
        
        public Dungeon Dungeon { get; private set; }

        public override bool OnCollision(Player player)
        {
            if (player.Location is DungeonLevel dungeonLevel)
            {
                dungeonLevel.GoUpOneLevel(player);
                return true;
            }
            return false;
        }
    }
}
