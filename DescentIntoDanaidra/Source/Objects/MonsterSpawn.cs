﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class MonsterSpawn : IMapObject
    {
        private int counter;
        private List<Monster> monsters;

        public MonsterSpawn(Location location, Point position, int distance, int delay, int maxMonsters, Texture2D texture, MonsterType monsterType)
        {
            Location = location;
            Position = position;
            Location.Spawns.Add(this);

            monsters = new List<Monster>();
            MonsterType = monsterType;
            Distance = distance;
            Delay = delay;
            MaxMonsters = maxMonsters;
            counter = 0;

            Reset();
        }
        
        public int Distance { get; private set; }
        public int Delay { get; private set; }
        public MonsterType MonsterType { get; private set; }
        public int MaxMonsters { get; private set; }
        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get { return null; } }
        public bool IsDead { get; set; }

        public virtual bool OnCollision(Player player)
        {
            return false;
        }

        bool CanSpawn(Point pos)
        {
            return Location.Tilemap.IsPositionWithinTilemap(pos) && Location.Tilemap.IsWalkable(pos) &&
                Location.Tilemap.GetObject(pos) == null;
        }

        void SpawnMonster()
        {
            int attempts = 0;
            bool isSpawned = false;

            while (!isSpawned && attempts < 50)
            {
                attempts++;

                Point pos = new Point(Randomiser.RandomNumber(Position.GetValueOrDefault().X - Distance, Position.GetValueOrDefault().X + Distance),
                    Randomiser.RandomNumber(Position.GetValueOrDefault().Y - Distance, Position.GetValueOrDefault().Y + Distance));

                if (CanSpawn(pos) && MonsterType != null)
                {
                    monsters.Add(new Monster(Location, pos, MonsterType, this, 4));
                    isSpawned = true;
                }
            }
        }

        void Reset()
        {
            if (monsters.Any())
            {
                for (int i = 0; i < monsters.Count; i++)
                {
                    if (monsters[i] != null)
                    {
                        monsters[i].IsDead = true;
                    }
                    monsters.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < MaxMonsters; i++)
            {
                SpawnMonster();
            }
        }

        void CheckForDead()
        {
            for (int i = 0; i < monsters.Count; i++)
            {
                if (monsters[i].IsDead)
                {
                    monsters.RemoveAt(i);
                    i--;
                }
            }
        }

        void UpdateCounter(Player player)
        {
            if (counter >= Delay)
            {
                if (monsters.Count < MaxMonsters && Vector2.Distance(player.Position.GetValueOrDefault().ToVector2(),
                    Position.GetValueOrDefault().ToVector2()) > 20)
                {
                    SpawnMonster();
                }

                counter = 0;
            }
            else
            {
                counter++;
            }
        }

        public void Update(Player player)
        {
            CheckForDead();
            UpdateCounter(player);
        }
    }
}
