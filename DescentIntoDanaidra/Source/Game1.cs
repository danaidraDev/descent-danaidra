﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Game1 : Game
    {
        public const int TileSize = 60;
        public const int BufferWidth = TileSize * 15;
        public const int BufferHeight = TileSize * 11;
        public const string GameTitle = "Descent into Danaidra";

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private InputHandler input;
        private SoundHandler sound;
        private SoundLibrary soundLibrary;
        private MusicLibrary musicLibrary;
        private Stack<GameState> gameStates;
        private Texture2D background;
        private UIStateHandler ui;
        private Options options;

        public Game1()
        {
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(20);

            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = BufferWidth,
                PreferredBackBufferHeight = BufferHeight,
                IsFullScreen = false
            };
            
            Window.Title = GameTitle;
            IsMouseVisible = false;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            input = new InputHandler();
            sound = new SoundHandler();
            soundLibrary = new SoundLibrary(Content);
            musicLibrary = new MusicLibrary(Content);
            ui = new UIStateHandler(Content, graphics.GraphicsDevice);
            options = new Options();

            gameStates = new Stack<GameState>();
            gameStates.Push(new MainMenu(Content, graphics, input, gameStates, sound, ui));
            
            spriteBatch = new SpriteBatch(GraphicsDevice);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            background = Content.Load<Texture2D>("ui/splash");
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                input.Update();

                if (gameStates.Any())
                {
                    if (gameStates.Peek().End)
                    {
                        CloseTopState();
                    }
                    else
                    {
                        gameStates.Peek().Update(gameTime);
                    }
                }
                else
                {
                    Exit();
                }
            }

            base.Update(gameTime);
        }

        private void CloseTopState()
        {
            gameStates.Pop();

            if (gameStates.Any())
            {
                gameStates.Peek().Open();
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (gameStates.Any())
            {
                if (!gameStates.Peek().End)
                {
                    gameStates.Peek().Draw(spriteBatch);
                }
                else
                {
                    spriteBatch.Begin();
                    spriteBatch.Draw(background, new Rectangle(0, 0, graphics.GraphicsDevice.Viewport.Width,
                        graphics.GraphicsDevice.Viewport.Height), Color.White);
                    spriteBatch.End();
                }
            }

            base.Draw(gameTime);
        }
    }
}
