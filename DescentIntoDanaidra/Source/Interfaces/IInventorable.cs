﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public interface IInventorable
    {
        string Name { get; }
        string TypeName { get; }
        string Description { get; }
        string PropertyLine1 { get; }
        string PropertyLine2 { get; }
        string PropertyLine3 { get; }
        Texture2D Texture { get; }
    }
}
