﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace DescentIntoDanaidra
{
    public interface IMapObject
    {
        Texture2D Texture { get; }
        Location Location { get; set; }
        Point? Position { get; set; }
        bool IsDead { get; set; }

        bool OnCollision(Player player);
        void Update(Player player);
    }
}