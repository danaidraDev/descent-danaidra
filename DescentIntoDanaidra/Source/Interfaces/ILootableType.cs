﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public interface ILootableType
    {
        int DropChance { get; }
    }
}
