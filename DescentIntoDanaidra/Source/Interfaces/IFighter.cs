﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public interface IFighter : IMapObject, IMoveable
    {
        string Name { get; }
        List<Status> Statuses { get; }
        Weapon EquippedWeapon { get; }
        Armor EquippedArmor { get; }

        #region primaries
        Stat Vitality { get; }
        Stat Magic { get; }
        Stat Strength { get; }
        Stat Focus { get; }
        #endregion

        #region secondaries
        Stat Level { get; }
        Stat Experience { get; }
        Stat Health { get; }
        Stat Mana { get; }
        int BaseDamage { get; }
        int BaseDefense { get; }
        Stat Sleep { get; }
        Stat Food { get; }
        #endregion

        #region bonuses
        int DefenseBonus { get; }
        int DamageBonus { get; }
        #endregion

        int ExpGained { get; }

        void IncreaseExp(int amount);
        void CheckLevelUp();
        void LevelUp();
        void AddStatus(Status status);
        void UpdateStatuses();
        void Die(IFighter attacker);
        void Kill(IFighter target);
    }
}