﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra
{
    public class KeyBinds
    {
        public static bool UpKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad8) || input.KeyHit(Keys.Up);
        }

        public static bool UpKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad8) || input.KeyPressed(Keys.Up);
        }

        public static bool UpLeftKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad7);
        }

        public static bool UpLeftKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad7);
        }

        public static bool UpRightKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad9);
        }

        public static bool UpRightKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad9);
        }

        public static bool DownKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad2) || input.KeyHit(Keys.Down);
        }

        public static bool DownKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad2) || input.KeyPressed(Keys.Down);
        }

        public static bool DownLeftKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad1);
        }

        public static bool DownLeftKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad1);
        }

        public static bool DownRightKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad3);
        }

        public static bool DownRightKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad3);
        }

        public static bool LeftKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad4) || input.KeyHit(Keys.Left);
        }

        public static bool LeftKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad4) || input.KeyPressed(Keys.Left);
        }

        public static bool RightKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad6) || input.KeyHit(Keys.Right);
        }

        public static bool RightKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad6) || input.KeyPressed(Keys.Right);
        }

        public static bool WaitKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad5) || input.KeyHit(Keys.RightShift);
        }

        public static bool WaitKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad5) || input.KeyPressed(Keys.RightShift);
        }

        public static bool PickUpKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad0) || input.KeyHit(Keys.RightControl);
        }

        public static bool PickUpKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad0) || input.KeyPressed(Keys.RightControl);
        }

        public static bool ConfirmKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.Enter);
        }

        public static bool ConfirmKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.Enter);
        }

        public static bool CancelKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.Escape) || input.KeyHit(Keys.Subtract) ||
                input.KeyHit(Keys.OemMinus);
        }

        public static bool CancelKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.Escape) || input.KeyPressed(Keys.Subtract) ||
                input.KeyPressed(Keys.OemMinus);
        }

        public static bool DetailsKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.OemPeriod) || input.KeyHit(Keys.Decimal);
        }

        public static bool DetailsKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.OemPeriod) || input.KeyPressed(Keys.Decimal);
        }

        public static bool ProgressKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.Add) || input.KeyHit(Keys.OemPlus);
        }

        public static bool ProgressKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.Add) || input.KeyPressed(Keys.OemPlus);
        }

        public static bool DropKeyHit(InputHandler input)
        {
            return input.KeyHit(Keys.NumPad0) || input.KeyHit(Keys.RightControl);
        }

        public static bool DropKeyPressed(InputHandler input)
        {
            return input.KeyPressed(Keys.NumPad0) || input.KeyPressed(Keys.RightControl);
        }
    }
}
