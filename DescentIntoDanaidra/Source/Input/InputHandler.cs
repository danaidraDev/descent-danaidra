﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra
{
    public class InputHandler
    {
        #region MOUSE
        MouseState currentMouseState = Mouse.GetState();
        MouseState previousMouseState;
        
        public Point MousePosition()
        {
            return currentMouseState.Position;
        }
        
        public bool MouseInViewport(GraphicsDeviceManager graphics)
        {
            return graphics.GraphicsDevice.Viewport.Bounds.Contains(Mouse.GetState().Position);
        }
        
        public bool MouseLeftClick()
        {
            return currentMouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released;
        }
        
        public bool MouseLeftPressed()
        {
            return currentMouseState.LeftButton == ButtonState.Pressed;
        }
        
        public bool MouseRightClick()
        {
            return currentMouseState.RightButton == ButtonState.Pressed && previousMouseState.RightButton == ButtonState.Released;
        }
        
        public bool MouseRightPressed()
        {
            return currentMouseState.RightButton == ButtonState.Pressed;
        }
        #endregion

        #region KEYBOARD
        KeyboardState currentKeyboardState = Keyboard.GetState();
        KeyboardState previousKeyboardState;
        
        public bool KeyHit(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key) && previousKeyboardState.IsKeyUp(key);
        }
        
        public bool KeyPressed(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key);
        }
        #endregion

        public void Update()
        {
            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();

            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();
        }

        public bool AnyKeyPressed()
        {
            return Keyboard.GetState().GetPressedKeys() != null && Keyboard.GetState().GetPressedKeys().Length != 0;
        }
    }
}

