# Descent into Danaidra
Repository for the Descent into Danaidra game.

## Notices
This game was put up on the MonoGame framework:
https://www.monogame.net/

The MonoGame license is available here:
https://github.com/MonoGame/MonoGame/blob/develop/LICENSE.txt